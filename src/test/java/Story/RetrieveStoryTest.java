package Story;

import Clients.StoryClient;
import Common.Flows;
import Helpers.ConfigurationInfo;
import arp.FlowReport;
import arp.ReportService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.sql.ResultSet;

import static Database.Connector.closeMySqlStatement;
import static Database.Connector.getTestCases;
import static Database.RelimeDBConnector.closeRelimeDBStatement;
import static Helpers.ConfigurationInfo.businessCriticality;

/**
 * Created by mukhin on 11/28/2016.
 */
@RunWith(JUnit4.class)
public class RetrieveStoryTest {
    private final String tableName = "Story";
    private Flows flow = new Flows(tableName, StoryClient.class, this.getClass(), "getRetrieveStoryResponse");

    @Before
    public void setUp() {
    }

    @Test
    public void retrieveStoryCheck() throws Exception {
        for (String aBusinessCriticality : businessCriticality) {
            ResultSet setOfTestCases = getTestCases(tableName, "checkRetrieveStory", aBusinessCriticality);
            if (setOfTestCases.next()) {
                setOfTestCases.beforeFirst();
                FlowReport.open(ConfigurationInfo.arpProjectKey, this.getClass().getName());
                flow.checkRetrieveStory(setOfTestCases);
                ReportService.closeAndSendToAnotherURL("http://10.10.80.162:90/Services/Deployment.asmx?WSDL");
            }
        }
    }

    @After
    public void CreateReport() throws Exception {
        try {
            closeRelimeDBStatement();
            closeMySqlStatement();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
