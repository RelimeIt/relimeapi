package Jira;

import Clients.JiraClient;
import Common.Flows;
import Helpers.ConfigurationInfo;
import arp.FlowReport;
import arp.ReportService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.sql.ResultSet;

import static Database.Connector.closeMySqlStatement;
import static Database.Connector.getTestCases;
import static Database.RelimeDBConnector.closeRelimeDBStatement;
import static Helpers.ConfigurationInfo.businessCriticality;

/**
 * Created by mukhin on 1/5/2017.
 */
@RunWith(JUnit4.class)
public class GetJiraLinkedStoryDetailsTest {
    private final String tableName = "Jira";
    private Flows flow = new Flows(tableName, JiraClient.class, this.getClass(), "getJiraLinkedStoryDetailsResponse");

    @Before
    public void setUp() {
    }

    @Test
    public void isJiraUniqueStoryTest() throws Exception {
        for (String aBusinessCriticality : businessCriticality) {
            ResultSet setOfTestCases = getTestCases(tableName, "checkJiraLinkedStoryDetails", aBusinessCriticality);
            if (setOfTestCases.next()) {
                setOfTestCases.beforeFirst();
                FlowReport.open(ConfigurationInfo.arpProjectKey, this.getClass().getName());
                flow.checkJiraLinkedStoryDetails(setOfTestCases);
                ReportService.closeAndSendToAnotherURL("http://10.10.80.162:90/Services/Deployment.asmx?WSDL");
            }
        }
    }

    @After
    public void CreateReport() throws Exception {
        try {
            closeRelimeDBStatement();
            closeMySqlStatement();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}

