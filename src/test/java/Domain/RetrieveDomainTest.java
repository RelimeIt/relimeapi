package Domain;

import Clients.DomainClient;
import Common.Flows;
import Common.Methods;
import Helpers.ConfigurationInfo;
import arp.FlowReport;
import arp.ReportService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.sql.ResultSet;

import static Database.Connector.closeMySqlStatement;
import static Database.Connector.getTestCases;
import static Database.RelimeDBConnector.closeRelimeDBStatement;
import static Helpers.ConfigurationInfo.businessCriticality;

/**
 * Created by mukhin on 11/10/2016.
 */
@RunWith(JUnit4.class)
public class RetrieveDomainTest {
    private final String tableName = "Domain";
    private Flows flow = new Flows(tableName, DomainClient.class, this.getClass(), "getRetreiveDomainResponse");
    Methods methods = new Methods();

    @Before
    public void setUp() {
    }

    @Test
    public void retrieveDomainTest() throws Exception {
        for (String aBusinessCriticality : businessCriticality) {
            ResultSet setOfTestCases = getTestCases(tableName, "retrieveDomainCheck", aBusinessCriticality);
            if (setOfTestCases.next()) {
                setOfTestCases.beforeFirst();
                FlowReport.open(ConfigurationInfo.arpProjectKey, this.getClass().getName());
                flow.retrieveDomainCheck(setOfTestCases);
                ReportService.closeAndSendToAnotherURL("http://10.10.80.162:90/Services/Deployment.asmx?WSDL");
            }
        }
    }

    @After
    public void CreateReport() throws Exception {
        try {
            closeRelimeDBStatement();
            closeMySqlStatement();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
