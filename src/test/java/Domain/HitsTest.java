package Domain;

import Clients.DomainClient;
import Common.Flows;
import Helpers.ConfigurationInfo;
import arp.FlowReport;
import arp.ReportService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.sql.ResultSet;

import static Database.Connector.closeMySqlStatement;
import static Database.Connector.getTestCases;
import static Helpers.ConfigurationInfo.businessCriticality;

/**
 * Created by mukhin on 11/8/2016.
 */
@RunWith(JUnit4.class)
public class HitsTest {
    private final String tableName = "Domain";
    private Flows flow = new Flows(tableName, DomainClient.class, this.getClass(), "getHitsResponse");

    @Before
    public void setUp() {
    }

    @Test
    public void hitsTest() throws Exception {
        for (String aBusinessCriticality : businessCriticality) {
            ResultSet setOfTestCases = getTestCases(tableName, "hitsCheck", aBusinessCriticality);
            if (setOfTestCases.next()) {
                setOfTestCases.beforeFirst();
                FlowReport.open(ConfigurationInfo.arpProjectKey, this.getClass().getName());

                flow.hitsCheck(setOfTestCases);

                ReportService.closeAndSendToAnotherURL("http://10.10.80.162:90/Services/Deployment.asmx?WSDL");
            }
        }
    }

    @After
    public void CreateReport() throws Exception {
        try {
            closeMySqlStatement();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
