package Account;

import Clients.AccountClient;
import Common.Flows;
import Helpers.ConfigurationInfo;
import arp.FlowReport;
import arp.ReportService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.sql.ResultSet;

import static Database.Connector.closeMySqlStatement;
import static Database.Connector.getTestCases;
import static Database.RelimeDBConnector.closeRelimeDBStatement;
import static Helpers.ConfigurationInfo.businessCriticality;

/**
 * Created by mukhin on 11/23/2016.
 */
@RunWith(JUnit4.class)
public class DeleteAccountTest {
    private final String tableName = "Account";
    private Flows flow = new Flows(tableName, AccountClient.class, this.getClass(), "getDeleteAccountResponse");

    @Before
    public void setUp() {
    }

    @Test
    public void deleteAccountTest() throws Exception {
        for (String aBusinessCriticality : businessCriticality) {
            ResultSet setOfTestCases = getTestCases(tableName, "checkDeleteAccount", aBusinessCriticality);
            if (setOfTestCases.next()) {
                setOfTestCases.beforeFirst();
                FlowReport.open(ConfigurationInfo.arpProjectKey, this.getClass().getName());
                flow.checkDeleteAccount(setOfTestCases);
                System.setProperty("http.proxyHost", "localhost");
                System.setProperty("http.proxyPort", "8888");
                ReportService.closeAndSendToAnotherURL("http://10.10.80.162:90/Services/Deployment.asmx?WSDL");
            }
        }
    }

    @After
    public void CreateReport() throws Exception {
        try {
            closeRelimeDBStatement();
            closeMySqlStatement();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
