package Auth;

import Clients.AuthClient;
import Common.Flows;
import Common.Methods;
import Helpers.Classes.Domains;
import Helpers.ConfigurationInfo;
import arp.FlowReport;
import arp.ReportService;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.IOException;
import java.sql.ResultSet;

import static Database.Connector.closeMySqlStatement;
import static Database.Connector.getTestCases;
import static Helpers.ConfigurationInfo.businessCriticality;

/**
 * Created by mukhin on 11/3/2016.
 */
@RunWith(JUnit4.class)
public class ChangeBasicInfoTest {
    private final String tableName = "Auth";
    private Flows flow = new Flows(tableName, AuthClient.class, this.getClass(), "getChangeBasicInfoResponse");
    Methods methods = new Methods();

    @Before
    public void setUp()  {

    }

    @Test
    public void changeBasicInfoCheck() throws Exception {
        for (String aBusinessCriticality : businessCriticality) {
            ResultSet setOfTestCases = getTestCases(tableName, "changeBasicInfoCheck", aBusinessCriticality);
            if (setOfTestCases.next()) {
                setOfTestCases.beforeFirst();
                FlowReport.open(ConfigurationInfo.arpProjectKey, this.getClass().getName());
                flow.changeBasicInfoCheck(setOfTestCases);
                ReportService.closeAndSendToAnotherURL("http://10.10.80.162:90/Services/Deployment.asmx?WSDL");
            }
        }
    }

    @After
    public void CreateReport() throws Exception {
        try {
            closeMySqlStatement();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
