package Project;

import Clients.ProjectClient;
import Common.Flows;
import Helpers.ConfigurationInfo;
import arp.FlowReport;
import arp.ReportService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.sql.ResultSet;

import static Database.Connector.closeMySqlStatement;
import static Database.Connector.getTestCases;
import static Database.RelimeDBConnector.closeRelimeDBStatement;
import static Helpers.ConfigurationInfo.businessCriticality;

/**
 * Created by mukhin on 11/16/2016.
 */
@RunWith(JUnit4.class)
public class RetrieveProjectTest {
    private final String tableName = "Project";
    private Flows flow = new Flows(tableName, ProjectClient.class, this.getClass(), "getRetrieveProjectResponse");

    @Before
    public void setUp() {
    }

    @Test
    public void retrieveProjectTest() throws Exception {
        for (String aBusinessCriticality : businessCriticality) {
            ResultSet setOfTestCases = getTestCases(tableName, "checkRetrieveProject", aBusinessCriticality);
            if (setOfTestCases.next()) {
                setOfTestCases.beforeFirst();
                FlowReport.open(ConfigurationInfo.arpProjectKey, this.getClass().getName());
                flow.checkRetrieveProject(setOfTestCases);
                ReportService.closeAndSendToAnotherURL("http://10.10.80.162:90/Services/Deployment.asmx?WSDL");
            }
        }
    }

    @After
    public void CreateReport() throws Exception {
        try {
            closeRelimeDBStatement();
            closeMySqlStatement();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
