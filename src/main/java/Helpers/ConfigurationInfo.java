package Helpers;

import org.apache.commons.configuration.XMLConfiguration;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

/**
 * Created by logovskoy on 10/26/2016.
 */
public class ConfigurationInfo {

    public static String URL;

    public static String getURLWithPrefix() {
        return URLWithPrefix;
    }

    // prefix can be name of domain or name of project, etc.
    public static void setURLWithPrefix(String prefix) {
        ConfigurationInfo.URLWithPrefix = "http://" + prefix + "." + URL.replace("http://", "");
    }

    private static String URLWithPrefix;
    public static String LOGIN;
    public static String PASSWORD;
    private static String ReportSystemURL;
    private static String TargetTestCycle;
    public static String DBLocation;
    public static List<String> businessCriticality;
    public static String arpProjectKey;
    public static String serverLogin;
    public static String serverPassword;
    public static String databaseIp;
    public static String dbLogin;
    public static String dbPassword;

    static {
        HashMap<String, String> config = read();
        if (config != null) {
            try {
                URL = "http://" + config.get("url");
                LOGIN = config.get("login");
                PASSWORD = config.get("password");
                ReportSystemURL = config.get("reportingSystemURL");
                TargetTestCycle = config.get("targetTestCycle");
                DBLocation = config.get("dbLocation");
                arpProjectKey = config.get("arpProjectKey");
                serverLogin = config.get("serverLogin");
                serverPassword = config.get("serverPassword");
                databaseIp = config.get("databaseIp");
                dbLogin = config.get("dbLogin");
                dbPassword = config.get("dbPassword");
                try {
                    String commandLineArguments = System.getProperty("priority");
                    if (commandLineArguments != null) {
                        businessCriticality = Arrays.asList(commandLineArguments.toLowerCase().split("/"));
                    } else {
                        businessCriticality = Arrays.asList(config.get("businessCriticality").toLowerCase().split("/"));
                        if (businessCriticality.size() == 0) {
                            throw new Exception();
                        }
                    }
                } catch (Exception ex) {
                    businessCriticality = Collections.singletonList(config.get("businessCriticality").toLowerCase());
                }
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        }
    }

    private static HashMap<String, String> read() {
        XMLConfiguration config;
        HashMap<String, String> map = new HashMap<>();
        try {
            config = new XMLConfiguration("RelimeConfiguration.xml");
            map.put("arpProjectKey", config.getString("arpProjectKey"));
            map.put("url", config.getString("url"));
            map.put("login", config.getString("login"));
            map.put("password", config.getString("password"));
            map.put("reportingSystemURL", config.getString("reportingSystemURL"));
            map.put("targetTestCycle", config.getString("targetTestCycle"));
            map.put("dbLocation", config.getString("dbLocation"));
            map.put("businessCriticality", config.getString("businessCriticality"));
            map.put("dbLogin", config.getString("relimeDbConfiguration.dbLogin"));
            map.put("dbPassword", config.getString("relimeDbConfiguration.dbPassword"));
            map.put("serverLogin", config.getString("sftpConfiguration.serverLogin"));
            map.put("serverPassword", config.getString("sftpConfiguration.serverPassword"));
            map.put("databaseIp", config.getString("sftpConfiguration.databaseIp"));
            return map;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}
