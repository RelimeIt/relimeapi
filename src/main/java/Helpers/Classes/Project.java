
package Helpers.Classes;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
        "id",
        "name",
        "key",
        "description",
        "bddFrameworkType",
        "isActive",
        "isPrivate",
        "savingMode"
})
public class Project {

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("name")
    private String name;
    @JsonProperty("key")
    private String key;
    @JsonProperty("description")
    private String description;
    @JsonProperty("bddFrameworkType")
    private String bddFrameworkType;
    @JsonProperty("isActive")
    private Boolean isActive;
    @JsonProperty("isPrivate")
    private Boolean isPrivate;
    @JsonProperty("savingMode")
    private String savingMode;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     *
     * @return
     * The id
     */
    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The name
     */
    @JsonProperty("name")
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     * The key
     */
    @JsonProperty("key")
    public String getKey() {
        return key;
    }

    /**
     *
     * @param key
     * The key
     */
    @JsonProperty("key")
    public void setKey(String key) {
        this.key = key;
    }

    /**
     *
     * @return
     * The description
     */
    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    /**
     *
     * @param description
     * The description
     */
    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     *
     * @return
     * The bddFrameworkType
     */
    @JsonProperty("bddFrameworkType")
    public String getBddFrameworkType() {
        return bddFrameworkType;
    }

    /**
     *
     * @param bddFrameworkType
     * The bddFrameworkType
     */
    @JsonProperty("bddFrameworkType")
    public void setBddFrameworkType(String bddFrameworkType) {
        this.bddFrameworkType = bddFrameworkType;
    }

    /**
     *
     * @return
     * The isActive
     */
    @JsonProperty("isActive")
    public Boolean getIsActive() {
        return isActive;
    }

    /**
     *
     * @param isActive
     * The isActive
     */
    @JsonProperty("isActive")
    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    /**
     *
     * @return
     * The isPrivate
     */
    @JsonProperty("isPrivate")
    public Boolean getIsPrivate() {
        return isPrivate;
    }

    /**
     *
     * @param isPrivate
     * The isPrivate
     */
    @JsonProperty("isPrivate")
    public void setIsPrivate(Boolean isPrivate) {
        this.isPrivate = isPrivate;
    }

    /**
     *
     * @return
     * The savingMode
     */
    @JsonProperty("savingMode")
    public String getSavingMode() {
        return savingMode;
    }

    /**
     *
     * @param savingMode
     * The savingMode
     */
    @JsonProperty("savingMode")
    public void setSavingMode(String savingMode) {
        this.savingMode = savingMode;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Project project = (Project) o;

        if (id != null ? !id.equals(project.id) : project.id != null) return false;
        if (name != null ? !name.equals(project.name) : project.name != null) return false;
        if (key != null ? !key.equals(project.key) : project.key != null) return false;
        if (description != null ? !description.equals(project.description) : project.description != null) return false;
        if (bddFrameworkType != null ? !bddFrameworkType.equals(project.bddFrameworkType) : project.bddFrameworkType != null)
            return false;
        if (isActive != null ? !isActive.equals(project.isActive) : project.isActive != null) return false;
        if (isPrivate != null ? !isPrivate.equals(project.isPrivate) : project.isPrivate != null) return false;
        if (savingMode != null ? !savingMode.equals(project.savingMode) : project.savingMode != null) return false;
        if (additionalProperties != null ? !additionalProperties.equals(project.additionalProperties) : project.additionalProperties != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (key != null ? key.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (bddFrameworkType != null ? bddFrameworkType.hashCode() : 0);
        result = 31 * result + (isActive != null ? isActive.hashCode() : 0);
        result = 31 * result + (isPrivate != null ? isPrivate.hashCode() : 0);
        result = 31 * result + (savingMode != null ? savingMode.hashCode() : 0);
        result = 31 * result + (additionalProperties != null ? additionalProperties.hashCode() : 0);
        return result;
    }
}