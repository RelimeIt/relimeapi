package Helpers.Classes;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Generated;
import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
        "isFolder",
        "label",
        "children",
        "id",
        "parentId",
        "storyId"
})
public class StoryTree {

    @JsonProperty("isFolder")
    private Boolean isFolder;
    @JsonProperty("label")
    private String label;
    @JsonProperty("children")
    private List<StoryTree> children = new ArrayList<StoryTree>();
    @JsonProperty("id")
    private Integer id;
    @JsonProperty("parentId")
    private Integer parentId;
    @JsonProperty("storyId")
    private Integer storyId;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     *
     * @return
     * The isFolder
     */
    @JsonProperty("isFolder")
    public Boolean getIsFolder() {
        return isFolder;
    }

    /**
     *
     * @param isFolder
     * The isFolder
     */
    @JsonProperty("isFolder")
    public void setIsFolder(Boolean isFolder) {
        this.isFolder = isFolder;
    }

    /**
     *
     * @return
     * The label
     */
    @JsonProperty("label")
    public String getLabel() {
        return label;
    }

    /**
     *
     * @param label
     * The label
     */
    @JsonProperty("label")
    public void setLabel(String label) {
        this.label = label;
    }

    /**
     *
     * @return
     * The children
     */
    @JsonProperty("children")
    public List<StoryTree> getChildren() {
        return children;
    }

    /**
     *
     * @param children
     * The children
     */
    @JsonProperty("children")
    public void setChildren(List<StoryTree> children) {
        if (children == null) {
            children = new ArrayList<>();
        }
        this.children = children;
    }

    /**
     *
     * @return
     * The id
     */
    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The parentId
     */
    @JsonProperty("parentId")
    public Integer getParentId() {
        return parentId;
    }

    /**
     *
     * @param parentId
     * The parentId
     */
    @JsonProperty("parentId")
    public void setParentId(Integer parentId) {
        if (parentId == null) {
            parentId = 0;
        }
        this.parentId = parentId;

    }

    /**
     *
     * @return
     * The storyId
     */
    @JsonProperty("storyId")
    public Integer getStoryId() {
        return storyId;
    }

    /**
     *
     * @param storyId
     * The storyId
     */
    @JsonProperty("storyId")
    public void setStoryId(Integer storyId) {
        if (storyId == null) {
            storyId = 0;
        }
        this.storyId = storyId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        StoryTree storyTree = (StoryTree) o;

        if (isFolder != null ? !isFolder.equals(storyTree.isFolder) : storyTree.isFolder != null) return false;
        if (label != null ? !label.equals(storyTree.label) : storyTree.label != null) return false;
        if (children != null ? !children.equals(storyTree.children) : storyTree.children != null) return false;
        if (id != null ? !id.equals(storyTree.id) : storyTree.id != null) return false;
        if (parentId != null ? !parentId.equals(storyTree.parentId) : storyTree.parentId != null) return false;
        if (storyId != null ? !storyId.equals(storyTree.storyId) : storyTree.storyId != null) return false;
        if (additionalProperties != null ? !additionalProperties.equals(storyTree.additionalProperties) : storyTree.additionalProperties != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = isFolder != null ? isFolder.hashCode() : 0;
        result = 31 * result + (label != null ? label.hashCode() : 0);
        result = 31 * result + (children != null ? children.hashCode() : 0);
        result = 31 * result + (id != null ? id.hashCode() : 0);
        result = 31 * result + (parentId != null ? parentId.hashCode() : 0);
        result = 31 * result + (storyId != null ? storyId.hashCode() : 0);
        result = 31 * result + (additionalProperties != null ? additionalProperties.hashCode() : 0);
        return result;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
