package Helpers.Classes;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Generated;
import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
        "id",
        "storyId",
        "name",
        "description",
        "tags",
        "fileId",
        "uniqueTag",
        "isLinked",
        "key"
})
public class Story {

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("storyId")
    private Integer storyId;
    @JsonProperty("name")
    private String name;
    @JsonProperty("description")
    private String description;
    @JsonProperty("tags")
    private List<String> tags = new ArrayList<String>();
    @JsonProperty("fileId")
    private Integer fileId;
    @JsonProperty("uniqueTag")
    private String uniqueTag;
    @JsonProperty("isLinked")
    private Boolean isLinked;
    @JsonProperty("key")
    private Object key;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     *
     * @return
     * The id
     */
    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The storyId
     */
    @JsonProperty("storyId")
    public Integer getStoryId() {
        return storyId;
    }

    /**
     *
     * @param storyId
     * The storyId
     */
    @JsonProperty("storyId")
    public void setStoryId(Integer storyId) {
        this.storyId = storyId;
    }

    /**
     *
     * @return
     * The name
     */
    @JsonProperty("name")
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     * The description
     */
    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    /**
     *
     * @param description
     * The description
     */
    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     *
     * @return
     * The tags
     */
    @JsonProperty("tags")
    public List<String> getTags() {
        return tags;
    }

    /**
     *
     * @param tags
     * The tags
     */
    @JsonProperty("tags")
    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    /**
     *
     * @return
     * The fileId
     */
    @JsonProperty("fileId")
    public Integer getFileId() {
        return fileId;
    }

    /**
     *
     * @param fileId
     * The fileId
     */
    @JsonProperty("fileId")
    public void setFileId(Integer fileId) {
        this.fileId = fileId;
    }

    /**
     *
     * @return
     * The uniqueTag
     */
    @JsonProperty("uniqueTag")
    public String getUniqueTag() {
        return uniqueTag;
    }

    /**
     *
     * @param uniqueTag
     * The uniqueTag
     */
    @JsonProperty("uniqueTag")
    public void setUniqueTag(String uniqueTag) {
        this.uniqueTag = uniqueTag;
    }

    /**
     *
     * @return
     * The isLinked
     */
    @JsonProperty("isLinked")
    public Boolean getIsLinked() {
        return isLinked;
    }

    /**
     *
     * @param isLinked
     * The isLinked
     */
    @JsonProperty("isLinked")
    public void setIsLinked(Boolean isLinked) {
        this.isLinked = isLinked;
    }

    /**
     *
     * @return
     * The key
     */
    @JsonProperty("key")
    public Object getKey() {
        return key;
    }

    /**
     *
     * @param key
     * The key
     */
    @JsonProperty("key")
    public void setKey(Object key) {
        if (key == null) {
            key = 0;
        }
        this.key = key;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Story story = (Story) o;

        if (id != null ? !id.equals(story.id) : story.id != null) return false;
        if (storyId != null ? !storyId.equals(story.storyId) : story.storyId != null) return false;
        if (name != null ? !name.equals(story.name) : story.name != null) return false;
        if (description != null ? !description.equals(story.description) : story.description != null) return false;
        if (tags != null ? !tags.equals(story.tags) : story.tags != null) return false;
        if (fileId != null ? !fileId.equals(story.fileId) : story.fileId != null) return false;
        if (uniqueTag != null ? !uniqueTag.equals(story.uniqueTag) : story.uniqueTag != null) return false;
        if (isLinked != null ? !isLinked.equals(story.isLinked) : story.isLinked != null) return false;
        if (key != null ? !key.equals(story.key) : story.key != null) return false;
        if (additionalProperties != null ? !additionalProperties.equals(story.additionalProperties) : story.additionalProperties != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (storyId != null ? storyId.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (tags != null ? tags.hashCode() : 0);
        result = 31 * result + (fileId != null ? fileId.hashCode() : 0);
        result = 31 * result + (uniqueTag != null ? uniqueTag.hashCode() : 0);
        result = 31 * result + (isLinked != null ? isLinked.hashCode() : 0);
        result = 31 * result + (key != null ? key.hashCode() : 0);
        result = 31 * result + (additionalProperties != null ? additionalProperties.hashCode() : 0);
        return result;
    }
}
