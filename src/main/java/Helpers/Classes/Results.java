package Helpers.Classes;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Generated;
import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
        "resultsCount",
        "pagesCount",
        "results"
})
public class Results {

    @JsonProperty("resultsCount")
    private Integer resultsCount;
    @JsonProperty("pagesCount")
    private Integer pagesCount;
    @JsonProperty("results")
    private List<Result> results = new ArrayList<Result>();
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     *
     * @return
     * The resultsCount
     */
    @JsonProperty("resultsCount")
    public Integer getResultsCount() {
        return resultsCount;
    }

    /**
     *
     * @param resultsCount
     * The resultsCount
     */
    @JsonProperty("resultsCount")
    public void setResultsCount(Integer resultsCount) {
        this.resultsCount = resultsCount;
    }

    /**
     *
     * @return
     * The pagesCount
     */
    @JsonProperty("pagesCount")
    public Integer getPagesCount() {
        return pagesCount;
    }

    /**
     *
     * @param pagesCount
     * The pagesCount
     */
    @JsonProperty("pagesCount")
    public void setPagesCount(Integer pagesCount) {
        this.pagesCount = pagesCount;
    }

    /**
     *
     * @return
     * The results
     */
    @JsonProperty("results")
    public List<Result> getResults() {
        return results;
    }

    /**
     *
     * @param results
     * The results
     */
    @JsonProperty("results")
    public void setResults(List<Result> results) {
        this.results = results;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}