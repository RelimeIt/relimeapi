package Helpers.Classes;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "name",
        "key",
        "managementToolId",
        "fileName",
        "description",
        "tags",
        "parentFolderId",
        "parsedScenarios",
        "isJiraLinkedStory"
})
public class JiraStory {

    @JsonProperty("name")
    private String name;
    @JsonProperty("key")
    private Object key;
    @JsonProperty("managementToolId")
    private Integer managementToolId;
    @JsonProperty("fileName")
    private String fileName;
    @JsonProperty("description")
    private String description;
    @JsonProperty("tags")
    private List<Object> tags = null;
    @JsonProperty("parentFolderId")
    private Object parentFolderId;
    @JsonProperty("parsedScenarios")
    private List<ParsedScenario> parsedScenarios = null;
    @JsonProperty("isJiraLinkedStory")
    private Boolean isJiraLinkedStory;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("key")
    public Object getKey() {
        return key;
    }

    @JsonProperty("key")
    public void setKey(Object key) {
        this.key = key;
    }

    @JsonProperty("managementToolId")
    public Integer getManagementToolId() {
        return managementToolId;
    }

    @JsonProperty("managementToolId")
    public void setManagementToolId(Integer managementToolId) {
        this.managementToolId = managementToolId;
    }

    @JsonProperty("fileName")
    public String getFileName() {
        return fileName;
    }

    @JsonProperty("fileName")
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    @JsonProperty("tags")
    public List<Object> getTags() {
        return tags;
    }

    @JsonProperty("tags")
    public void setTags(List<Object> tags) {
        this.tags = tags;
    }

    @JsonProperty("parentFolderId")
    public Object getParentFolderId() {
        return parentFolderId;
    }

    @JsonProperty("parentFolderId")
    public void setParentFolderId(Object parentFolderId) {
        if(parentFolderId == null) {
            this.parentFolderId = 0;
        } else {
            this.parentFolderId = parentFolderId;
        }
    }

    @JsonProperty("parsedScenarios")
    public List<ParsedScenario> getParsedScenarios() {
        return parsedScenarios;
    }

    @JsonProperty("parsedScenarios")
    public void setParsedScenarios(List<ParsedScenario> parsedScenarios) {
        this.parsedScenarios = parsedScenarios;
    }

    @JsonProperty("isJiraLinkedStory")
    public Boolean getIsJiraLinkedStory() {
        return isJiraLinkedStory;
    }

    @JsonProperty("isJiraLinkedStory")
    public void setIsJiraLinkedStory(Boolean isJiraLinkedStory) {
        this.isJiraLinkedStory = isJiraLinkedStory;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        JiraStory jiraStory = (JiraStory) o;

        if (name != null ? !name.equals(jiraStory.name) : jiraStory.name != null) return false;
        if (key != null ? !key.equals(jiraStory.key) : jiraStory.key != null) return false;
        if (managementToolId != null ? !managementToolId.equals(jiraStory.managementToolId) : jiraStory.managementToolId != null)
            return false;
        if (fileName != null ? !fileName.equals(jiraStory.fileName) : jiraStory.fileName != null) return false;
        if (description != null ? !description.equals(jiraStory.description) : jiraStory.description != null)
            return false;
        if (tags != null ? !tags.equals(jiraStory.tags) : jiraStory.tags != null) return false;
        if (parentFolderId != null ? !parentFolderId.equals(jiraStory.parentFolderId) : jiraStory.parentFolderId != null)
            return false;
        if (parsedScenarios != null ? !parsedScenarios.equals(jiraStory.parsedScenarios) : jiraStory.parsedScenarios != null)
            return false;
        if (isJiraLinkedStory != null ? !isJiraLinkedStory.equals(jiraStory.isJiraLinkedStory) : jiraStory.isJiraLinkedStory != null)
            return false;
        return additionalProperties != null ? additionalProperties.equals(jiraStory.additionalProperties) : jiraStory.additionalProperties == null;

    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (key != null ? key.hashCode() : 0);
        result = 31 * result + (managementToolId != null ? managementToolId.hashCode() : 0);
        result = 31 * result + (fileName != null ? fileName.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (tags != null ? tags.hashCode() : 0);
        result = 31 * result + (parentFolderId != null ? parentFolderId.hashCode() : 0);
        result = 31 * result + (parsedScenarios != null ? parsedScenarios.hashCode() : 0);
        result = 31 * result + (isJiraLinkedStory != null ? isJiraLinkedStory.hashCode() : 0);
        result = 31 * result + (additionalProperties != null ? additionalProperties.hashCode() : 0);
        return result;
    }
}
