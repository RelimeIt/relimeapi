package Helpers.Classes;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;

import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
        "id",
        "name",
        "description",
        "uniqueTag",
        "type",
        "isBriefInfo",
        "gitSynced",
        "ttsSynced"
})
public class ScenarioCreateResponse {

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("name")
    private String name;
    @JsonProperty("description")
    private String description;
    @JsonProperty("uniqueTag")
    private String uniqueTag;
    @JsonProperty("type")
    private String type;
    @JsonProperty("isBriefInfo")
    private Boolean isBriefInfo;
    @JsonProperty("gitSynced")
    private Object gitSynced;
    @JsonProperty("ttsSynced")
    private Object ttsSynced;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * @return The id
     */
    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    /**
     * @param id The id
     */
    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return The name
     */
    @JsonProperty("name")
    public String getName() {
        return name;
    }

    /**
     * @param name The name
     */
    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return The description
     */
    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    /**
     * @param description The description
     */
    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return The uniqueTag
     */
    @JsonProperty("uniqueTag")
    public String getUniqueTag() {
        return uniqueTag;
    }

    /**
     * @param uniqueTag The uniqueTag
     */
    @JsonProperty("uniqueTag")
    public void setUniqueTag(String uniqueTag) {
        this.uniqueTag = uniqueTag;
    }

    /**
     * @return The type
     */
    @JsonProperty("type")
    public String getType() {
        return type;
    }

    /**
     * @param type The type
     */
    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return The isBriefInfo
     */
    @JsonProperty("isBriefInfo")
    public Boolean getIsBriefInfo() {
        return isBriefInfo;
    }

    /**
     * @param isBriefInfo The isBriefInfo
     */
    @JsonProperty("isBriefInfo")
    public void setIsBriefInfo(Boolean isBriefInfo) {
        this.isBriefInfo = isBriefInfo;
    }

    /**
     * @return The gitSynced
     */
    @JsonProperty("gitSynced")
    public Object getGitSynced() {
        return gitSynced;
    }

    /**
     * @param gitSynced The gitSynced
     */
    @JsonProperty("gitSynced")
    public void setGitSynced(Object gitSynced) {
        if (gitSynced == null) {
            gitSynced = 0;
        }
        this.gitSynced = gitSynced;
    }

    /**
     * @return The ttsSynced
     */
    @JsonProperty("ttsSynced")
    public Object getTtsSynced() {
        return ttsSynced;
    }

    /**
     * @param ttsSynced The ttsSynced
     */
    @JsonProperty("ttsSynced")
    public void setTtsSynced(Object ttsSynced) {
        if (ttsSynced == null) {
            ttsSynced = 0;
        }
        this.ttsSynced = ttsSynced;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ScenarioCreateResponse that = (ScenarioCreateResponse) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (description != null ? !description.equals(that.description) : that.description != null) return false;
        if (uniqueTag != null ? !uniqueTag.equals(that.uniqueTag) : that.uniqueTag != null) return false;
        if (type != null ? !type.equals(that.type) : that.type != null) return false;
        if (isBriefInfo != null ? !isBriefInfo.equals(that.isBriefInfo) : that.isBriefInfo != null) return false;
        if (gitSynced != null ? !gitSynced.equals(that.gitSynced) : that.gitSynced != null) return false;
        if (ttsSynced != null ? !ttsSynced.equals(that.ttsSynced) : that.ttsSynced != null) return false;
        if (additionalProperties != null ? !additionalProperties.equals(that.additionalProperties) : that.additionalProperties != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (uniqueTag != null ? uniqueTag.hashCode() : 0);
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + (isBriefInfo != null ? isBriefInfo.hashCode() : 0);
        result = 31 * result + (gitSynced != null ? gitSynced.hashCode() : 0);
        result = 31 * result + (ttsSynced != null ? ttsSynced.hashCode() : 0);
        result = 31 * result + (additionalProperties != null ? additionalProperties.hashCode() : 0);
        return result;
    }

    //    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (o == null || getClass() != o.getClass()) return false;
//
//        ScenarioCreateResponse that = (ScenarioCreateResponse) o;
//
//        if (id != null ? !id.equals(that.id) : that.id != null) return false;
//        if (name != null ? !name.equals(that.name) : that.name != null) return false;
//        if (description != null ? !description.equals(that.description) : that.description != null) return false;
//        if (uniqueTag != null ? !uniqueTag.equals(that.uniqueTag) : that.uniqueTag != null) return false;
//        if (type != null ? !type.equals(that.type) : that.type != null) return false;
//        if (isBriefInfo != null ? !isBriefInfo.equals(that.isBriefInfo) : that.isBriefInfo != null) return false;
//        if (gitSynced != null ? !gitSynced.equals(that.gitSynced) : that.gitSynced != null) return false;
//        if (ttsSynced != null ? !ttsSynced.equals(that.ttsSynced) : that.ttsSynced != null) return false;
//        if (additionalProperties != null ? !additionalProperties.equals(that.additionalProperties) : that.additionalProperties != null)
//            return false;
//
//        return true;
//    }
//
//    @Override
//    public int hashCode() {
//        int result = id != null ? id.hashCode() : 0;
//        result = 31 * result + (name != null ? name.hashCode() : 0);
//        result = 31 * result + (description != null ? description.hashCode() : 0);
//        result = 31 * result + (uniqueTag != null ? uniqueTag.hashCode() : 0);
//        result = 31 * result + (type != null ? type.hashCode() : 0);
//        result = 31 * result + (isBriefInfo != null ? isBriefInfo.hashCode() : 0);
//        result = 31 * result + (gitSynced != null ? gitSynced.hashCode() : 0);
//        result = 31 * result + (ttsSynced != null ? ttsSynced.hashCode() : 0);
//        result = 31 * result + (additionalProperties != null ? additionalProperties.hashCode() : 0);
//        return result;
//    }

}
