package Helpers.Classes;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Generated;
import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
        "domains",
        "totalCount"
})
public class Domains {

    @JsonProperty("domains")
    private List<Domain> domains = new ArrayList<Domain>();
    @JsonProperty("totalCount")
    private Integer totalCount;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     *
     * @return
     * The domains
     */
    @JsonProperty("domains")
    public List<Domain> getDomains() {
        return domains;
    }

    /**
     *
     * @param domains
     * The domains
     */
    @JsonProperty("domains")
    public void setDomains(List<Domain> domains) {
        this.domains = domains;
    }

    /**
     *
     * @return
     * The totalCount
     */
    @JsonProperty("totalCount")
    public Integer getTotalCount() {
        return totalCount;
    }

    /**
     *
     * @param totalCount
     * The totalCount
     */
    @JsonProperty("totalCount")
    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Domains domains1 = (Domains) o;

        if (domains != null ? !domains.equals(domains1.domains) : domains1.domains != null) return false;
        if (totalCount != null ? !totalCount.equals(domains1.totalCount) : domains1.totalCount != null) return false;
        if (additionalProperties != null ? !additionalProperties.equals(domains1.additionalProperties) : domains1.additionalProperties != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = domains != null ? domains.hashCode() : 0;
        result = 31 * result + (totalCount != null ? totalCount.hashCode() : 0);
        result = 31 * result + (additionalProperties != null ? additionalProperties.hashCode() : 0);
        return result;
    }
}