package Helpers.Classes;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Generated;
import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
        "Description",
        "Tags",
        "StoryId",
        "ExampleTable",
        "StepText",
        "type",
        "Name",
        "IsBriefInfo"
})
public class ParsedScenario {

    /*
    !!!!!!!!!!!!!!!!!!!!!!
    UNUSED CLASS
    !!!!!!!!!!!!!!!!!!!!!!
     */

    @JsonProperty("Description")
    private String description;
    @JsonProperty("Tags")
    private List<String> tags = new ArrayList<String>();
    @JsonProperty("StoryId")
    private Integer storyId;
    @JsonProperty("ExampleTable")
    private ExampleTable exampleTable;
    @JsonProperty("StepText")
    private List<String> stepText = new ArrayList<String>();
    @JsonProperty("type")
    private String type;
    @JsonProperty("Name")
    private String name;
    @JsonProperty("IsBriefInfo")
    private Boolean isBriefInfo;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     *
     * @return
     * The description
     */
    @JsonProperty("Description")
    public String getDescription() {
        return description;
    }

    /**
     *
     * @param description
     * The Description
     */
    @JsonProperty("Description")
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     *
     * @return
     * The tags
     */
    @JsonProperty("Tags")
    public List<String> getTags() {
        return tags;
    }

    /**
     *
     * @param tags
     * The Tags
     */
    @JsonProperty("Tags")
    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    /**
     *
     * @return
     * The storyId
     */
    @JsonProperty("StoryId")
    public Integer getStoryId() {
        return storyId;
    }

    /**
     *
     * @param storyId
     * The StoryId
     */
    @JsonProperty("StoryId")
    public void setStoryId(Integer storyId) {
        this.storyId = storyId;
    }

    /**
     *
     * @return
     * The exampleTable
     */
    @JsonProperty("ExampleTable")
    public ExampleTable getExampleTable() {
        return exampleTable;
    }

    /**
     *
     * @param exampleTable
     * The ExampleTable
     */
    @JsonProperty("ExampleTable")
    public void setExampleTable(ExampleTable exampleTable) {
        this.exampleTable = exampleTable;
    }

    /**
     *
     * @return
     * The stepText
     */
    @JsonProperty("StepText")
    public List<String> getStepText() {
        return stepText;
    }

    /**
     *
     * @param stepText
     * The StepText
     */
    @JsonProperty("StepText")
    public void setStepText(List<String> stepText) {
        this.stepText = stepText;
    }

    /**
     *
     * @return
     * The type
     */
    @JsonProperty("type")
    public String getType() {
        return type;
    }

    /**
     *
     * @param type
     * The type
     */
    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    /**
     *
     * @return
     * The name
     */
    @JsonProperty("Name")
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The Name
     */
    @JsonProperty("Name")
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     * The isBriefInfo
     */
    @JsonProperty("IsBriefInfo")
    public Boolean getIsBriefInfo() {
        return isBriefInfo;
    }

    /**
     *
     * @param isBriefInfo
     * The IsBriefInfo
     */
    @JsonProperty("IsBriefInfo")
    public void setIsBriefInfo(Boolean isBriefInfo) {
        this.isBriefInfo = isBriefInfo;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}