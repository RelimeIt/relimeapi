package Helpers.Classes;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
        "id",
        "vcsType",
        "url",
        "belongToAccount",
        "isRepositoryAccesible",
        "isValid",
        "errorMessage"
})
public class Vcs {

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("vcsType")
    private String vcsType;
    @JsonProperty("url")
    private String url;
    @JsonProperty("belongToAccount")
    private Integer belongToAccount;
    @JsonProperty("isRepositoryAccesible")
    private Boolean isRepositoryAccesible;
    @JsonProperty("isValid")
    private Boolean isValid;
    @JsonProperty("errorMessage")
    private String errorMessage;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     *
     * @return
     * The id
     */
    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The vcsType
     */
    @JsonProperty("vcsType")
    public String getVcsType() {
        return vcsType;
    }

    /**
     *
     * @param vcsType
     * The vcsType
     */
    @JsonProperty("vcsType")
    public void setVcsType(String vcsType) {
        this.vcsType = vcsType;
    }

    /**
     *
     * @return
     * The url
     */
    @JsonProperty("url")
    public String getUrl() {
        return url;
    }

    /**
     *
     * @param url
     * The url
     */
    @JsonProperty("url")
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     *
     * @return
     * The belongToAccount
     */
    @JsonProperty("belongToAccount")
    public Integer getBelongToAccount() {
        return belongToAccount;
    }

    /**
     *
     * @param belongToAccount
     * The belongToAccount
     */
    @JsonProperty("belongToAccount")
    public void setBelongToAccount(Integer belongToAccount) {
        this.belongToAccount = belongToAccount;
    }

    /**
     *
     * @return
     * The isRepositoryAccesible
     */
    @JsonProperty("isRepositoryAccesible")
    public Boolean getIsRepositoryAccesible() {
        return isRepositoryAccesible;
    }

    /**
     *
     * @param isRepositoryAccesible
     * The isRepositoryAccesible
     */
    @JsonProperty("isRepositoryAccesible")
    public void setIsRepositoryAccesible(Boolean isRepositoryAccesible) {
        this.isRepositoryAccesible = isRepositoryAccesible;
    }

    /**
     *
     * @return
     * The isValid
     */
    @JsonProperty("isValid")
    public Boolean getIsValid() {
        return isValid;
    }

    /**
     *
     * @param isValid
     * The isValid
     */
    @JsonProperty("isValid")
    public void setIsValid(Boolean isValid) {
        this.isValid = isValid;
    }

    /**
     *
     * @return
     * The errorMessage
     */
    @JsonProperty("errorMessage")
    public String getErrorMessage() {
        return errorMessage;
    }

    /**
     *
     * @param errorMessage
     * The errorMessage
     */
    @JsonProperty("errorMessage")
    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Vcs vcs = (Vcs) o;

        if (id != null ? !id.equals(vcs.id) : vcs.id != null) return false;
        if (vcsType != null ? !vcsType.equals(vcs.vcsType) : vcs.vcsType != null) return false;
        if (url != null ? !url.equals(vcs.url) : vcs.url != null) return false;
        if (belongToAccount != null ? !belongToAccount.equals(vcs.belongToAccount) : vcs.belongToAccount != null)
            return false;
        if (isRepositoryAccesible != null ? !isRepositoryAccesible.equals(vcs.isRepositoryAccesible) : vcs.isRepositoryAccesible != null)
            return false;
        if (isValid != null ? !isValid.equals(vcs.isValid) : vcs.isValid != null) return false;
        if (errorMessage != null ? !errorMessage.equals(vcs.errorMessage) : vcs.errorMessage != null) return false;
        if (additionalProperties != null ? !additionalProperties.equals(vcs.additionalProperties) : vcs.additionalProperties != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (vcsType != null ? vcsType.hashCode() : 0);
        result = 31 * result + (url != null ? url.hashCode() : 0);
        result = 31 * result + (belongToAccount != null ? belongToAccount.hashCode() : 0);
        result = 31 * result + (isRepositoryAccesible != null ? isRepositoryAccesible.hashCode() : 0);
        result = 31 * result + (isValid != null ? isValid.hashCode() : 0);
        result = 31 * result + (errorMessage != null ? errorMessage.hashCode() : 0);
        result = 31 * result + (additionalProperties != null ? additionalProperties.hashCode() : 0);
        return result;
    }
}
