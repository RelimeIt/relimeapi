package Helpers.Classes;

import java.util.HashMap;
import java.util.Map;
import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "id",
        "name",
        "url",
        "belongsToAccount",
        "isAccessible"
})
public class TaskTracker {

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("name")
    private String name;
    @JsonProperty("url")
    private String url;
    @JsonProperty("belongsToAccount")
    private Integer belongsToAccount;
    @JsonProperty("isAccessible")
    private Boolean isAccessible;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     *
     * @return
     * The id
     */
    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The name
     */
    @JsonProperty("name")
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     * The url
     */
    @JsonProperty("url")
    public String getUrl() {
        return url;
    }

    /**
     *
     * @param url
     * The url
     */
    @JsonProperty("url")
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     *
     * @return
     * The belongsToAccount
     */
    @JsonProperty("belongsToAccount")
    public Integer getBelongsToAccount() {
        return belongsToAccount;
    }

    /**
     *
     * @param belongsToAccount
     * The belongsToAccount
     */
    @JsonProperty("belongsToAccount")
    public void setBelongsToAccount(Integer belongsToAccount) {
        this.belongsToAccount = belongsToAccount;
    }

    /**
     *
     * @return
     * The isAccessible
     */
    @JsonProperty("isAccessible")
    public Boolean getIsAccessible() {
        return isAccessible;
    }

    /**
     *
     * @param isAccessible
     * The isAccessible
     */
    @JsonProperty("isAccessible")
    public void setIsAccessible(Boolean isAccessible) {
        this.isAccessible = isAccessible;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
