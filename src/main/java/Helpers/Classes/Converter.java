package Helpers.Classes;

import org.codehaus.jackson.map.ObjectMapper;
import java.io.File;
import java.io.IOException;

public class Converter {

    private final static String baseFile = "user.json";

    public String objectToJson(Object obj) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        String jsonObject = mapper.writeValueAsString(obj);
        return jsonObject;
    }

 /*   // update the return type
    public StorySample toJavaObject() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(new File(baseFile), StorySample.class);
    }
*/
}
