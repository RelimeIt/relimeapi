package Helpers.Classes;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import javax.annotation.Generated;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mukhin on 11/23/2016.
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
        "accountsResults"
})
public class RetrieveAccounts {
    @JsonProperty("accountsResults")
    private List<RetrieveAccount> accountsResults = new ArrayList<RetrieveAccount>();

    /**
     *
     * @return
     * The accountArray
     */
    @JsonProperty("accountsResults")
    public List<RetrieveAccount> getAccountsResults() {
        return accountsResults;
    }

    /**
     *
     * @param accountArray
     * The accountArray
     */
    @JsonProperty("accountsResults")
    public void setAccountsResults(List<RetrieveAccount> accountArray) {
        this.accountsResults = accountArray;
    }
}
