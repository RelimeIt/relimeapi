package Helpers.Classes;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Generated;
import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
        "StoryId",
        "type",
        "id",
        "name",
        "isBriefInfo",
        "scenarioType",
        "description",
        "tags",
        "stepText",
        "exampleTable"
})
public class ScenarioSample {
    @JsonProperty("id")
    private Integer id;
    @JsonProperty("name")
    private String name;
    @JsonProperty("isBriefInfo")
    private Boolean isBriefInfo;
    @JsonProperty("scenarioType")
    private String scenarioType;
    @JsonProperty("description")
    private String description;
    @JsonProperty("tags")
    private List<String> tags = null;
    @JsonProperty("stepText")
    private List<String> stepText = null;
    @JsonProperty("exampleTable")
    private ExampleTable exampleTable;
    @JsonProperty("StoryId")
    private Integer storyId;
    @JsonProperty("type")
    private String type;

    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     *
     * @return
     * The storyId
     */
    @JsonProperty("StoryId")
    public Integer getStoryId() {
        return storyId;
    }

    /**
     *
     * @param storyId
     * The StoryId
     */
    @JsonProperty("StoryId")
    public void setStoryId(Integer storyId) {
        this.storyId = storyId;
    }

    /**
     *
     * @return
     * The type
     */
    @JsonProperty("type")
    public String getType() {
        return type;
    }

    /**
     *
     * @param type
     * The type
     */
    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }


    /**
     *
     * @return
     * The id
     */
    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The name
     */
    @JsonProperty("name")
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     * The isBriefInfo
     */
    @JsonProperty("isBriefInfo")
    public Boolean getIsBriefInfo() {
        return isBriefInfo;
    }

    /**
     *
     * @param isBriefInfo
     * The isBriefInfo
     */
    @JsonProperty("isBriefInfo")
    public void setIsBriefInfo(Boolean isBriefInfo) {
        this.isBriefInfo = isBriefInfo;
    }

    /**
     *
     * @return
     * The scenarioType
     */
    @JsonProperty("scenarioType")
    public String getScenarioType() {
        return scenarioType;
    }

    /**
     *
     * @param scenarioType
     * The scenarioType
     */
    @JsonProperty("scenarioType")
    public void setScenarioType(String scenarioType) {
        this.scenarioType = scenarioType;
    }

    /**
     *
     * @return
     * The description
     */
    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    /**
     *
     * @param description
     * The description
     */
    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     *
     * @return
     * The tags
     */
    @JsonProperty("tags")
    public List<String> getTags() {
        return tags;
    }

    /**
     *
     * @param tags
     * The tags
     */
    @JsonProperty("tags")
    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    /**
     *
     * @return
     * The stepText
     */
    @JsonProperty("stepText")
    public List<String> getStepText() {
        return stepText;
    }

    /**
     *
     * @param stepText
     * The stepText
     */
    @JsonProperty("stepText")
    public void setStepText(List<String> stepText) {
        this.stepText = stepText;
    }

    /**
     *
     * @return
     * The exampleTable
     */
    @JsonProperty("exampleTable")
    public ExampleTable getExampleTable() {
        return exampleTable;
    }

    /**
     *
     * @param exampleTable
     * The exampleTable
     */
    @JsonProperty("exampleTable")
    public void setExampleTable(ExampleTable exampleTable) {
        this.exampleTable = exampleTable;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
}

