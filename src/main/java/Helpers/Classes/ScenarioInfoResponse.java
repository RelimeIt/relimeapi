package Helpers.Classes;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "name",
        "description",
        "tags",
        "stepText",
        "exampleTable",
        "isBriefInfo"
})
public class ScenarioInfoResponse {

    @JsonProperty("name")
    private String name;
    @JsonProperty("description")
    private String description;
    @JsonProperty("tags")
    private List<String> tags = null;
    @JsonProperty("stepText")
    private List<String> stepText = null;
    @JsonProperty("exampleTable")
    private ExampleTable exampleTable;
    @JsonProperty("isBriefInfo")
    private Boolean isBriefInfo;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     *
     * @return
     * The name
     */
    @JsonProperty("name")
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     * The description
     */
    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    /**
     *
     * @param description
     * The description
     */
    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     *
     * @return
     * The tags
     */
    @JsonProperty("tags")
    public List<String> getTags() {
        return tags;
    }

    /**
     *
     * @param tags
     * The tags
     */
    @JsonProperty("tags")
    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    /**
     *
     * @return
     * The stepText
     */
    @JsonProperty("stepText")
    public List<String> getStepText() {
        return stepText;
    }

    /**
     *
     * @param stepText
     * The stepText
     */
    @JsonProperty("stepText")
    public void setStepText(List<String> stepText) {
        this.stepText = stepText;
    }

    /**
     *
     * @return
     * The exampleTable
     */
    @JsonProperty("exampleTable")
    public ExampleTable getExampleTable() {
        return exampleTable;
    }

    /**
     *
     * @param exampleTable
     * The exampleTable
     */
    @JsonProperty("exampleTable")
    public void setExampleTable(ExampleTable exampleTable) {
        this.exampleTable = exampleTable;
    }

    /**
     *
     * @return
     * The isBriefInfo
     */
    @JsonProperty("isBriefInfo")
    public Boolean getIsBriefInfo() {
        return isBriefInfo;
    }

    /**
     *
     * @param isBriefInfo
     * The isBriefInfo
     */
    @JsonProperty("isBriefInfo")
    public void setIsBriefInfo(Boolean isBriefInfo) {
        this.isBriefInfo = isBriefInfo;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ScenarioInfoResponse that = (ScenarioInfoResponse) o;

        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (description != null ? !description.equals(that.description) : that.description != null) return false;
        if (tags != null ? !tags.equals(that.tags) : that.tags != null) return false;
        if (stepText != null ? !stepText.equals(that.stepText) : that.stepText != null) return false;
        if (exampleTable != null ? !exampleTable.equals(that.exampleTable) : that.exampleTable != null) return false;
        if (isBriefInfo != null ? !isBriefInfo.equals(that.isBriefInfo) : that.isBriefInfo != null) return false;
        return additionalProperties != null ? additionalProperties.equals(that.additionalProperties) : that.additionalProperties == null;

    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (tags != null ? tags.hashCode() : 0);
        result = 31 * result + (stepText != null ? stepText.hashCode() : 0);
        result = 31 * result + (exampleTable != null ? exampleTable.hashCode() : 0);
        result = 31 * result + (isBriefInfo != null ? isBriefInfo.hashCode() : 0);
        result = 31 * result + (additionalProperties != null ? additionalProperties.hashCode() : 0);
        return result;
    }
}
