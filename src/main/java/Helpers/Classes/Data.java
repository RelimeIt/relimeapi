package Helpers.Classes;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;

import org.codehaus.jackson.annotate.*;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
        "arg1",
        "arg2"
})
public class Data {
    @JsonProperty("arg1")
    private String arg1;
    @JsonProperty("arg2")
    private String arg2;

    /**
     *
     * @return
     * The arg1
     */
    @JsonProperty("arg1")
    public String getArg1() {
        return arg1;
    }

    /**
     *
     * @param arg1
     * The arg1
     */
    @JsonProperty("arg1")
    public void setArg1(String arg1) {
        this.arg1 = arg1;
    }

    /**
     *
     * @return
     * The arg2
     */
    @JsonProperty("arg2")
    public String getArg2() {
        return arg2;
    }

    /**
     *
     * @param arg2
     * The arg2
     */
    @JsonProperty("arg2")
    public void setArg2(String arg2) {
        this.arg2 = arg2;
    }

    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Data data = (Data) o;

        if (arg1 != null ? !arg1.equals(data.arg1) : data.arg1 != null) return false;
        if (arg2 != null ? !arg2.equals(data.arg2) : data.arg2 != null) return false;
        if (additionalProperties != null ? !additionalProperties.equals(data.additionalProperties) : data.additionalProperties != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = arg1 != null ? arg1.hashCode() : 0;
        result = 31 * result + (arg2 != null ? arg2.hashCode() : 0);
        result = 31 * result + (additionalProperties != null ? additionalProperties.hashCode() : 0);
        return result;
    }
}
