package Common;

import Clients.*;
import Helpers.Classes.*;
import arp.FlowReport;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.*;
import java.security.SecureRandom;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static Common.SessionAuthorizationInfo.*;
import static Database.RelimeDBConnector.executeQueryToRelimeBase;
import static Helpers.ConfigurationInfo.*;

public class Methods {

    private String sendHTTPRequest(String requestType, String apiURL, String content, String parameters, String additionalParameter) {
        HttpPost post;
        HttpPut put;
        HttpGet get;
        HttpDelete delete;
        HttpResponse response = null;
        StringEntity params;
        try {
            HttpClient client = HttpClientBuilder.create().build();
            String url = apiURL;
            if (parameters != null) {
                url += "?" + parameters;
            }
            switch (requestType.toUpperCase()) {
                case "GET":
                    get = new HttpGet(url);
                    get.setHeader("Content-Type", "application/json");
                    get.addHeader("Authorization", "bearer " + token);
                    if (additionalParameter != null) {
                        get.addHeader("X-ProjectKey", additionalParameter);
                    }
                    response = client.execute(get);
                    break;
                case "POST":
                    post = new HttpPost(apiURL);
                    post.setHeader("Content-Type", "application/json");
                    post.addHeader("Authorization", "bearer " + token);
                    if (additionalParameter != null) {
                        post.addHeader("X-ProjectKey", additionalParameter);
                    }
                    if (content != null) {
                        params = new StringEntity(content);
                        post.setEntity(params);
                    }
                    response = client.execute(post);
                    break;
                case "PUT":
                    put = new HttpPut(apiURL);
                    put.setHeader("Content-Type", "application/json");
                    put.addHeader("Authorization", "bearer " + token);
                    if (additionalParameter != null) {
                        put.addHeader("X-ProjectKey", additionalParameter);
                    }
                    params = new StringEntity(content);
                    put.setEntity(params);
                    response = client.execute(put);
                    break;
                case "DELETE":
                    delete = new HttpDelete(url);
                    delete.setHeader("Content-Type", "application/json");
                    delete.addHeader("Authorization", "bearer " + token);
                    if (additionalParameter != null) {
                        delete.addHeader("X-ProjectKey", additionalParameter);
                    }
                    response = client.execute(delete);
                    break;

            }
            if (response.getStatusLine().getStatusCode() == 200) {
                if (response.getHeaders("Set-Cookie").length != 0) {
                    Header[] headers = response.getAllHeaders();
                    String str = headers[5].getValue();
                    sanboxToken = str.substring(str.indexOf("=")+1, str.indexOf(";"));
                }
                BufferedReader rd = new BufferedReader(
                        new InputStreamReader(response.getEntity().getContent()));
                StringBuffer result = new StringBuffer();
                String line = "";
                while ((line = rd.readLine()) != null) {
                    result.append(line);
                }
                return result.toString();
            } else {
                return String.valueOf(response.getStatusLine().getStatusCode());
            }
        } catch (Exception e) {
            return e.getMessage();
        }
    }

    public String getAuthorizationInfo() {
        try {

            StringBuilder sb = new StringBuilder();
            sb.append("{\"Email\":\"").append(LOGIN).append("\",").append("\"Password\":\"").append(PASSWORD).append("\"}");
            String content = sb.toString();
            String response = getPOSTResponse(URL + "api/auth/login", content, null);
            return returnTokenAndSetStaticVariables(response);

        } catch (Exception e) {
            return e.getMessage();
        }

    }

    public String logOut() {
        try {
            FlowReport.nextStep();
            FlowReport.addStep("Trying to logout", "");
            HttpClient client = HttpClientBuilder.create().build();
            HttpPost post = new HttpPost("http://relime.ga/api/auth/logout");
            post.setHeader("Content-Type", "application/json");
            post.addHeader("Authorization", "bearer " + token);
            HttpResponse response = client.execute(post);

            if (response.getStatusLine().getStatusCode() == 200) {
                FlowReport.reportAction("Logout is successful", true);
                token = null;
                BufferedReader rd = new BufferedReader(
                        new InputStreamReader(response.getEntity().getContent()));
                StringBuffer result = new StringBuffer();
                String line = "";
                while ((line = rd.readLine()) != null) {
                    result.append(line);
                }
                return result.toString();
            } else {
                Methods methods = new Methods();
                methods.saveAndAddResponse("Request URL:" + "http://relime.ga/api/auth/logout" + "\n-------------------\n" + "Token: \n" + token);
                methods.saveAndAddResponse("Response code: " + response.getStatusLine().getStatusCode() + "\n-------------------\n");
                FlowReport.reportAction("Logout is not successful", false);
                return String.valueOf(response.getStatusLine().getStatusCode());
            }

        } catch (Exception e) {

            return e.getMessage();
        }
    }

    public String authorizeWithSpecificCreds(String email, String password) throws Exception {
        String response = "";

        StringBuilder sb = new StringBuilder();
        sb.append("{\"Email\":\"").append(email).append("\",").append("\"Password\":\"").append(password).append("\"}");
        String content = sb.toString();
        response = getPOSTResponse(URL + "api/auth/login", content, null);
        return returnTokenAndSetStaticVariables(response);
    }

    public String getPOSTResponse(String apiURL, String content, String additionalParameter) {
        return sendHTTPRequest("POST", apiURL, content, null, additionalParameter);
    }

    public String getGETResponse(String apiURL, String parameters, String additionalParameter) {
        return sendHTTPRequest("GET", apiURL, null, parameters, additionalParameter);
    }

    public String getDELETEResponse(String apiURL, String parameters, String additionalParameter) {
        return sendHTTPRequest("DELETE", apiURL, null, parameters, additionalParameter);
    }

    public String getPUTResponse(String apiURL, String content, String additionalParameter) {
        return sendHTTPRequest("PUT", apiURL, content, null, additionalParameter);
    }

    public String genRandomEwerything(String postfix, String randObject) {
        final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        SecureRandom rnd = new SecureRandom();
        int len = 0;
        if (randObject.equals("ProjectKey")) {
            len = 4;
            StringBuilder sb = new StringBuilder(len);
            for (int i = 0; i < len; i++)
                sb.append(AB.charAt(rnd.nextInt(AB.length())));
            return sb.toString().toUpperCase();
        } else if (randObject.equals("VcsUrl")) {
            len = 6;
            StringBuilder sb = new StringBuilder(len);
            for (int i = 0; i < len; i++)
                sb.append(AB.charAt(rnd.nextInt(AB.length())));
            return "https://github.com/shotforlife/" + sb.toString() + ".git";
        } else if ((randObject.equals("JiraURL"))) {
            len = 6;
            StringBuilder sb = new StringBuilder(len);
            for (int i = 0; i < len; i++)
                sb.append(AB.charAt(rnd.nextInt(AB.length())));
            return "https://jira." + sb.toString() + ".com/";
        } else {
            len = 10;
            StringBuilder sb = new StringBuilder(len);
            for (int i = 0; i < len; i++)
                sb.append(AB.charAt(rnd.nextInt(AB.length())));

            return sb.toString() + postfix;
        }
    }

    private String returnTokenAndSetStaticVariables(String response) {
        JSONObject jsonObj = new JSONObject(response);
        token = jsonObj.getString("token");
        tokenType = jsonObj.getString("tokenType");
        userId = jsonObj.getInt("userId");
        firstName = jsonObj.getString("firstName");
        lastName = jsonObj.getString("lastName");
        return jsonObj.getString("token");
    }

    public List<String> getListOfActiveDomainsByEmail(String email) {
        List<String> listOfDomainNames = new ArrayList<>();

        try {
            ResultSet set = executeQueryToRelimeBase("SELECT * FROM [RelimeDb].[dbo].[Domain] where Owner_Id = (SELECT UserId FROM [RelimeDb].[dbo].[User] where UserName = '" + email + "') and Status = 0 order by Id desc ");
            while (set.next()) {
                listOfDomainNames.add(set.getString("Name"));
            }
        } catch (Exception ex) {
            return null;
        }
        return listOfDomainNames;

    }

    public Domains getListOfActiveDomainsInfoByEmail(String email) {
        Domains domains = new Domains();
        try {
            ResultSet set = executeQueryToRelimeBase("SELECT * FROM [RelimeDb].[dbo].[Domain] where Owner_Id = (SELECT UserId FROM [RelimeDb].[dbo].[User] where UserName = '" + email + "') and Status = 0 order by Id desc ");
            List<Domain> domainList = new ArrayList<Domain>();
            while (set.next()) {
                Domain domain = new Domain();
                domain.setId(set.getInt("Id"));
                domain.setName(set.getString("Name"));
                domain.setIsPrivate(set.getBoolean("IsPrivate"));
                domain.setOwnerId(set.getInt("Owner_Id"));
                domainList.add(domain);
            }
            domains.setDomains(domainList);
            domains.setTotalCount(domainList.size());
            return domains;
        } catch (Exception ex) {
            return null;
        }
    }


    public List<Integer> getIdsOfActiveDomainsByEmail(String email) {
        List<Integer> listOfDomainIds = new ArrayList<>();

        try {
            ResultSet set = executeQueryToRelimeBase("SELECT Id FROM [RelimeDb].[dbo].[Domain] where Owner_Id = (SELECT UserId FROM [RelimeDb].[dbo].[User] where UserName = '" + email + "') and Status = 0 order by Id desc ");
            while (set.next()) {
                listOfDomainIds.add(set.getInt("Id"));
            }
        } catch (Exception ex) {
            return null;
        }
        return listOfDomainIds;
    }

    public List<String> getNamesOfActiveDomainsById(String id) {
        List<String> listOfActiveDomainsNames = new ArrayList<>();
        try {
            ResultSet set = executeQueryToRelimeBase("SELECT * FROM [RelimeDb].[dbo].[Domain] where Id = " + id + " and Status = 0 order by Id desc ");
            while (set.next()) {
                listOfActiveDomainsNames.add(set.getString("Name"));
            }
        } catch (Exception ex) {
            return null;
        }
        return listOfActiveDomainsNames;
    }

    public UserBasicInfo getRegistredUserByEmail(String email) throws SQLException {
        try {
            UserBasicInfo userBasicInfo = new UserBasicInfo();
            ResultSet set = executeQueryToRelimeBase("SELECT  * FROM [RelimeDb].[dbo].[User] Where Email = '" + email + "'");
            while (set.next()) {
                userBasicInfo.setEmail(set.getString("Email"));
                userBasicInfo.setFirstName(set.getString("FirstName"));
                userBasicInfo.setLastName(set.getString("LastName"));
                userBasicInfo.setToken(set.getString("Token"));
                if (set.getInt("IsActive") == 1) {
                    userBasicInfo.setIsActive(true);
                } else {
                    userBasicInfo.setIsActive(false);
                }
            }
            return userBasicInfo;
        } catch (Exception ex) {
            return null;
        }
    }

    public boolean validateUserBasicInfo(UserBasicInfo responseObject, UserBasicInfo databaseObject) {
        if (responseObject.getFirstName().equals(databaseObject.getFirstName()) & responseObject.getLastName().equals(databaseObject.getLastName())
                & responseObject.getEmail().equals(databaseObject.getEmail()) & responseObject.getIsActive().equals(databaseObject.getIsActive())) {
            return true;
        }
        return false;
    }

    public List<String> getIdOfActiveProjectByDomainName(String domainName) {
        List<String> listOfActiveProjectIds = new ArrayList<>();
        try {
            ResultSet set = executeQueryToRelimeBase("SELECT Id FROM [RelimeDb].[dbo].[Project] where Domain_Id = (SELECT Id FROM [RelimeDb].[dbo].[Domain] where Name = '" + domainName + "') and IsActive = 1 order by Id desc ");
            while (set.next()) {
                listOfActiveProjectIds.add(set.getString("Id"));
            }
        } catch (Exception ex) {
            return null;
        }
        return listOfActiveProjectIds;
    }

    public List<String> getListOfDomainsFromHitsApi(String response) {
        try {
            List<String> listOfDomains = new ArrayList<>();
            JSONObject jsonObj = new JSONObject(response);
            JSONArray array = jsonObj.getJSONArray("result");
            for (int i = 0; i < array.length(); i++) {
                listOfDomains.add(array.getString(i));
            }
            return listOfDomains;
        } catch (Exception ex) {
            return null;
        }
    }


    public void saveAndAddResponse(String response) {
        try {
            File tempDir = new File(System.getProperty("java.io.tmpdir"));
            File tempFile = File.createTempFile("report", ".txt", tempDir);
            FileWriter fileWriter = new FileWriter(tempFile, true);
            System.out.println(tempFile.getAbsolutePath());
            BufferedWriter bw = new BufferedWriter(fileWriter);
            bw.write(response);
            bw.close();
            FlowReport.addAttachment(tempFile.getAbsolutePath());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Domains getListOfSubscribedDomainByLogin(String email) {
        Domains domains = new Domains();
        try {
            ResultSet subscribedProjectIds = executeQueryToRelimeBase("SELECT Project_Id FROM [RelimeDb].[dbo].[Subscription] where User_Id = (SELECT [UserId] FROM [RelimeDb].[dbo].[User] where IsActive = 1 and UserName = '" + email + "')");
            List<Domain> domainsList = new ArrayList<Domain>();
            List<Integer> subscribedProjIds = new ArrayList<>();
            while (subscribedProjectIds.next()) {
                int projectId = subscribedProjectIds.getInt("Project_Id");
                subscribedProjIds.add(projectId);
            }
            for (Integer id : subscribedProjIds) {
                ResultSet domainInfo = executeQueryToRelimeBase("SELECT [Id] ,[Name] ,[IsPrivate],[Owner_Id] FROM [RelimeDb].[dbo].[Domain] where Status = 0 and  Id = (SELECT  [Domain_Id]  FROM [RelimeDb].[dbo].[Project] where id = " + String.valueOf(id) + ")");
                while (domainInfo.next()) {
                    Domain domain = new Domain();
                    domain.setId(domainInfo.getInt("Id"));
                    domain.setOwnerId(domainInfo.getInt("Owner_Id"));
                    domain.setIsPrivate(domainInfo.getBoolean("IsPrivate"));
                    domain.setName(domainInfo.getString("Name"));
                    domainsList.add(domain);
                }
            }

            domains.setDomains(domainsList);
            domains.setTotalCount(domainsList.size());
        } catch (Exception ex) {

        }
        return domains;
    }

    public Domains getListOfOwnAndSubscribedDomainsByEmail(String login, String isOwned) {
        try {
            Domains listForReturn = new Domains();
            List<Domain> listDomainsForReturn = new ArrayList<Domain>();


            Domains listOfOwnDomains = getListOfActiveDomainsInfoByEmail(login);
            listDomainsForReturn.addAll(listOfOwnDomains.getDomains());


            if (!isOwned.equals("true")) {
                Domains listOfSubscribedDomains = getListOfSubscribedDomainByLogin(login);
                listDomainsForReturn.addAll(listOfSubscribedDomains.getDomains());
            }
            listForReturn.setDomains(listDomainsForReturn);
            return listForReturn;

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public boolean validateDomainsInfo(Domains responseObject, Domains dataBaseObject) {

        List<Domain> resp = responseObject.getDomains();
        List<Domain> databaseInfo = dataBaseObject.getDomains();
        int validCount = 0;
        if (resp.size() == databaseInfo.size()) {
            for (int i = 0; i < resp.size(); i++) {
                for (int j = 0; j < databaseInfo.size(); j++) {
                    if (resp.get(i).getId().equals(databaseInfo.get(j).getId()) & resp.get(i).getName().equals(databaseInfo.get(j).getName())
                            & resp.get(i).getOwnerId().equals(databaseInfo.get(j).getOwnerId()) & resp.get(i).getIsPrivate().equals(databaseInfo.get(j).getIsPrivate())) {
                        validCount++;
                    }
                }
            }
            if (validCount == resp.size()) {
                return true;
            }
        } else {
            return false;
        }
        return false;
    }



    public Results getListOfActiveProjectsByDomainName(String domainName) {
        Results results = new Results();
        try {
            ResultSet set = executeQueryToRelimeBase("SELECT [Project].Id, [Project].Name as ProjectName, [Project].isPrivate, [Project].isActive, [Project].ProjectKey, [Project].SavingMode, [Project].Description, [BddFrameworkType].Name as BDDFrameworkType FROM [RelimeDb].[dbo].[Project] " +
                    "inner join [RelimeDb].[dbo].[BddFrameworkType] on [RelimeDb].[dbo].[Project].BddFrameworkType_Id = [RelimeDb].[dbo].[BddFrameworkType].Id \n" +
                    "where Domain_Id = (SELECT Id FROM [RelimeDb].[dbo].[Domain] where Name = '" + domainName + "' and Status = 0 ) order by Id desc");
            List<Result> resultList = new ArrayList<Result>();
            while (set.next()) {
                Result result = new Result();
                result.setId(set.getInt("Id"));
                result.setName(set.getString("ProjectName"));
                result.setIsPrivate(set.getBoolean("IsPrivate"));
                result.setIsActive(set.getBoolean("IsActive"));
                result.setBddFrameworkType(set.getString("BddFrameworkType"));
                result.setDescription(set.getString("Description"));
                result.setKey(set.getString("ProjectKey"));
                result.setSavingMode(set.getString("SavingMode"));
                resultList.add(result);
            }
            results.setResults(resultList);
            results.setResultsCount(resultList.size());
            results.setPagesCount(results.getPagesCount());
            return results;
        } catch (Exception ex) {
            return null;
        }
    }

    public Results getListOfProjectsByDomainName(String domainName) {
        try {
            Results listForReturn = new Results();
            List<Result> listProjectsForReturn = new ArrayList<Result>();


            Results listOfProjects = getListOfActiveProjectsByDomainName(domainName);
            listProjectsForReturn.addAll(listOfProjects.getResults());

            listForReturn.setResults(listProjectsForReturn);
            return listForReturn;

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public boolean validateResultsInfo(Results responseObject, Results dataBaseObject) {

        List<Result> resp = responseObject.getResults();
        List<Result> databaseInfo = dataBaseObject.getResults();
        int validCount = 0;
        if (resp.size() <= databaseInfo.size()) {
            for (int i = 0; i < resp.size(); i++) {
                for (int j = 0; j < databaseInfo.size(); j++) {
                    if (resp.get(i).getId().equals(databaseInfo.get(j).getId()) & resp.get(i).getName().equals(databaseInfo.get(j).getName()) & resp.get(i).getDescription().equals(databaseInfo.get(j).getDescription())
                            & resp.get(i).getIsPrivate().equals(databaseInfo.get(j).getIsPrivate()) & resp.get(i).getIsActive().equals(databaseInfo.get(j).getIsActive())
                            & resp.get(i).getBddFrameworkType().equals(databaseInfo.get(j).getBddFrameworkType()) & resp.get(i).getKey().equals(databaseInfo.get(j).getKey())
                            & resp.get(i).getSavingMode().equals(databaseInfo.get(j).getSavingMode())) {
                        validCount++;
                    }
                }
            }
            if (validCount == resp.size()) {
                return true;
            }
        } else {
            return false;
        }
        return false;
    }

    public Projects getActiveProjectByProjectKey(String projectKey) {
        Projects projects = new Projects();
        try {
            ResultSet set = executeQueryToRelimeBase("SELECT [Project].Id, [Project].Name as ProjectName, [Project].isPrivate, [Project].isActive, [Project].ProjectKey, [Project].SavingMode, [Project].Description, [BddFrameworkType].Name as BDDFrameworkType " +
                    "From [RelimeDb].[dbo].[Project] inner join [RelimeDb].[dbo].[BddFrameworkType] on [RelimeDb].[dbo].[Project].BddFrameworkType_Id = [RelimeDb].[dbo].[BddFrameworkType].Id " +
                    "where ProjectKey = '" + projectKey + "'");
            Project project = new Project();
            if (set.next()) {
                project.setId(set.getInt("Id"));
                project.setName(set.getString("ProjectName"));
                project.setIsPrivate(set.getBoolean("IsPrivate"));
                project.setIsActive(set.getBoolean("IsActive"));
                project.setBddFrameworkType(set.getString("BddFrameworkType"));
                project.setDescription(set.getString("Description"));
                project.setKey(set.getString("ProjectKey"));
                project.setSavingMode(set.getString("SavingMode"));
            }
            projects.setProject(project);
            return projects;
        } catch (Exception ex) {
            return null;
        }
    }

    public List<String> getListOfActiveProjectKeysByDomainName(String domainName) {
        try {
            ResultSet set = executeQueryToRelimeBase("SELECT [Project].[ProjectKey]\n" +
                    "FROM [RelimeDb].[dbo].[Project]\n" +
                    "inner join [RelimeDb].[dbo].[Domain] on [Domain].Id = [Project].Domain_Id\n" +
                    "where [Domain].Name = '" + domainName + "'\n" +
                    "and IsActive = 1");
            List<String> listForReturn = new ArrayList<>();
            while (set.next()) {
                listForReturn.add(set.getString("ProjectKey"));
            }
            return listForReturn;
        } catch (Exception ex) {
            return null;
        }
    }

    public Project getActiveProjectByProjectKeyProject(String projectKey) {
        try {
            ResultSet set = executeQueryToRelimeBase("SELECT [Project].Id, [Project].Name as ProjectName, [Project].isPrivate, [Project].isActive, [Project].ProjectKey, [Project].SavingMode, [Project].Description, [BddFrameworkType].Name as BDDFrameworkType " +
                    "From [RelimeDb].[dbo].[Project] inner join [RelimeDb].[dbo].[BddFrameworkType] on [RelimeDb].[dbo].[Project].BddFrameworkType_Id = [RelimeDb].[dbo].[BddFrameworkType].Id " +
                    "where ProjectKey = '" + projectKey + "'");
            Project project = new Project();
            if (set.next()) {
                project.setId(set.getInt("Id"));
                project.setName(set.getString("ProjectName"));
                project.setIsPrivate(set.getBoolean("IsPrivate"));
                project.setIsActive(set.getBoolean("IsActive"));
                project.setBddFrameworkType(set.getString("BddFrameworkType"));
                project.setDescription(set.getString("Description"));
                project.setKey(set.getString("ProjectKey"));
                project.setSavingMode(set.getString("SavingMode"));
            }
            return project;
        } catch (Exception ex) {
            return null;
        }
    }

    public String getVcsUrlByProjectKey(String projectKey) {
        String vcsByKey = "";
        try {
            ResultSet set = executeQueryToRelimeBase("Select [Vcs].Url, [Project].Name  FROM [RelimeDb].[dbo].[Vcs] inner join [RelimeDb].[dbo].[Project] on [RelimeDb].[dbo].[Vcs].Project_Id = [RelimeDb].[dbo].[Project].Id where [Project].ProjectKey = '" + projectKey + "'");
            while (set.next()) {
                vcsByKey = set.getString("Url");
            }
        } catch (Exception ex) {
            return null;
        }
        return vcsByKey;
    }

    public Vcs getVcsInfoByProjectKey(String projectKey) {
        try {
            ResultSet set = executeQueryToRelimeBase("Select [Vcs].Id, [VcsType].Name, [Vcs].Url, [VcsAccount].Account_Id FROM [RelimeDb].[dbo].[Vcs]\n" +
                    "inner join [RelimeDb].[dbo].[VcsType] on [RelimeDb].[dbo].[Vcs].Type_Id = [RelimeDb].[dbo].[VcsType].Id\n" +
                    "left join [RelimeDb].[dbo].[VcsAccount] on [RelimeDb].[dbo].[Vcs].Id = [RelimeDb].[dbo].[VcsAccount].Vcs_Id\n" +
                    "inner join [RelimeDb].[dbo].[Project] on [RelimeDb].[dbo].[Vcs].Project_Id = [RelimeDb].[dbo].[Project].Id\n" +
                    "where [Project].ProjectKey = '" + projectKey + "'");
            Vcs vcs = new Vcs();
            if (set.next()) {
                vcs.setId(set.getInt("Id"));
                vcs.setVcsType(set.getString("Name"));
                vcs.setUrl(set.getString("Url"));
                vcs.setBelongToAccount(set.getInt("Account_Id"));
            }
            return vcs;
        } catch (Exception ex) {
            return null;
        }
    }

    public Integer getVcsIdByProjectKey(String projectKey) throws SQLException {
        try {
            ResultSet set = executeQueryToRelimeBase("SELECT *\n" +
                    "  FROM [RelimeDb].[dbo].[Vcs]\n" +
                    "  inner join [RelimeDb].[dbo].[Project] on [Vcs].Project_Id = [Project].[Id]\n" +
                    "  where [Project].ProjectKey = '" + projectKey + "'");
            Integer vcsId = 0;
            if (set.next()) {
                vcsId = set.getInt("Id");
            }
            return vcsId;
        } catch (Exception ex) {
            return null;
        }
    }

    public boolean validateProjectVcsInfo(Vcs responseObject, Vcs dataBaseObject) {
        if (responseObject.getId().equals(dataBaseObject.getId()) & responseObject.getVcsType().equals(dataBaseObject.getVcsType()) & responseObject.getUrl().equals(dataBaseObject.getUrl())
                & responseObject.getBelongToAccount().equals(dataBaseObject.getBelongToAccount())) {
            String isActiveUrl = getGETResponse(responseObject.getUrl(), null, null);
            if (isActiveUrl != null) {
                return true;
            }
        } else {
            return false;
        }
        return false;
    }

    public Vcs getVcsInfoByProjectKeyWithoutAccount(String projectKey) {
        try {
            ResultSet set = executeQueryToRelimeBase("Select [Vcs].Id, [VcsType].Name, [Vcs].Url FROM [RelimeDb].[dbo].[Vcs]\n" +
                    "inner join [RelimeDb].[dbo].[VcsType] on [RelimeDb].[dbo].[Vcs].Type_Id = [RelimeDb].[dbo].[VcsType].Id\n" +
                    "inner join [RelimeDb].[dbo].[Project] on [RelimeDb].[dbo].[Vcs].Project_Id = [RelimeDb].[dbo].[Project].Id\n" +
                    "where [Project].ProjectKey = '" + projectKey + "'");
            Vcs vcs = new Vcs();
            if (set.next()) {
                vcs.setId(set.getInt("Id"));
                vcs.setVcsType(set.getString("Name"));
                vcs.setUrl(set.getString("Url"));
            }
            return vcs;
        } catch (Exception ex) {
            return null;
        }
    }

    public boolean validateProjectVcsInfoWithoutAccount(Vcs responseObject, Vcs dataBaseObject) {
        if (responseObject.getId().equals(dataBaseObject.getId()) & responseObject.getVcsType().equals(dataBaseObject.getVcsType()) & responseObject.getUrl().equals(dataBaseObject.getUrl())) {
            return true;
        } else {
            return false;
        }
    }

    public String getAccountNameById(int accountId) {
        String nameById = "";
        try {
            ResultSet set = executeQueryToRelimeBase("SELECT [Id],[Name] FROM [RelimeDb].[dbo].[Account] where Id = " + accountId);
            while (set.next()) {
                nameById = set.getString("Name");
            }
        } catch (Exception ex) {
            return null;
        }
        return nameById;
    }

    public void updateFieldsInDBForCreatingAndUpdatingAccounts(ResultSet setOfTestCases) throws Exception {
        if (setOfTestCases.getString("AccountPassword").contains("Password")) {
            FlowReport.nextStep();
            FlowReport.addStep("Updating 'AccountPassword' field in DB.", "");
            setOfTestCases.updateString("AccountPassword", genRandomEwerything("Password", ""));
            setOfTestCases.updateRow();
            FlowReport.reportAction("Field 'AccountPassword' updated", true);
        } else {
            FlowReport.reportAction("Field 'AccountPassword' didn't updated", false);
        }

        if (setOfTestCases.getString("AccountLogin").contains("Login")) {
            FlowReport.nextStep();
            FlowReport.addStep("Updating 'AccountLogin' field in DB.", "");
            setOfTestCases.updateString("AccountLogin", genRandomEwerything("Login", ""));
            setOfTestCases.updateRow();
            FlowReport.reportAction("Field 'AccountLogin' updated", true);
        } else {
            FlowReport.reportAction("Field 'AccountLogin' didn't updated", false);
        }

        if (setOfTestCases.getString("AccountName").contains("Name")) {
            FlowReport.nextStep();
            FlowReport.addStep("Updating 'AccountName' field in DB.", "");
            setOfTestCases.updateString("AccountName", genRandomEwerything("Name", ""));
            setOfTestCases.updateRow();
            FlowReport.reportAction("Field 'AccountName' updated", true);
        } else {
            FlowReport.reportAction("Field 'AccountName' didn't updated", false);
        }
    }

    public RetrieveAccounts getListOfAccountsInfoByEmailAsList(String email, ResultSet testCase) {
        try {
            RetrieveAccounts retrieveAccounts = new RetrieveAccounts();
            ResultSet set = executeQueryToRelimeBase("SELECT [Account].Id, [Account].Name, [Account].Login FROM [RelimeDb].[dbo].[Account] " +
                    "inner join [RelimeDb].[dbo].[User] on [Account].User_Id = [User].UserId " +
                    "where [User].UserName = '" + email + "'");
            List<RetrieveAccount> retrieveAccountsList = new ArrayList<>();
            while (set.next()) {
                RetrieveAccount retrieveAccount = new RetrieveAccount();
                retrieveAccount.setId(set.getInt("Id"));
                retrieveAccount.setAccountName(set.getString("Name"));
                if (testCase.getString("IncludeLoginNames").equals("true")) {
                    retrieveAccount.setLogin(set.getString("Login"));
                }
                retrieveAccountsList.add(retrieveAccount);
            }
            retrieveAccounts.setAccountsResults(retrieveAccountsList);
            return retrieveAccounts;
        } catch (Exception ex) {
            return null;
        }
    }

    public RetrieveAccounts getListOfAccountInfoByEmail(String email, ResultSet testCase) {
        try {
            RetrieveAccounts listForReturn = new RetrieveAccounts();
            List<RetrieveAccount> listAccountsForReturn = new ArrayList<RetrieveAccount>();


            RetrieveAccounts listOfAccounts = getListOfAccountsInfoByEmailAsList(email, testCase);
            listAccountsForReturn.addAll(listOfAccounts.getAccountsResults());

            listForReturn.setAccountsResults(listAccountsForReturn);
            return listForReturn;

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public boolean validateAccountsInfo(RetrieveAccounts responseObject, RetrieveAccounts dataBaseObject) throws SQLException {
        List<RetrieveAccount> resp = responseObject.getAccountsResults();
        List<RetrieveAccount> databaseInfo = dataBaseObject.getAccountsResults();
        int validCount = 0;
        if (resp.size() == databaseInfo.size()) {
            for (int i = 0; i < resp.size(); i++) {
                for (int j = 0; j < databaseInfo.size(); j++) {
                    if (resp.get(i).getId().equals(databaseInfo.get(j).getId()) & resp.get(i).getAccountName().equals(databaseInfo.get(j).getAccountName())) {
                        validCount++;
                    }
                }
            }
            if (validCount == resp.size()) {
                return true;
            }
        } else {
            return false;
        }
        return false;
    }

    public List<Integer> getListOfAccountIdsByEmail(String email) throws SQLException {
        ResultSet set = executeQueryToRelimeBase("SELECT [Account].Id, [User].UserName FROM [RelimeDb].[dbo].[Account]\n" +
                "inner join [RelimeDb].[dbo].[User] on [User].UserId = [Account].User_Id\n" +
                "where [User].UserName = '" + email + "'");
        List<Integer> listOfAccountIds = new ArrayList<>();
        while (set.next()) {
            listOfAccountIds.add(set.getInt("Id"));
        }
        return listOfAccountIds;
    }

    public StoryTrees getStoryTreesByProjectKey(String projectKey) {
        try {
            StoryTrees storyTrees = new StoryTrees();
            ResultSet set = executeQueryToRelimeBase("SELECT [Tree].[Id], [Tree].[ParentId], [Tree].[Name], [Tree].[TreeNodeId], [Story].[Id] as StoryId FROM [RelimeDb].[dbo].[Tree]\n" +
                    "left join [RelimeDb].[dbo].[Story] on [Tree].Id = [Story].Tree_Id\n" +
                    "inner join [RelimeDb].[dbo].[Project] on [Tree].Project_Id = [Project].Id\n" +
                    "where [Project].[ProjectKey] = '" + projectKey + "' " +
                    "order by [Story].[Id] asc");
            List<StoryTree> retrieveStoryTreeList = new ArrayList<>();
            while (set.next()) {
                StoryTree storyTree = new StoryTree();
                storyTree.setId(set.getInt("Id"));
                storyTree.setLabel(set.getString("Name"));
                storyTree.setParentId(set.getInt("ParentId"));
                storyTree.setStoryId(set.getInt("StoryId"));
                storyTree.setChildren(new ArrayList<>());
                if (set.getString("TreeNodeId").equals("1")) {
                    storyTree.setIsFolder(false);
                } else {
                    storyTree.setIsFolder(true);
                }

                retrieveStoryTreeList.add(storyTree);
            }
            storyTrees.setStoryTree(retrieveStoryTreeList);
            return storyTrees;
        } catch (Exception ex) {
            return null;
        }
    }

    public StoryTrees getChildrens(List<StoryTree> list) {
        StoryTrees storyTrees = new StoryTrees();
        while (list.stream().anyMatch(p -> !p.getParentId().equals(0))) {
            for (int i = 0; i < list.size(); i++) {
                if (!list.get(i).getParentId().equals(0)) {
                    StoryTree temp = list.get(i);
                    list.remove(i);
                    i--;
                    for (StoryTree possibleParent : list) {
                        putInParent(possibleParent, temp);
                    }
                }
            }
        }
        storyTrees.setStoryTree(list);
        return storyTrees;
    }

    public void putInParent(StoryTree p, StoryTree temp) {
        if (p.getId().equals(temp.getParentId())) {
            List<StoryTree> pChildren = p.getChildren();
            pChildren.add(temp);
            p.setChildren(pChildren);
        }
        for (StoryTree x : p.getChildren()) {
            putInParent(x, temp);
        }
    }

    public List<Integer> getParentIdsByProjectKey(String projectKey) {
        try {
            ResultSet set = executeQueryToRelimeBase("SELECT DISTINCT [Tree].[ParentId] FROM [RelimeDb].[dbo].[Tree]\n" +
                    "inner join [RelimeDb].[dbo].[Project] on [Tree].Project_Id = [Project].Id\n" +
                    "where [Project].[ProjectKey] = '" + projectKey + "'");
            List<Integer> parentsIdList = new ArrayList<>();
            while (set.next()) {
                parentsIdList.add(set.getInt("ParentId"));
            }
            return parentsIdList;
        } catch (Exception ex) {
            return null;
        }
    }

    public List<Integer> getStoryTreeIdsByProjectKey(String projectKey) {
        try {
            ResultSet set = executeQueryToRelimeBase("SELECT DISTINCT [Tree].[Id] FROM [RelimeDb].[dbo].[Tree]\n" +
                    "inner join [RelimeDb].[dbo].[Project] on [Tree].Project_Id = [Project].Id\n" +
                    "where [Project].[ProjectKey] = '" + projectKey + "'");
            List<Integer> idsList = new ArrayList<>();
            while (set.next()) {
                idsList.add(set.getInt("Id"));
            }
            return idsList;
        } catch (Exception ex) {
            return null;
        }
    }

    public StoryTrees sortStoryTreeObject(StoryTrees storyTree) {
        StoryTrees storyTrees = new StoryTrees();

        List<StoryTree> inputTreeList = new ArrayList<>();
        inputTreeList.addAll(storyTree.getStoryTree());
        List<StoryTree> listForReturn = new ArrayList<>();
        int index = 0;
        for (int i = 0; i < inputTreeList.size(); i++) {
            Integer min = inputTreeList.get(i).getStoryId();
            index = i;
            for (int j = i + 1; j < inputTreeList.size(); j++) {
                if (inputTreeList.get(j).getStoryId() < min) {
                    min = inputTreeList.get(j).getStoryId();
                    index = j;
                }
            }
            listForReturn.add(inputTreeList.get(index));
            inputTreeList.remove(index);
            i--;
        }
        storyTrees.setStoryTree(listForReturn);
        return storyTrees;
    }

    public List<String> getListOfStoryTreeNamesByProjectKey(String projectKey) {
        try {
            ResultSet set = executeQueryToRelimeBase("SELECT  [Tree].[Name] FROM [RelimeDb].[dbo].[Tree]\n" +
                    "inner join [RelimeDb].[dbo].[Project] on [Tree].Project_Id = [Project].Id\n" +
                    "where [Project].[ProjectKey] = '" + projectKey + "'");
            List<String> parentsIdList = new ArrayList<>();
            while (set.next()) {
                parentsIdList.add(set.getString("Name"));
            }
            return parentsIdList;
        } catch (Exception ex) {
            return null;
        }
    }

    public Stories getStoriesInfoByProjectKey(String projectKey) {
        try {
            Stories stories = new Stories();
            ResultSet set = executeQueryToRelimeBase("SELECT [Story].[Id], [Story].[Name], [Story].[Description], [Story].[Tree_Id] as FieldId, [StoryManagementTool].[Key], [Tag].[Name] as TagName\n" +
                    "  FROM [RelimeDb].[dbo].[Story]\n" +
                    "  left join [RelimeDb].[dbo].[StoryManagementTool] on [StoryManagementTool].[Story_Id] = [Story].[Id]\n" +
                    "  left join [RelimeDb].[dbo].[TagStory] on [TagStory].[Story_Id] = [Story].[Id]\n" +
                    "  left join [RelimeDb].[dbo].[Tag] on  [TagStory].[Tag_Id] = [Tag].[Id]\n" +
                    "  left join [RelimeDb].[dbo].[Tree] on [Tree].[Id] = [Story].[Tree_Id]\n" +
                    "  left join [RelimeDb].[dbo].[Project] on [Tree].Project_Id = [Project].Id\n" +
                    "  where [Project].ProjectKey = '" + projectKey + "'");
            List<Story> retrieveStoryList = new ArrayList<>();
            while (set.next()) {
                Story story = new Story();
                story.setId(set.getInt("Id"));
                story.setDescription(set.getString("Description"));
                story.setName(set.getString("Name"));
                story.setFileId(set.getInt("FieldId"));
                story.setKey(set.getString("Key"));

                story.setTags(new ArrayList<>());

                story.setUniqueTag("ST_" + set.getInt("Id"));
                if (story.getKey().equals(0)) {
                    story.setIsLinked(false);
                } else {
                    story.setIsLinked(true);
                }

                retrieveStoryList.add(story);
            }
            stories.setStories(retrieveStoryList);
            return stories;
        } catch (Exception ex) {
            return null;
        }
    }

    public List<String> getTagsByProjectKey(String projectKey) {
        try {
            ResultSet set = executeQueryToRelimeBase("SELECT [Story].[Id], [Story].[Name], [Story].[Description], [Story].[Tree_Id] as FieldId, [StoryManagementTool].[Key], [Tag].[Name] as TagName\n" +
                    "  FROM [RelimeDb].[dbo].[Story]\n" +
                    "  left join [RelimeDb].[dbo].[StoryManagementTool] on [StoryManagementTool].[Story_Id] = [Story].[Id]\n" +
                    "  left join [RelimeDb].[dbo].[TagStory] on [TagStory].[Story_Id] = [Story].[Id]\n" +
                    "  left join [RelimeDb].[dbo].[Tag] on  [TagStory].[Tag_Id] = [Tag].[Id]\n" +
                    "  left join [RelimeDb].[dbo].[Tree] on [Tree].[Id] = [Story].[Tree_Id]\n" +
                    "  left join [RelimeDb].[dbo].[Project] on [Tree].Project_Id = [Project].Id\n" +
                    "  where [Project].ProjectKey = '" + projectKey + "'");
            List<String> tagsList = new ArrayList<>();
            while (set.next()) {
                tagsList.add(set.getString("TagName"));
            }
            return tagsList;
        } catch (Exception ex) {
            return null;
        }
    }

    public Stories setTagsList(List<Story> storyList, List<String> tagsList) {
        Stories stories = new Stories();
        Story story = null;
        List<String> tags = new ArrayList<>();
        for (int i = 0; i < storyList.size() - 1; i++) {
            if (!storyList.get(i).getId().equals(storyList.get(i + 1).getId())) {
                if (tagsList.get(i) != null) {
                    tags.add(tagsList.get(i));
                    story = storyList.get(i);
                    story.setTags(tags);
                    tags = new ArrayList<>();
                    storyList.set(i, story);
                }
            } else {
                tags.add(tagsList.get(i));
                storyList.remove(i);
                tagsList.remove(i);
                i--;
            }
        }
        if (tagsList.size() != 0) {
            tags.add(tagsList.get(storyList.size() - 1));
            story = storyList.get(storyList.size() - 1);
            story.setTags(tags);
            storyList.set(storyList.size() - 1, story);
        }

        stories.setStories(storyList);
        return stories;
    }

    public boolean validateStoryCreation(List<Story> storyList, Story responseObject) {
        for (int i = 0; i < storyList.size(); i++) {
            if (storyList.get(i).getId().equals(responseObject.getStoryId()) & storyList.get(i).getFileId().equals(responseObject.getFileId())
                    & storyList.get(i).getUniqueTag().equals(responseObject.getUniqueTag())) {
                return true;
            }
        }
        return false;
    }

    public List<Integer> getListOfStoryIdsByProjectKey(String projectKey) {
        try {
            ResultSet set = executeQueryToRelimeBase("SELECT [Story].[Id]\n" +
                    "  FROM [RelimeDb].[dbo].[Story]\n" +
                    "  left join [RelimeDb].[dbo].[Tree] on [Tree].[Id] = [Story].[Tree_Id]\n" +
                    "  left join [RelimeDb].[dbo].[Project] on [Tree].Project_Id = [Project].Id\n" +
                    "  where [Project].ProjectKey = '" + projectKey + "'");
            List<Integer> storyIdList = new ArrayList<>();
            while (set.next()) {
                storyIdList.add(set.getInt("Id"));
            }
            return storyIdList;
        } catch (Exception ex) {
            return null;
        }
    }

    public ScenarioCreateResponse getScenarioInfoFromDBByStoryId(int storyId) {
        try {
            ScenarioCreateResponse scenarioResponse = new ScenarioCreateResponse();
            ResultSet set = executeQueryToRelimeBase("SELECT [Scenario].[Id], [Scenario].[Description], [Scenario].[Name], [Scenario].[TtsSynced], [Scenario].[GitSynced], [ScenarioType].Type\n" +
                    "FROM [RelimeDb].[dbo].[Scenario]\n" +
                    "inner join [RelimeDb].[dbo].[ScenarioType] on [Scenario].[ScenarioType_Id] = [ScenarioType].Id\n" +
                    "where [Scenario].Story_Id = " + storyId);
            while (set.next()) {
                scenarioResponse.setId(set.getInt("Id"));
                scenarioResponse.setDescription(set.getString("Description"));
                scenarioResponse.setName(set.getString("Name"));
                scenarioResponse.setType(set.getString("Type"));
                scenarioResponse.setIsBriefInfo(true);
                if (scenarioResponse.getType().equals("Background")) {
                    scenarioResponse.setUniqueTag("BG");
                } else {
                    scenarioResponse.setUniqueTag("SC_" + scenarioResponse.getId());
                }
                scenarioResponse.setGitSynced(set.getInt("GitSynced"));
                scenarioResponse.setTtsSynced(set.getInt("TtsSynced"));
            }
            return scenarioResponse;
        } catch (Exception ex) {
            return null;
        }
    }

    public List<Integer> getScenarioIdsListByStoryId(Integer storyId) {
        try {
            List<Integer> listOfIds = new ArrayList<>();
            ResultSet set = executeQueryToRelimeBase("SELECT [Id]\n" +
                    "FROM [RelimeDb].[dbo].[Scenario]\n" +
                    "where Story_Id = " + storyId);
            while (set.next()) {
                listOfIds.add(set.getInt("Id"));
            }
            return listOfIds;
        } catch (Exception ex) {
            return null;
        }
    }

    public ScenarioInfoResponse getScenarioInfoByScenarioIdFromDB(Integer scenarioId) {
        try {
            ScenarioInfoResponse scenarioInfoResponse = new ScenarioInfoResponse();
            ResultSet set = executeQueryToRelimeBase("SELECT [Scenario].[Id], [Scenario].[Description], [Scenario].[Name], [Scenario].[TtsSynced], [Scenario].[GitSynced], [ScenarioType].Type\n" +
                    "FROM [RelimeDb].[dbo].[Scenario]\n" +
                    "inner join [RelimeDb].[dbo].[ScenarioType] on [Scenario].[ScenarioType_Id] = [ScenarioType].Id\n" +
                    "where [Scenario].Id = " + scenarioId);
            while (set.next()) {
                scenarioInfoResponse.setDescription(set.getString("Description"));
                scenarioInfoResponse.setName(set.getString("Name"));
                scenarioInfoResponse.setIsBriefInfo(false);
            }
            scenarioInfoResponse.setTags(getTagsByScenarioId(scenarioId));
            scenarioInfoResponse.setStepText(getStepTextByScenarioId(scenarioId));
            scenarioInfoResponse.setExampleTable(getExampleTableFromDBByScenarioId(scenarioId));
            return scenarioInfoResponse;
        } catch (Exception ex) {
            return null;
        }
    }

    public List<String> getTagsByScenarioId(int scenarioId) {
        try {
            ResultSet set = executeQueryToRelimeBase("SELECT [Scenario].[Id], [Scenario].[Description], [Scenario].[Name], [Scenario].[TtsSynced], [Scenario].[GitSynced], [ScenarioType].Type, [Tag].Name as TagName\n" +
                    "FROM [RelimeDb].[dbo].[Scenario]\n" +
                    "inner join [RelimeDb].[dbo].[ScenarioType] on [Scenario].[ScenarioType_Id] = [ScenarioType].Id\n" +
                    "inner join [RelimeDb].[dbo].[TagScenario] on [TagScenario].[Scenario_Id] = [Scenario].Id\n" +
                    "inner join [RelimeDb].[dbo].[Tag] on  [TagScenario].[Tag_Id] = [Tag].[Id]\n" +
                    "where [Scenario].Id = " + scenarioId);
            List<String> tagsList = new ArrayList<>();
            while (set.next()) {
                tagsList.add(set.getString("TagName"));
            }
            return tagsList;
        } catch (Exception ex) {
            return null;
        }
    }

    public List<String> getStepTextByScenarioId(int scenarioId) {
        try {
            ResultSet set = executeQueryToRelimeBase("SELECT [StepText]\n" +
                    "FROM [RelimeDb].[dbo].[StepLine]\n" +
                    "where Scenario_Id = " + scenarioId);
            List<String> stepTextList = new ArrayList<>();
            while (set.next()) {
                stepTextList.add(set.getString("StepText"));
            }
            return stepTextList;
        } catch (Exception ex) {
            return null;
        }
    }

    public ExampleTable getExampleTableFromDBByScenarioId(int scenarioId) {
        try {
            ExampleTable exampleTable = new ExampleTable();
            ResultSet set = executeQueryToRelimeBase("SELECT [Id], [TitleRow], [BodyRow], [Description], [Scenario_Id], [Name]\n" +
                    "  FROM [RelimeDb].[dbo].[ExampleTable]\n" +
                    "  where Scenario_Id = " + scenarioId);
            while (set.next()) {
                exampleTable.setDescription(set.getString("Description"));
                exampleTable.setName(set.getString("Name"));
                exampleTable.setData(getTableDataFromDB(set.getString("BodyRow")));
                exampleTable.setColumns(getTableColumns(set.getString("TitleRow")));
            }
            return exampleTable;
        } catch (Exception ex) {
            return null;
        }
    }

    public List<Data> getTableDataFromDB(String bodyRow) throws SQLException {
        List<Data> dataList = new ArrayList<>();
        Data data = new Data();
        List<String> bodyRowList = new ArrayList<>();
        bodyRow = bodyRow.trim();
        bodyRow = bodyRow.substring(1, bodyRow.length() - 1);
        bodyRowList.addAll(Arrays.asList(bodyRow.split("\\|\r\n\\|")));
        List<String> finalList = new ArrayList<>();
        for (int i = 0; i < bodyRowList.size(); i++) {
            finalList.addAll(Arrays.asList(bodyRowList.get(i).split("\\|")));
        }
        for (int i = 0; i < finalList.size(); i++) {
            data.setArg1(finalList.get(i));
            data.setArg2(finalList.get(i + 1));
            i++;
            dataList.add(data);
        }
        return dataList;
    }

    public List<String> getTableColumns(String titleRow) throws SQLException {
        List<String> columnList = new ArrayList<>();
        titleRow = titleRow.trim().substring(1, titleRow.length() - 1);
        columnList.addAll(Arrays.asList(titleRow.split("\\|")));

        return columnList;
    }

    // check that user have active domain or create new
    public List<String> checkThatDomainsIsExist(ResultSet setOfTestCases) throws Exception {
        DomainClient domainClient = new DomainClient();
        List<String> activeDomainsList = getListOfActiveDomainsByEmail(setOfTestCases.getString("Login"));
        if (activeDomainsList.size() == 0) {
            if (setOfTestCases.getString("DomainName").contains("Name")) {
                String curName = genRandomEwerything("Name", "");
                FlowReport.addStep("Update Name (" + curName + ") of domain in DB.", "");
                setOfTestCases.updateString("DomainName", curName);
                setOfTestCases.updateRow();
                FlowReport.reportAction("Domain names updated.", true);
            } else FlowReport.reportAction("Something wrong with domains names updating!", false);

            domainClient.getDomainCreationResponse(setOfTestCases);
            activeDomainsList = getListOfActiveDomainsByEmail(setOfTestCases.getString("Login"));
            FlowReport.reportAction("Domains created", true);
        }
        return activeDomainsList;
    }

    // check that current domain have project or create new
    public List<String> checkThatProjectsIsExist(ResultSet setOfTestCases) throws Exception {
        ProjectClient projectClient = new ProjectClient();
        List<String> listOfActiveProjectKeys = getListOfActiveProjectKeysByDomainName(setOfTestCases.getString("DomainName"));
        if (listOfActiveProjectKeys.size() == 0) {
            FlowReport.nextStep();
            FlowReport.addStep("Updating 'ProjectKey' field in DB.", "");
            setOfTestCases.updateString("ProjectKey", genRandomEwerything("", "ProjectKey"));
            setOfTestCases.updateRow();
            FlowReport.reportAction("Field 'ProjectKey' updated", true);

            FlowReport.nextStep();
            FlowReport.addStep("Updating 'ProjectName' field in DB.", "");
            setOfTestCases.updateString("ProjectName", genRandomEwerything("Name", ""));
            setOfTestCases.updateRow();
            FlowReport.reportAction("Field 'ProjectName' updated", true);

            projectClient.getCreateProjectResponse(setOfTestCases);
            listOfActiveProjectKeys = getListOfActiveProjectKeysByDomainName(setOfTestCases.getString("DomainName"));
        }
        return listOfActiveProjectKeys;
    }

    // check that project have active vcs or add it to project
    public void checkThatVcsIsExist(ResultSet setOfTestCases, String projectKey) throws Exception {
        VcsClient vcsClient = new VcsClient();

        if (getVcsInfoByProjectKeyWithoutAccount(projectKey).getUrl() == null) {
            if (setOfTestCases.getString("URL").contains("git")) {
                FlowReport.nextStep();
                FlowReport.addStep("Updating 'URL' field in DB.", "");
                setOfTestCases.updateString("URl", genRandomEwerything("", "VcsUrl"));
                setOfTestCases.updateRow();
                FlowReport.reportAction("Field 'URL' updated", true);
            }
            vcsClient.getAddVcsToProjectResponse(setOfTestCases);
        }
    }

    // check that project have active tts or add it to project
    public Integer checkThatTtsIsExist(ResultSet setOfTestCases) throws Exception {
        Integer ttsId = 0;
        TaskTrackerClient taskTrackerClient = new TaskTrackerClient();
        List<TaskTracker> taskTrackerList = getTaskTrackerInfoFromDbByProjectKey(setOfTestCases.getString("ProjectKey")).getTaskTrackers();
        if (taskTrackerList.size() == 0) {
            FlowReport.nextStep();
            FlowReport.addStep("Updating 'TaskTrackerURL' (" + setOfTestCases.getString("TaskTrackerURL") + ") field in DB.", "");
            setOfTestCases.updateString("TaskTrackerURL", setOfTestCases.getString("TaskTrackerURL"));
            setOfTestCases.updateRow();
            FlowReport.reportAction("Field 'TaskTrackerURL' updated", true);

            String response = taskTrackerClient.getCreateTaskTrackerSystemResponse(setOfTestCases);
            JSONObject jsonObject = new JSONObject(response);
            ttsId = jsonObject.getInt("id");
            return ttsId;
        }
        ttsId = taskTrackerList.get(0).getId();
        return ttsId;
    }

    // check that user have vcs/tts account or create new
    public List<Integer> checkThatAccountsIsExist(ResultSet setOfTestCases) throws Exception {
        AccountClient accountClient = new AccountClient();
        List<Integer> listOfAccountIds = getListOfAccountIdsByEmail(setOfTestCases.getString("Login"));
        if (listOfAccountIds.size() == 0) {
            updateFieldsInDBForCreatingAndUpdatingAccounts(setOfTestCases);
            FlowReport.nextStep();
            FlowReport.addStep("Creating new account.", "");
            accountClient.getCreateAccountResponse(setOfTestCases);
            FlowReport.reportAction("Account created successfully.", true);
            listOfAccountIds = getListOfAccountIdsByEmail(setOfTestCases.getString("Login"));
        }
        return listOfAccountIds;
    }

    // check that project have a story or create new
    public List<Integer> checkThatStoryIsExist(ResultSet setOfTestCases) throws Exception {
        StoryClient storyClient = new StoryClient();
        List<Integer> listOfStoryIds = getListOfStoryIdsByProjectKey(setOfTestCases.getString("ProjectKey"));
        if (listOfStoryIds.size() == 0) {
            FlowReport.nextStep();
            FlowReport.addStep("Getting a list of parents ids.", "");
            List<Integer> listOfParentIds = getParentIdsByProjectKey(setOfTestCases.getString("ProjectKey"));
            if (listOfParentIds.size() == 0) {
                FlowReport.reportAction("Parent ids is absent.", false);
            } else {
                FlowReport.reportAction("List of parents ids received.", true);
                if (listOfParentIds.get(0) == 0) {
                    FlowReport.nextStep();
                    FlowReport.addStep("Updating 'ParentFolderId' (null) field in DB.", "");
                    setOfTestCases.updateString("ParentFolderId", null);
                    setOfTestCases.updateRow();
                    FlowReport.reportAction("Field 'ParentFolderId' updated", true);
                } else {
                    FlowReport.nextStep();
                    FlowReport.addStep("Updating 'ParentFolderId' (" + listOfParentIds.get(0) + ") field in DB.", "");
                    setOfTestCases.updateString("ParentFolderId", listOfParentIds.get(0).toString());
                    setOfTestCases.updateRow();
                    FlowReport.reportAction("Field 'ParentFolderId' updated", true);
                }
            }

            if (setOfTestCases.getString("StoryName").contains("Name")) {
                FlowReport.nextStep();
                FlowReport.addStep("Updating 'StoryName' field in DB.", "");
                setOfTestCases.updateString("StoryName", genRandomEwerything("Name", ""));
                setOfTestCases.updateRow();
                FlowReport.reportAction("Field 'StoryName' updated", true);
            } else {
                FlowReport.reportAction("Something wrong with 'StoryName' field updating!", false);
            }

            if (setOfTestCases.getString("FileName").contains("Name")) {
                FlowReport.nextStep();
                FlowReport.addStep("Updating 'FileName' field in DB.", "");
                setOfTestCases.updateString("FileName", genRandomEwerything("Name", ""));
                setOfTestCases.updateRow();
                FlowReport.reportAction("Field 'FileName' updated", true);
            } else {
                FlowReport.reportAction("Something wrong with 'FileName' field updating!", false);
            }
            storyClient.getCreateStoryResponse(setOfTestCases);
            listOfStoryIds = getListOfStoryIdsByProjectKey(setOfTestCases.getString("ProjectKey"));
        }
        return listOfStoryIds;
    }

    // check that story have a scenario or create new
    public List<Integer> checkThatCsenarioIsExist(ResultSet setOfTestCases) throws Exception {
        ScenarioClient scenarioClient = new ScenarioClient();
        List<Integer> listOfScenarioIds = getScenarioIdsListByStoryId(setOfTestCases.getInt("StoryId"));
        if (listOfScenarioIds.size() == 0) {
            if (setOfTestCases.getString("ScenarioName").contains("Name")) {
                FlowReport.nextStep();
                FlowReport.addStep("Updating 'ScenarioName' field in DB.", "");
                setOfTestCases.updateString("ScenarioName", genRandomEwerything("Name", ""));
                setOfTestCases.updateRow();
                FlowReport.reportAction("Field 'ScenarioName' updated", true);
            } else {
                FlowReport.reportAction("Something wrong with 'ScenarioName' field updating!", false);
            }

            if (setOfTestCases.getString("TableName").contains("Table")) {
                FlowReport.nextStep();
                FlowReport.addStep("Updating 'TableName' field in DB.", "");
                setOfTestCases.updateString("TableName", genRandomEwerything("Table", ""));
                setOfTestCases.updateRow();
                FlowReport.reportAction("Field 'TableName' updated", true);
            } else {
                FlowReport.reportAction("Something wrong with 'TableName' field updating!", false);
            }
            scenarioClient.getCreateScenarioResponse(setOfTestCases);
            listOfScenarioIds = getScenarioIdsListByStoryId(setOfTestCases.getInt("StoryId"));
        }
        return listOfScenarioIds;
    }

    public ManagementTool getProjectManagementToolIdByProjectKey(String projectKey) {
        try {
            ManagementTool managementTool = new ManagementTool();
            ResultSet set = executeQueryToRelimeBase("SELECT [ProjectManagementTool].[ManagementTool_Id], [ManagementTool].Url\n" +
                    "FROM [RelimeDb].[dbo].[ProjectManagementTool]\n" +
                    "inner join [RelimeDb].[dbo].[Project] on [ProjectManagementTool].Project_Id = [Project].Id\n" +
                    "inner join [RelimeDb].[dbo].[ManagementTool] on [ManagementTool].Id = [ProjectManagementTool].ManagementTool_Id\n" +
                    "where [Project].ProjectKey = '" + projectKey + "'");
            while (set.next()) {
                managementTool.setId(set.getInt("ManagementTool_Id"));
                managementTool.setUrl(set.getString("Url"));
            }
            return managementTool;
        } catch (Exception ex) {
            return null;
        }
    }


    public TaskTrackers getTaskTrackerInfoFromDbByProjectKey(String projectKey) {
        try {
            TaskTrackers taskTrackers = new TaskTrackers();
            TaskTracker taskTracker = new TaskTracker();
            List<TaskTracker> taskTrackerList = new ArrayList<>();
            ResultSet set = executeQueryToRelimeBase("SELECT [ProjectManagementTool].ManagementTool_Id as Id,[ManagementToolType].Name,[ManagementTool].Url,[Account].Id as BelongToAccount from \n" +
                    "[RelimeDb].[dbo].[Project] join [RelimeDb].[dbo].[ProjectManagementTool] on [RelimeDb].[dbo].[Project].Id = [RelimeDb].[dbo].[ProjectManagementTool].Project_Id\n" +
                    "join [RelimeDb].[dbo].[ManagementTool] on [ManagementTool].Id = [ProjectManagementTool].ManagementTool_Id\n" +
                    "join [RelimeDb].[dbo].[ManagementToolType] on [ManagementToolType].id = [ManagementTool].Type_Id\n" +
                    "join [RelimeDb].[dbo].[Domain] on [RelimeDb].[dbo].[Project].Domain_Id = [Domain].Id\n" +
                    "join [RelimeDb].[dbo].[Account] on [RelimeDb].[dbo].[Account].User_Id = [Domain].Owner_id\n" +
                    "where [RelimeDb].[dbo].[Project].ProjectKey = '" + projectKey + "'");
            while (set.next()) {
                taskTracker.setName(set.getString("Name"));
                taskTracker.setUrl(set.getString("Url"));
                taskTracker.setId(set.getInt("Id"));
                taskTracker.setBelongsToAccount(set.getInt("BelongToAccount"));
                taskTrackerList.add(taskTracker);
            }
            taskTrackers.setTaskTrackers(taskTrackerList);
            return taskTrackers;
        } catch (Exception ex) {
            return null;
        }
    }

    public boolean validateTaskTrackersInfo(TaskTrackers responseObject, TaskTrackers databaseObject) {
        List<TaskTracker> responseList = responseObject.getTaskTrackers();
        List<TaskTracker> databaseList = databaseObject.getTaskTrackers();
        int validCount = 0;
        for (int i = 0; i < responseList.size(); i++) {
            for (int j = 0; j < databaseList.size(); j++) {
                if (responseList.get(i).getUrl().equals(databaseList.get(j).getUrl()) & responseList.get(i).getName().equals(databaseList.get(j).getName()) &
                        responseList.get(i).getId().equals(databaseList.get(j).getId()) & responseList.get(i).getBelongsToAccount().equals(databaseList.get(j).getBelongsToAccount())) {
                    validCount++;
                }
            }
        }
        if (validCount == responseList.size()) {
            return true;
        } else {
            return false;
        }
    }

    public boolean checkThatStoryHaveBackground(int storyId) {
        try {
            ResultSet set = executeQueryToRelimeBase("SELECT [Story].Id, [Scenario].ScenarioType_Id\n" +
                    "  FROM [RelimeDb].[dbo].[Story]\n" +
                    "  inner join [RelimeDb].[dbo].[Scenario] on [Story].Id = [Scenario].Story_Id\n" +
                    "  where [Story].Id = " + storyId + "\n" +
                    "  and [Scenario].ScenarioType_Id = 1");
            while (set.next()) {
                if (set.getInt("ScenarioType_Id") == 1) {
                    return true;
                }
            }
            return false;
        } catch (Exception ex) {
            return false;
        }
    }

    public Story getListOfJiraLinkedStoryByJiraStoryKey(String jiraStoryKey) {
        try {
            Story story = new Story();
            ResultSet set = executeQueryToRelimeBase("SELECT [Story].Id, [StoryManagementTool].[Key]\n" +
                    "FROM [RelimeDb].[dbo].[Story]\n" +
                    "inner join [RelimeDb].[dbo].[StoryManagementTool] on [StoryManagementTool].Story_Id = [Story].Id\n" +
                    "left join [RelimeDb].[dbo].[Tree] on [Tree].[Id] = [Story].[Tree_Id]\n" +
                    "left join [RelimeDb].[dbo].[Project] on [Tree].Project_Id = [Project].Id\n" +
                    "where [StoryManagementTool].[Key] = '" + jiraStoryKey + "'");
            while (set.next()) {
                story.setKey(set.getString("Key"));
                story.setStoryId(set.getInt("Id"));
            }
            return story;
        } catch (Exception ex) {
            return null;
        }
    }

    public Integer getJiraAccountIdByProjectKey(String projectKey) {
        try {
            Integer accountId = 0;
            ResultSet set = executeQueryToRelimeBase("SELECT [Account].Id\n" +
                    "FROM [RelimeDb].[dbo].[Account]\n" +
                    "inner join [RelimeDb].[dbo].[User] on [User].UserId = [Account].User_Id\n" +
                    "inner join [RelimeDb].[dbo].[Domain] on [Domain].Owner_Id = [User].UserId\n" +
                    "inner join [RelimeDb].[dbo].[Project] on [Project].Domain_Id = [Domain].Id\n" +
                    "where [Project].ProjectKey = '" + projectKey + "'\n" +
                    "and [Account].ManagementTool_Id = 6");
            while (set.next()) {
                accountId = set.getInt("Id");
            }
            return accountId;
        } catch (Exception ex) {
            return null;
        }
    }

    public boolean deleteGitRepositoryLinkFromRelimeDB(String domainName, String projectKey) {
        try {
            ResultSet set = executeQueryToRelimeBase("delete from vcs\n" +
                    "where id = (select vcs.id from vcs \n" +
                    "join Project pr on (vcs.Project_id = pr.Id)\n" +
                    "join Domain dm on (pr.Domain_id = dm.Id)\n" +
                    "where dm.Name = '" + domainName + "' and pr.ProjectKey = '" + projectKey + "')");
            return true;
        } catch (Exception ex) {
            return false;
        }
    }

    public JiraStory getJiraStoryInfoFromRelimeDbByJiraKey(String jiraKey) {
        try {
            JiraStory jiraStory = new JiraStory();
            ResultSet set = executeQueryToRelimeBase("SELECT [Story].Id, [Story].Name, [Story].[Description], [StoryManagementTool].ManagementTool_Id, [StoryManagementTool].[Key], [Tree].Name as [FileName], [Tree].ParentId,\n" +
                    "[Scenario].[Description] as ScenarioDescription, [Scenario].Story_Id, [Scenario].Name as ScenarioName, [ScenarioType].[Type]\n" +
                    "FROM [RelimeDb].[dbo].[Story]\n" +
                    "inner join [RelimeDb].[dbo].[StoryManagementTool] on [Story].Id = [StoryManagementTool].Story_Id\n" +
                    "inner join [RelimeDb].[dbo].[Tree] on [Story].Tree_Id = [Tree].Id\n" +
                    "inner join [RelimeDb].[dbo].[Scenario] on [Story].Id = [Scenario].Story_Id\n" +
                    "left join [RelimeDb].[dbo].[ScenarioType] on [Scenario].ScenarioType_Id = [ScenarioType].Id\n" +
                    "where [StoryManagementTool].ManagementTool_Id = 6\n" +
                    "and [StoryManagementTool].[Key] = '" + jiraKey + "'\n" +
                    "order by [ScenarioType].[Type]");
            while (set.next()) {
                jiraStory.setName(set.getString("Name"));
                jiraStory.setFileName(set.getString("FileName"));
                jiraStory.setKey(set.getString("Key"));
                jiraStory.setDescription(set.getString("Description"));
                jiraStory.setManagementToolId(set.getInt("ManagementTool_Id"));
                jiraStory.setParentFolderId(set.getInt("ParentId"));
                jiraStory.setTags(getStoryTagsListFromRelimeDb(jiraKey, set.getInt("Id")));
                jiraStory.setIsJiraLinkedStory(false);
                jiraStory.setParsedScenarios(getParsedScenario(set));
            }
            return jiraStory;
        } catch (Exception ex) {
            return null;
        }
    }

    public List<ParsedScenario> getParsedScenario(ResultSet set) throws SQLException {
        try {
            List<ParsedScenario> parsedScenarioList = new ArrayList<>();
            do {
                ParsedScenario parsedScenario = new ParsedScenario();
                parsedScenario.setType(set.getString("Type"));
                parsedScenario.setStoryId(set.getInt("Story_Id"));
                parsedScenario.setDescription(set.getString("ScenarioDescription"));
                parsedScenario.setIsBriefInfo(false);
                parsedScenario.setName(set.getString("ScenarioName"));
                parsedScenario.setTags(getScenarioTagsListFromRelimeDb(parsedScenario.getName(), parsedScenario.getStoryId()));
                parsedScenario.setExampleTable(null);
                parsedScenario.setStepText(getScenarioStepTextFromRelimeDb(parsedScenario.getName(), parsedScenario.getStoryId()));

                parsedScenarioList.add(parsedScenario);
            } while (set.next());
            return parsedScenarioList;
        } catch (Exception ex) {
            return null;
        }
    }

    public List<String> getScenarioTagsListFromRelimeDb(String scenarioName, Integer storyId) {
        try {
            List<String> tagsList = new ArrayList<>();
            ResultSet set = executeQueryToRelimeBase("SELECT [Story].Name, [Scenario].Story_Id, [Scenario].Name as ScenarioName, ScenarioTag.Name as ScenarioTag\n" +
                    "FROM [RelimeDb].[dbo].[Story]\n" +
                    "inner join [RelimeDb].[dbo].[Scenario] on [Story].Id = [Scenario].Story_Id\n" +
                    "left join [RelimeDb].[dbo].[TagScenario] on [TagScenario].Scenario_Id = [Scenario].Id\n" +
                    "left join [RelimeDb].[dbo].[Tag] as ScenarioTag on ScenarioTag.Id = [TagScenario].Tag_Id\n" +
                    "where [Scenario].Name = '" + scenarioName + "'\n" +
                    "and  [Story].Id = " + storyId + "");
            while (set.next()) {
                if (!(set.getString("ScenarioTag") == null)) {
                    tagsList.add(set.getString("ScenarioTag"));
                } else {
                    return new ArrayList<>();
                }
            }
            return tagsList;
        } catch (Exception ex) {
            return null;
        }
    }

    public List<Object> getStoryTagsListFromRelimeDb(String jiraKey, Integer storyId) {
        try {
            List<Object> tagsList = new ArrayList<>();
            ResultSet set = executeQueryToRelimeBase("SELECT  [Tag].Name as StoryTag, [Tree].Name as FileName\n" +
                    "FROM [RelimeDb].[dbo].[Story]\n" +
                    "inner join [RelimeDb].[dbo].[Tree] on [Story].Tree_Id = [Tree].Id\n" +
                    "left join [RelimeDb].[dbo].[TagStory] on [TagStory].Story_Id = [Story].Id\n" +
                    "left join [RelimeDb].[dbo].[Tag] on [Tag].Id = [TagStory].Tag_Id\n" +
                    "where [Tree].Name = '" + jiraKey + "'\n" +
                    "and  [Story].Id = " + storyId + "");
            while (set.next()) {
                if (!(set.getString("StoryTag") == null)) {
                    tagsList.add(set.getString("StoryTag"));
                } else {
                    return new ArrayList<>();
                }
            }
            return tagsList;
        } catch (Exception ex) {
            return null;
        }
    }

    public List<String> getScenarioStepTextFromRelimeDb(String scenarioName, Integer storyId) {
        try {
            List<String> stepTextList = new ArrayList<>();
            ResultSet set = executeQueryToRelimeBase("SELECT [Scenario].Name as ScenarioName, [StepLine].StepText\n" +
                    "FROM [RelimeDb].[dbo].[Story]\n" +
                    "inner join [RelimeDb].[dbo].[Scenario] on [Story].Id = [Scenario].Story_Id\n" +
                    "left join [RelimeDb].[dbo].[StepLine] on [Scenario].Id = [StepLine].Scenario_Id\n" +
                    "where [Scenario].Name = '" + scenarioName + "'\n" +
                    "and  [Story].Id = " + storyId + "");
            while (set.next()) {
                stepTextList.add(set.getString("StepText"));
            }
            return stepTextList;
        } catch (Exception ex) {
            return null;
        }
    }

}