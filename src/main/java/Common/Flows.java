package Common;

import Clients.*;
import Helpers.Classes.*;
import arp.FlowReport;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONObject;

import java.lang.reflect.Method;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.List;

import static Common.SessionAuthorizationInfo.token;
import static Common.SessionAuthorizationInfo.sanboxToken;


/**
 * Created by logovskoy on 10/27/2016.
 */
public class Flows {

    private String tableName = "";
    private String responseMethodName = "";
    private String inputDataCreationMethodName = "";
    private Object webServiceClient = null;
    private Object inputDataClient = null;
    private Statement testCaseDatabaseStatement = null;
    private Object responseType = null;
    public Class clientClass = null;
    public Class testInitiatorClass = null;
    public Methods methods = new Methods();
    public AuthClient authClient = new AuthClient();
    public Domain domain = new Domain();
    public StoryClient storyClient = new StoryClient();
    public ScenarioClient scenarioClient = new ScenarioClient();
    public ProjectClient projectClient = new ProjectClient();
    public AccountClient accountClient = new AccountClient();

    public Flows() {
    }

    public Flows(String tableName, Class clientClass, Class testInitiatorClass, String responseMethodName) {
        this.tableName = tableName;
        this.clientClass = clientClass;
        this.testInitiatorClass = testInitiatorClass;
        this.responseMethodName = responseMethodName;
    }


    public void authorizationCheck(ResultSet setOfTestCases) {
        try {
            FlowReport.initFlow(setOfTestCases, testInitiatorClass);
            while (setOfTestCases.next()) {
                try {
                    FlowReport.startTest(setOfTestCases.getString("TestCaseName"));
                    FlowReport.addStep("Check that authorization with valid creds is possible", "");
                    Method method = clientClass.getDeclaredMethod(responseMethodName, ResultSet.class);
                    Object obj = clientClass.newInstance();
                    String response = (String) method.invoke(obj, setOfTestCases);
                    FlowReport.reportAction("Authorization response received", true);
                    methods.saveAndAddResponse(response);
                    FlowReport.nextStep();
                    FlowReport.addStep("Validation of response...", "");
                    String expected = setOfTestCases.getString("ExpectedMessage");
                    if (response.toLowerCase().contains(expected.toLowerCase())) {
                        FlowReport.reportAction("Validation done successfully.", true);
                    } else {
                        FlowReport.reportAction("Validation done unsuccessfully.", false);
                    }
                } catch (Throwable ex) {
                    FlowReport.reportAction(ex.getMessage() == null ? "Response was not recieved" : ex.getMessage(), false);
                } finally {
                    methods.logOut();
                    FlowReport.finishTest();
                }
            }
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
    }

    public void registrationCheck(ResultSet setOfTestCases) throws Exception {
        try {
            FlowReport.initFlow(setOfTestCases, testInitiatorClass);
            while (setOfTestCases.next()) {
                try {
                    FlowReport.startTest(setOfTestCases.getString("TestCaseName"));
                    FlowReport.addStep("Check that registration with valid userName and email is possible", "");
                    Method method = clientClass.getDeclaredMethod(responseMethodName, ResultSet.class);
                    Object obj = clientClass.newInstance();
                    if (setOfTestCases.getString("email").contains("dummyEmail")) {
                        setOfTestCases.updateString("email", methods.genRandomEwerything("@dummyEmail.com", ""));
                        setOfTestCases.updateRow();
                        FlowReport.reportAction("Email updated in DB successfully", true);
                    }
                    String response = (String) method.invoke(obj, setOfTestCases);
                    FlowReport.reportAction("Registration response recieved", true);
                    methods.saveAndAddResponse(response);
                    FlowReport.nextStep();
                    FlowReport.addStep("Validating recieved data", "");
                    JSONObject jsonObj = new JSONObject(response);
                    token = jsonObj.getString("token");
                    UserBasicInfo validationInfoFromRelimeDB = methods.getRegistredUserByEmail(setOfTestCases.getString("email"));
                    try {
                        String reportString = "";
                        if (!jsonObj.getString("token").equals(validationInfoFromRelimeDB.getToken())) {
                            reportString += "Tokens are different.";
                        }
                        if (!jsonObj.getString("firstName").equals(validationInfoFromRelimeDB.getFirstName())) {
                            reportString += "First Names are different";
                        }
                        if (!jsonObj.getString("lastName").equals(validationInfoFromRelimeDB.getLastName())) {
                            reportString += "Last Names are different";
                        }
                        if (!reportString.equals("")) {
                            FlowReport.reportAction(reportString, false);
                        } else {
                            FlowReport.reportAction("Validated. Registred user is present in DB", true);
                        }
                    } catch (Exception e) {
                        FlowReport.reportAction(e.getMessage(), false);
                    }
                } catch (Throwable ex) {
                    FlowReport.reportAction(ex.getMessage() == null ? "Response was not recieved" : ex.getMessage(), false);
                } finally {
                    methods.logOut();
                    FlowReport.finishTest();
                }
            }
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
    }

    public void validationCheck(ResultSet setOfTestCases) throws Exception {
        try {
            FlowReport.initFlow(setOfTestCases, testInitiatorClass);
            while (setOfTestCases.next()) {
                try {
                    FlowReport.startTest(setOfTestCases.getString("TestCaseName"));
                    Method method = clientClass.getDeclaredMethod(responseMethodName, ResultSet.class);
                    Object obj = clientClass.newInstance();
                    FlowReport.addStep("Getting response from client method", "");
                    String response = (String) method.invoke(obj, setOfTestCases);
                    JSONObject jsonObj = new JSONObject(response);
                    FlowReport.reportAction("Validation response received", true);
                    methods.saveAndAddResponse(response);
                    FlowReport.nextStep();
                    FlowReport.addStep("Check that response is complies to expected result", "");
                    if (jsonObj.getBoolean("result") == Boolean.parseBoolean(setOfTestCases.getString("ExpectedMessage"))) {
                        FlowReport.reportAction("Validated. Field is unique.", true);
                    } else {
                        FlowReport.reportAction("Fail. Something wrong with response validation.", false);
                    }
                } catch (Exception e) {
                    FlowReport.reportAction(e.getMessage() == null ? "Response was not recieved" : e.getMessage(), false);
                } finally {
                    FlowReport.finishTest();
                }
            }
        } catch (Exception ex) {
            FlowReport.reportAction(ex.getMessage() == null ? "" : ex.getMessage(), false);
        }
    }

    public void changePasswordCheck(ResultSet setOfTestCases) {
        try {
            FlowReport.initFlow(setOfTestCases, testInitiatorClass);
            while (setOfTestCases.next()) {
                try {
                    FlowReport.startTest(setOfTestCases.getString("TestCaseName"));
                    FlowReport.addStep("Check that password is changeable.", "");
                    Method method = clientClass.getDeclaredMethod(responseMethodName, ResultSet.class);
                    Object obj = clientClass.newInstance();
                    if (setOfTestCases.getString("email").contains("dummyEmail")) {
                        setOfTestCases.updateString("email", methods.genRandomEwerything("@dummyEmail.com", ""));
                        setOfTestCases.updateRow();
                        FlowReport.reportAction("Email updated", true);
                    } else FlowReport.reportAction("Something wrong with email updating!", false);
                    FlowReport.nextStep();
                    FlowReport.addStep("Registration of new user and getting new token", "");
                    try {
                        token = new JSONObject(authClient.getRegistrationResponse(setOfTestCases)).getString("token");
                        FlowReport.reportAction("Registration done. Token received.", true);
                    } catch (NullPointerException e) {
                        FlowReport.reportAction(e.getMessage(), false);
                    }
                    FlowReport.nextStep();
                    FlowReport.addStep("Update passwords in DB.", "");
                    if (setOfTestCases.getString("Password").contains("password")) {
                        setOfTestCases.updateString("NewPassword", methods.genRandomEwerything("password", ""));
                        setOfTestCases.updateRow();
                        setOfTestCases.updateString("ConfirmNewPassword", setOfTestCases.getString("NewPassword"));
                        setOfTestCases.updateRow();
                        FlowReport.reportAction("Passwords updated.", true);
                    } else FlowReport.reportAction("Something wrong with passwords updating!", false);
                    FlowReport.nextStep();
                    FlowReport.addStep("Receive the response from client method.", "");
                    String response = (String) method.invoke(obj, setOfTestCases);
                    FlowReport.reportAction("Response about changing of password recieved", true);
                    methods.saveAndAddResponse(response);
                    FlowReport.nextStep();
                    FlowReport.addStep("Validating recieved data", "");
                    JSONObject jsonObj = new JSONObject(response);
                    UserBasicInfo validationInfoFromRelimeDB = methods.getRegistredUserByEmail(setOfTestCases.getString("email"));
                    try {
                        String reportString = "";
                        if (!jsonObj.getString("token").equals(validationInfoFromRelimeDB.getToken())) {
                            reportString += "Tokens are different";
                        }
                        if (!reportString.equals("")) {
                            FlowReport.reportAction(reportString, false);
                        } else {
                            FlowReport.reportAction("Validated. Password is changed successfully.", true);
                        }
                    } catch (Exception e) {
                        FlowReport.reportAction(e.getMessage(), false);
                    }
                } catch (Throwable ex) {
                    FlowReport.reportAction(ex.getMessage() == null ? "Response wasn`t returned" : ex.getMessage(), false);
                } finally {
                    methods.logOut();
                    FlowReport.finishTest();
                }
            }
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
    }

    public void changeBasicInfoCheck(ResultSet setOfTestCases) {
        try {
            FlowReport.initFlow(setOfTestCases, testInitiatorClass);
            while (setOfTestCases.next()) {
                try {
                    FlowReport.startTest(setOfTestCases.getString("TestCaseName"));
                    FlowReport.addStep("Check that basic info of user is changeable.", "");
                    Method method = clientClass.getDeclaredMethod(responseMethodName, ResultSet.class);
                    Object obj = clientClass.newInstance();
                    try {
                        methods.authorizeWithSpecificCreds(setOfTestCases.getString("Login"), setOfTestCases.getString("Password"));
                        FlowReport.reportAction("Authorization done. Token received.", true);
                    } catch (NullPointerException e) {
                        FlowReport.reportAction(e.getMessage(), false);
                    }

                    FlowReport.addStep("Update firstName and lastName in DB.", "");
                    if (setOfTestCases.getString("FirstName").contains("Name") && setOfTestCases.getString("LastName").contains("Name")) {
                        setOfTestCases.updateString("FirstName", methods.genRandomEwerything("Name", ""));
                        setOfTestCases.updateString("LastName", methods.genRandomEwerything("Name", ""));
                        setOfTestCases.updateRow();
                        FlowReport.reportAction("Names updated.", true);
                    } else FlowReport.reportAction("Something wrong with names updating!", false);

                    FlowReport.addStep("Receive the response from client method.", "");
                    String response = (String) method.invoke(obj, setOfTestCases);
                    FlowReport.reportAction("Response about changing of basic info recieved", true);
                    methods.saveAndAddResponse(response);
                    FlowReport.nextStep();
                    FlowReport.addStep("Validating recieved data", "");

                    if (response != null) {
                        UserBasicInfo validationInfoFromRelimeDB = methods.getRegistredUserByEmail(setOfTestCases.getString("Login"));
                        try {
                            String reportString = "";
                            if (!setOfTestCases.getString("FirstName").equals(validationInfoFromRelimeDB.getFirstName())) {
                                reportString += "First names are different";
                            }
                            if (!setOfTestCases.getString("LastName").equals(validationInfoFromRelimeDB.getLastName())) {
                                reportString += "Last names are different";
                            }
                            if (!reportString.equals("")) {
                                FlowReport.reportAction(reportString, false);
                            } else {
                                FlowReport.reportAction("Validated. Basic info is changed successfully.", true);
                            }
                        } catch (Exception e) {
                            FlowReport.reportAction(e.getMessage(), false);
                        }
                    }
                } catch (Throwable ex) {
                    FlowReport.reportAction(ex.getMessage() == null ? "Response wasn`t returned" : ex.getMessage(), false);
                } finally {
                    methods.logOut();
                    FlowReport.finishTest();
                }
            }
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
    }

    public void checkRetrieveBasicInfo(ResultSet setOfTestCases) {
        try {
            FlowReport.initFlow(setOfTestCases, testInitiatorClass);
            while (setOfTestCases.next()) {
                try {
                    FlowReport.startTest(setOfTestCases.getString("TestCaseName"));
                    FlowReport.addStep("Check that basic info of user is changeable.", "");
                    Method method = clientClass.getDeclaredMethod(responseMethodName, ResultSet.class);
                    Object obj = clientClass.newInstance();
                    try {
                        methods.authorizeWithSpecificCreds(setOfTestCases.getString("Login"), setOfTestCases.getString("Password"));
                        FlowReport.reportAction("Authorization done. Token received.", true);
                    } catch (NullPointerException e) {
                        FlowReport.reportAction(e.getMessage(), false);
                    }

                    FlowReport.addStep("Receive the response from client method.", "");
                    String response = (String) method.invoke(obj, setOfTestCases);
                    ObjectMapper mapper = new ObjectMapper();
                    UserBasicInfo responseObject = mapper.readValue(response, UserBasicInfo.class);
                    FlowReport.reportAction("Response about changing of basic info recieved", true);
                    methods.saveAndAddResponse(response);
                    FlowReport.nextStep();
                    FlowReport.addStep("Validating recieved data", "");

                    if (response != null) {
                        UserBasicInfo databaseObject = methods.getRegistredUserByEmail(setOfTestCases.getString("Login"));
                        try {
                            if (methods.validateUserBasicInfo(responseObject, databaseObject)) {
                                FlowReport.reportAction("Validation done!", true);
                            } else {
                                FlowReport.reportAction("Validation failed!", false);
                            }
                        } catch (Exception e) {
                            FlowReport.reportAction(e.getMessage(), false);
                        }
                    }
                } catch (Throwable ex) {
                    FlowReport.reportAction(ex.getMessage() == null ? "Response wasn`t returned" : ex.getMessage(), false);
                } finally {
                    methods.logOut();
                    FlowReport.finishTest();
                }
            }
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
    }

    public void createDomainCheck(ResultSet setOfTestCases) {
        try {
            FlowReport.initFlow(setOfTestCases, testInitiatorClass);
            while (setOfTestCases.next()) {
                try {
                    FlowReport.startTest(setOfTestCases.getString("TestCaseName"));
                    FlowReport.addStep("Check that user can create new domain.", "");
                    Method method = clientClass.getDeclaredMethod(responseMethodName, ResultSet.class);
                    Object obj = clientClass.newInstance();

                    try {
                        methods.authorizeWithSpecificCreds(setOfTestCases.getString("Login"), setOfTestCases.getString("Password"));
                        FlowReport.reportAction("Authorization done. Token received.", true);
                    } catch (NullPointerException e) {
                        FlowReport.reportAction(e.getMessage(), false);
                    }

                    List<String> oldListOfActiveDomains = methods.getListOfActiveDomainsByEmail(setOfTestCases.getString("Login"));

                    FlowReport.addStep("Update Name of domain and lastName in DB.", "");
                    if (setOfTestCases.getString("DomainName").contains("Name")) {
                        setOfTestCases.updateString("DomainName", methods.genRandomEwerything("Name", ""));
                        setOfTestCases.updateRow();
                        FlowReport.reportAction("Domain names updated.", true);
                    } else FlowReport.reportAction("Something wrong with domains names updating!", false);
                    FlowReport.addStep("Receive the response from client method.", "");
                    String response = (String) method.invoke(obj, setOfTestCases);
                    FlowReport.reportAction("Response about domain creation recieved ", true);
                    methods.saveAndAddResponse(response);
                    List<String> newListOfActiveDomains = methods.getListOfActiveDomainsByEmail(setOfTestCases.getString("Login"));

                    FlowReport.nextStep();
                    FlowReport.addStep("Validating recieved data", "");
                    if (response != null) {
                        try {
                            String reportString = "";
                            if (!(oldListOfActiveDomains.size() < newListOfActiveDomains.size())) {
                                reportString += "Domain didn't add.";
                            }
                            if (!newListOfActiveDomains.contains(setOfTestCases.getString("DomainName"))) {
                                reportString += "Domain names are different";
                            }
                            if (!reportString.equals("")) {
                                FlowReport.reportAction(reportString, false);
                            } else {
                                FlowReport.reportAction("Validated. Domain was created successfully.", true);
                            }
                        } catch (Exception e) {
                            FlowReport.reportAction(e.getMessage(), false);
                        }
                    }
                } catch (Throwable ex) {
                    FlowReport.reportAction(ex.getMessage() == null ? "Response wasn`t returned" : ex.getMessage(), false);
                } finally {
                    methods.logOut();
                    FlowReport.finishTest();
                }
            }
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
    }

    public void hitsCheck(ResultSet setOfTestCases) {
        try {
            FlowReport.initFlow(setOfTestCases, testInitiatorClass);
            while (setOfTestCases.next()) {
                try {
                    FlowReport.startTest(setOfTestCases.getString("TestCaseName"));
                    FlowReport.addStep("Check hits.", "");
                    Method method = clientClass.getDeclaredMethod(responseMethodName, ResultSet.class);
                    Object obj = clientClass.newInstance();

                    try {
                        methods.authorizeWithSpecificCreds(setOfTestCases.getString("Login"), setOfTestCases.getString("Password"));
                        FlowReport.reportAction("Authorization done. Token received.", true);
                    } catch (NullPointerException e) {
                        FlowReport.reportAction(e.getMessage(), false);
                    }

                    //if domains is absent - create new
                    List<String> activeDomainsList = methods.checkThatDomainsIsExist(setOfTestCases);

                    FlowReport.nextStep();
                    FlowReport.addStep("Updating 'DomainName' field in DB.", "");
                    setOfTestCases.updateString("DomainName", activeDomainsList.get(0));
                    setOfTestCases.updateRow();
                    FlowReport.reportAction("Field 'DomainName' updated", true);

                    Domains subAndOwnedDomainList = methods.getListOfOwnAndSubscribedDomainsByEmail(setOfTestCases.getString("Login"), "false");

                    FlowReport.addStep("Receive the response from client method.", "");
                    String response = (String) method.invoke(obj, setOfTestCases);
                    FlowReport.reportAction("Response about hits list recieved ", true);
                    methods.saveAndAddResponse(response);
                    FlowReport.nextStep();
                    FlowReport.addStep("Validating recieved data", "");
                    if (response != null) {
                        List<String> listOfHits = methods.getListOfDomainsFromHitsApi(response);
                        int countOfEquals = 0;
                        try {
                            String reportString = "";
                            if (!(subAndOwnedDomainList.getDomains().size() == listOfHits.size())) {
                                reportString += "Something wrong with hits list. Size.";
                            }
                            for (int i = 0; i < listOfHits.size(); i++) {
                                if (subAndOwnedDomainList.getDomains().get(i).getName().equals(listOfHits.get(i))) {
                                    countOfEquals++;
                                }
                            }
                            if (!(countOfEquals == listOfHits.size() & countOfEquals == subAndOwnedDomainList.getDomains().size())) {
                                reportString += "Something wrong with hits list.";
                            }
                            if (!reportString.equals("")) {
                                FlowReport.reportAction(reportString, false);
                            } else {
                                FlowReport.reportAction("Validated. Hits was received successfully.", true);
                            }
                        } catch (Exception e) {
                            FlowReport.reportAction(e.getMessage(), false);
                        }
                    }
                } catch (Throwable ex) {
                    FlowReport.reportAction(ex.getMessage() == null ? "Response wasn`t returned" : ex.getMessage(), false);
                } finally {
                    methods.logOut();
                    FlowReport.finishTest();
                }
            }
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
    }

    public void deleteDomainCheck(ResultSet setOfTestCases) {
        try {
            FlowReport.initFlow(setOfTestCases, testInitiatorClass);
            while (setOfTestCases.next()) {
                try {
                    FlowReport.startTest(setOfTestCases.getString("TestCaseName"));
                    FlowReport.addStep("Check that domains was deleted from domains list", "");
                    Method method = clientClass.getDeclaredMethod(responseMethodName, ResultSet.class);
                    Object obj = clientClass.newInstance();

                    try {
                        methods.authorizeWithSpecificCreds(setOfTestCases.getString("Login"), setOfTestCases.getString("Password"));
                        FlowReport.reportAction("Authorization done. Token received.", true);
                    } catch (NullPointerException e) {
                        FlowReport.reportAction(e.getMessage(), false);
                    }

                    //if domains is absent - create new
                    List<String> activeDomainsList = methods.checkThatDomainsIsExist(setOfTestCases);

                    List<Integer> domainIds = methods.getIdsOfActiveDomainsByEmail(setOfTestCases.getString("Login"));
                    for (int i = 0; i < domainIds.size(); i++) {
                        FlowReport.nextStep();
                        FlowReport.addStep("Updating 'DomainId' field in DB.", "");
                        setOfTestCases.updateString("DomainId", domainIds.get(i).toString());
                        setOfTestCases.updateRow();
                        FlowReport.reportAction("Field 'DomainId' updated", true);

                        FlowReport.nextStep();
                        FlowReport.addStep("Receive the response from client method.", "");
                        String response = (String) method.invoke(obj, setOfTestCases);
                        FlowReport.reportAction("Response about domains deleting recieved ", true);
                        methods.saveAndAddResponse(response);

                        FlowReport.nextStep();
                        FlowReport.addStep("Validating recieved data", "");
                        if (response != null) {
                            List<Integer> newDomainIds = methods.getIdsOfActiveDomainsByEmail(setOfTestCases.getString("Login"));
                            try {
                                String reportString = "";
                                if (!(newDomainIds.size() < domainIds.size())) {
                                    reportString += "Domain is not deleted. List is not changed.";
                                }
                                if (newDomainIds.size() != 0 && newDomainIds.contains(domainIds.get(i))) {
                                    reportString += "Domain is not deleted. List still have an id of 'deleted' domain.";
                                }
                                if (!reportString.equals("")) {
                                    FlowReport.reportAction(reportString, false);
                                } else {
                                    FlowReport.reportAction("Validated. Domain deleted successfully.", true);
                                }
                            } catch (Exception e) {
                                FlowReport.reportAction(e.getMessage(), false);
                            }
                        }
                    }
                } catch (Throwable ex) {
                    FlowReport.reportAction(ex.getMessage() == null ? "Response wasn`t returned" : ex.getMessage(), false);
                } finally {
                    methods.logOut();
                    FlowReport.finishTest();
                }
            }
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
    }

    public void updateDomainCheck(ResultSet setOfTestCases) {
        try {
            FlowReport.initFlow(setOfTestCases, testInitiatorClass);
            while (setOfTestCases.next()) {
                try {
                    FlowReport.startTest(setOfTestCases.getString("TestCaseName"));
                    FlowReport.addStep("Check that domains was updated.", "");
                    Method method = clientClass.getDeclaredMethod(responseMethodName, ResultSet.class);
                    Object obj = clientClass.newInstance();

                    try {
                        methods.authorizeWithSpecificCreds(setOfTestCases.getString("Login"), setOfTestCases.getString("Password"));
                        FlowReport.reportAction("Authorization done. Token received.", true);
                    } catch (NullPointerException e) {
                        FlowReport.reportAction(e.getMessage(), false);
                    }

                    //if domains is absent - create new
                    List<String> activeDomainsList = methods.checkThatDomainsIsExist(setOfTestCases);

                    List<Integer> domainIds = methods.getIdsOfActiveDomainsByEmail(setOfTestCases.getString("Login"));
                    for (int i = 0; i < domainIds.size(); i++) {
                        FlowReport.nextStep();
                        FlowReport.addStep("Updating 'RequestId' field in DB.", "");
                        setOfTestCases.updateString("RequestId", domainIds.get(i).toString());
                        setOfTestCases.updateRow();
                        FlowReport.reportAction("Field 'RequestId' updated", true);
                        FlowReport.nextStep();

                        List<String> oldListOfDomainNames = methods.getListOfActiveDomainsByEmail(setOfTestCases.getString("Login"));

                        FlowReport.addStep("Update Name of domain and lastName in DB.", "");
                        if (setOfTestCases.getString("DomainName").contains("Name")) {
                            setOfTestCases.updateString("DomainName", methods.genRandomEwerything("Name", ""));
                            setOfTestCases.updateRow();
                            FlowReport.reportAction("Domain names updated.", true);
                        } else FlowReport.reportAction("Something wrong with domains names updating!", false);

                        FlowReport.nextStep();
                        FlowReport.addStep("Receive the response from client method.", "");
                        String response = (String) method.invoke(obj, setOfTestCases);
                        FlowReport.reportAction("Response about domains updating recieved ", true);
                        methods.saveAndAddResponse(response);
                        FlowReport.nextStep();
                        FlowReport.addStep("Validating recieved data", "");
                        if (response != null) {
                            List<String> newListOfDomainNames = methods.getListOfActiveDomainsByEmail(setOfTestCases.getString("Login"));
                            try {
                                String reportString = "";
                                if (oldListOfDomainNames.equals(newListOfDomainNames)) {
                                    reportString += "Domain name is not updated. List is not changed.";
                                }
                                if (oldListOfDomainNames.get(i).contains(newListOfDomainNames.get(i))) {
                                    reportString += "Domain name is not updated.";
                                }
                                if (!reportString.equals("")) {
                                    FlowReport.reportAction(reportString, false);
                                } else {
                                    FlowReport.reportAction("Validated. Domain updated successfully.", true);
                                }
                            } catch (Exception e) {
                                FlowReport.reportAction(e.getMessage(), false);
                            }
                        }
                    }
                } catch (Throwable ex) {
                    FlowReport.reportAction(ex.getMessage() == null ? "Response wasn`t returned" : ex.getMessage(), false);
                } finally {
                    methods.logOut();
                    FlowReport.finishTest();
                }
            }
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
    }

    public void retrieveDomainCheck(ResultSet setOfTestCases) {
        try {
            FlowReport.initFlow(setOfTestCases, testInitiatorClass);
            while (setOfTestCases.next()) {
                try {
                    FlowReport.startTest(setOfTestCases.getString("TestCaseName"));
                    FlowReport.addStep("Check that domains info can be retrieved.", "");
                    Method method = clientClass.getDeclaredMethod(responseMethodName, ResultSet.class);
                    Object obj = clientClass.newInstance();

                    try {
                        methods.authorizeWithSpecificCreds(setOfTestCases.getString("Login"), setOfTestCases.getString("Password"));
                        FlowReport.reportAction("Authorization done. Token received.", true);
                    } catch (NullPointerException e) {
                        FlowReport.reportAction(e.getMessage(), false);
                    }

                    //if domains is absent - create new
                    methods.checkThatDomainsIsExist(setOfTestCases);

                    List<Integer> domainIds = methods.getIdsOfActiveDomainsByEmail(setOfTestCases.getString("Login"));

                    for (int i = 0; i < domainIds.size(); i++) {
                        FlowReport.nextStep();
                        FlowReport.addStep("Updating 'DomainId' (" + domainIds.get(i).toString() + ") field in DB.", "");
                        setOfTestCases.updateString("DomainId", domainIds.get(i).toString());
                        setOfTestCases.updateRow();
                        FlowReport.reportAction("Field 'DomainId' updated", true);

                        List<String> listOfDomainNames = methods.getNamesOfActiveDomainsById(setOfTestCases.getString("DomainId"));

                        FlowReport.nextStep();
                        FlowReport.addStep("Update Name (" + listOfDomainNames.get(0) + ") of domain in DB.", "");
                        setOfTestCases.updateString("DomainName", listOfDomainNames.get(0));
                        setOfTestCases.updateRow();
                        FlowReport.reportAction("Domain name updated.", true);

                        FlowReport.nextStep();
                        FlowReport.addStep("Receive the response from client method.", "");
                        String response = (String) method.invoke(obj, setOfTestCases);
                        FlowReport.reportAction("Response about domains retrieve recieved ", true);
                        methods.saveAndAddResponse(response);

                        FlowReport.nextStep();
                        FlowReport.addStep("Validating recieved data", "");
                        if (response != null) {
                            try {
                                String reportString = "";
                                if (!response.contains(setOfTestCases.getString("DomainName"))) {
                                    reportString += "Domain is not retrieved. Response doesn't have name of domain.";
                                }
                                if (!response.contains(setOfTestCases.getString("DomainId"))) {
                                    reportString += "Domain is not retrieved. Response doesn't have id of domain.";
                                }
                                if (!reportString.equals("")) {
                                    FlowReport.reportAction(reportString, false);
                                } else {
                                    FlowReport.reportAction("Validated. Domain retrieved successfully.", true);
                                }
                            } catch (Exception e) {
                                FlowReport.reportAction(e.getMessage(), false);
                            }
                        }
                    }
                } catch (Throwable ex) {
                    FlowReport.reportAction(ex.getMessage() == null ? "Response wasn`t returned" : ex.getMessage(), false);
                } finally {
                    methods.logOut();
                    FlowReport.finishTest();
                }
            }
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
    }

    public void ownedDomainsCheck(ResultSet setOfTestCases) {
        try {
            FlowReport.initFlow(setOfTestCases, testInitiatorClass);
            while (setOfTestCases.next()) {
                try {
                    FlowReport.startTest(setOfTestCases.getString("TestCaseName"));
                    FlowReport.addStep("Check that API returns info about active domains", "");
                    Method method = clientClass.getDeclaredMethod(responseMethodName, ResultSet.class);
                    Object obj = clientClass.newInstance();

                    try {
                        methods.authorizeWithSpecificCreds(setOfTestCases.getString("Login"), setOfTestCases.getString("Password"));
                        FlowReport.reportAction("Authorization done. Token received.", true);
                    } catch (NullPointerException e) {
                        FlowReport.reportAction(e.getMessage(), false);
                    }

                    //if domains is absent - create new
                    List<String> activeDomainsList = methods.checkThatDomainsIsExist(setOfTestCases);

                    for (int i = 0; i < activeDomainsList.size(); i++) {
                        FlowReport.nextStep();
                        FlowReport.addStep("Updating 'DomainName' field in DB.", "");
                        setOfTestCases.updateString("DomainName", activeDomainsList.get(i));
                        setOfTestCases.updateRow();
                        FlowReport.reportAction("Field 'DomainName' updated", true);

                        FlowReport.addStep("Receive the response from client method.", "");
                        String response = (String) method.invoke(obj, setOfTestCases);
                        ObjectMapper mapper = new ObjectMapper();
                        Domains responseObject = mapper.readValue(response, Domains.class);
                        FlowReport.reportAction("Response about domains info recieved ", true);
                        methods.saveAndAddResponse(response);
                        FlowReport.nextStep();
                        FlowReport.addStep("Validating recieved data", "");
                        if (response != null) {
                            Domains subAndOwnedDomainList = methods.getListOfOwnAndSubscribedDomainsByEmail(setOfTestCases.getString("Login"), setOfTestCases.getString("OnlyOwned"));
                            try {
                                if (methods.validateDomainsInfo(responseObject, subAndOwnedDomainList)) {
                                    FlowReport.reportAction("Data succsessfuly validated with RelimeDatabase", true);

                                } else {
                                    FlowReport.reportAction("Data in DB and in response is not the same", false);
                                }
                            } catch (Exception e) {
                                FlowReport.reportAction(e.getMessage(), false);
                            }
                        }
                    }
                } catch (Throwable ex) {
                    FlowReport.reportAction(ex.getMessage() == null ? "Response wasn`t returned" : ex.getMessage(), false);
                } finally {
                    methods.logOut();
                    FlowReport.finishTest();
                }
            }
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
    }

    public void checkDomainRights(ResultSet setOfTestCases) {
        try {
            FlowReport.initFlow(setOfTestCases, testInitiatorClass);
            while (setOfTestCases.next()) {
                try {
                    FlowReport.startTest(setOfTestCases.getString("TestCaseName"));
                    FlowReport.addStep("Check that domain rights API is work.", "");
                    Method method = clientClass.getDeclaredMethod(responseMethodName, ResultSet.class);
                    Object obj = clientClass.newInstance();
                    FlowReport.reportAction("An instance created.", true);

                    FlowReport.nextStep();
                    FlowReport.addStep("Getting response from client method.", "");
                    String response = (String) method.invoke(obj, setOfTestCases);
                    FlowReport.reportAction("Domain rigths response received", true);
                    methods.saveAndAddResponse(response);

                    FlowReport.nextStep();
                    FlowReport.addStep("Validation of response...", "");
                    String expected = setOfTestCases.getString("ExpectedMessage");
                    if (response.toLowerCase().contains(expected.toLowerCase())) {
                        FlowReport.reportAction("Validation done successfully.", true);
                    } else {
                        FlowReport.reportAction("Validation done unsuccessfully.", false);
                    }
                } catch (Throwable ex) {
                    FlowReport.reportAction(ex.getMessage() == null ? "Response was not recieved" : ex.getMessage(), false);
                } finally {
                    FlowReport.finishTest();
                }
            }
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
    }

    //results
    public void checkAllProjects(ResultSet setOfTestCases) {
        try {
            FlowReport.initFlow(setOfTestCases, testInitiatorClass);
            while (setOfTestCases.next()) {
                try {
                    FlowReport.startTest(setOfTestCases.getString("TestCaseName"));
                    FlowReport.addStep("Check that API returns all projects info.", "");
                    Method method = clientClass.getDeclaredMethod(responseMethodName, ResultSet.class);
                    Object obj = clientClass.newInstance();
                    FlowReport.reportAction("An instance created.", true);

                    try {
                        methods.authorizeWithSpecificCreds(setOfTestCases.getString("Login"), setOfTestCases.getString("Password"));
                        FlowReport.reportAction("Authorization done. Token received.", true);
                    } catch (NullPointerException e) {

                        FlowReport.reportAction(e.getMessage(), false);
                    }

                    //if domains is absent - create new
                    List<String> activeDomainsList = methods.checkThatDomainsIsExist(setOfTestCases);

                    FlowReport.nextStep();
                    FlowReport.addStep("Updating 'DomainName' field in DB.", "");
                    setOfTestCases.updateString("DomainName", activeDomainsList.get(0));
                    setOfTestCases.updateRow();
                    FlowReport.reportAction("Field 'DomainName' updated", true);

                    //if projects is absent - create new
                    methods.checkThatProjectsIsExist(setOfTestCases);

                    FlowReport.nextStep();
                    FlowReport.addStep("Receive the response from client method.", "");
                    String response = (String) method.invoke(obj, setOfTestCases);
                    ObjectMapper mapper = new ObjectMapper();
                    Results responseObject = mapper.readValue(response, Results.class);
                    FlowReport.reportAction("Response about all projects info recieved ", true);
                    methods.saveAndAddResponse(response);
                    FlowReport.nextStep();
                    FlowReport.addStep("Validating recieved data", "");
                    if (response != null) {
                        Results allProjectsList = methods.getListOfProjectsByDomainName(setOfTestCases.getString("DomainName"));
                        try {
                            if (methods.validateResultsInfo(responseObject, allProjectsList)) {
                                FlowReport.reportAction("Data succsessfuly validated with RelimeDatabase", true);
                            } else {
                                FlowReport.reportAction("Data in DB and in response is not the same", false);
                            }
                        } catch (Exception e) {
                            FlowReport.reportAction(e.getMessage(), false);
                        }
                    }
                } catch (Throwable ex) {
                    FlowReport.reportAction(ex.getMessage() == null ? "Response wasn`t returned" : ex.getMessage(), false);
                } finally {
                    methods.logOut();
                    FlowReport.finishTest();
                }
            }
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
    }

    //projects
    public void checkRetrieveProject(ResultSet setOfTestCases) {
        try {
            FlowReport.initFlow(setOfTestCases, testInitiatorClass);
            while (setOfTestCases.next()) {
                try {
                    FlowReport.startTest(setOfTestCases.getString("TestCaseName"));
                    FlowReport.addStep("Check can retrieve project by project key.", "");
                    Method method = clientClass.getDeclaredMethod(responseMethodName, ResultSet.class);
                    Object obj = clientClass.newInstance();
                    FlowReport.reportAction("An instance created.", true);

                    try {
                        FlowReport.nextStep();
                        FlowReport.addStep("Authorization...", "");
                        methods.authorizeWithSpecificCreds(setOfTestCases.getString("Login"), setOfTestCases.getString("Password"));
                        FlowReport.reportAction("Authorization done. Token received.", true);
                    } catch (NullPointerException e) {
                        FlowReport.reportAction(e.getMessage(), false);
                    }

                    //if domains is absent - create new
                    List<String> activeDomainsList = methods.checkThatDomainsIsExist(setOfTestCases);

                    FlowReport.nextStep();
                    FlowReport.addStep("Updating 'DomainName'(" + activeDomainsList.get(0) + ") field in DB.", "");
                    setOfTestCases.updateString("DomainName", activeDomainsList.get(0));
                    setOfTestCases.updateRow();
                    FlowReport.reportAction("Field 'DomainName' updated", true);

                    //if projects is absent - create new
                    List<String> listOfActiveProjectKeys = methods.checkThatProjectsIsExist(setOfTestCases);

                    for (int i = 0; i < listOfActiveProjectKeys.size(); i++) {
                        FlowReport.nextStep();
                        FlowReport.addStep("Updating 'ProjectKey' (" + listOfActiveProjectKeys.get(i) + ") field in DB.", "");
                        setOfTestCases.updateString("ProjectKey", listOfActiveProjectKeys.get(i));
                        setOfTestCases.updateRow();
                        FlowReport.reportAction("Field 'ProjectKey' updated", true);

                        FlowReport.nextStep();
                        FlowReport.addStep("Receive the response from client method.", "");
                        String response = (String) method.invoke(obj, setOfTestCases);
                        ObjectMapper mapper = new ObjectMapper();
                        Projects responseObject = mapper.readValue(response, Projects.class);
                        FlowReport.reportAction("Response about retrieved project recieved ", true);
                        methods.saveAndAddResponse(response);
                        FlowReport.nextStep();
                        FlowReport.addStep("Validating recieved data", "");
                        if (response != null) {
                            Projects allProjectInfo = methods.getActiveProjectByProjectKey(setOfTestCases.getString("ProjectKey"));
                            try {
                                if (responseObject.equals(allProjectInfo)) {
                                    FlowReport.reportAction("Data succsessfuly validated with RelimeDatabase", true);
                                } else {
                                    Converter converter = new Converter();
                                    methods.saveAndAddResponse(converter.objectToJson(allProjectInfo));
                                    FlowReport.reportAction("Data in DB and in response is not the same", false);
                                }
                            } catch (Exception e) {
                                FlowReport.reportAction(e.getMessage(), false);
                            }
                        }
                    }
                } catch (Throwable ex) {

                    FlowReport.reportAction(ex.getMessage() == null ? "Response wasn`t returned" : ex.getMessage(), false);
                } finally {
                    methods.logOut();
                    FlowReport.finishTest();
                }
            }
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
    }

    //project
    public void checkCreateProject(ResultSet setOfTestCases) {
        try {
            FlowReport.initFlow(setOfTestCases, testInitiatorClass);
            while (setOfTestCases.next()) {
                try {
                    FlowReport.startTest(setOfTestCases.getString("TestCaseName"));
                    FlowReport.addStep("Check project can be created.", "");
                    Method method = clientClass.getDeclaredMethod(responseMethodName, ResultSet.class);
                    Object obj = clientClass.newInstance();
                    FlowReport.reportAction("An instance created.", true);

                    try {
                        FlowReport.nextStep();
                        FlowReport.addStep("Authorization...", "");
                        methods.authorizeWithSpecificCreds(setOfTestCases.getString("Login"), setOfTestCases.getString("Password"));
                        FlowReport.reportAction("Authorization done. Token received.", true);
                    } catch (NullPointerException e) {
                        FlowReport.reportAction(e.getMessage(), false);
                    }

                    //if domains is absent - create new
                    List<String> activeDomainsList = methods.checkThatDomainsIsExist(setOfTestCases);

                    FlowReport.nextStep();
                    FlowReport.addStep("Updating 'DomainName' field in DB.", "");
                    setOfTestCases.updateString("DomainName", activeDomainsList.get(0));
                    setOfTestCases.updateRow();
                    FlowReport.reportAction("Field 'DomainName' updated", true);

                    FlowReport.nextStep();
                    FlowReport.addStep("Updating 'ProjectKey' field in DB.", "");
                    setOfTestCases.updateString("ProjectKey", methods.genRandomEwerything("", "ProjectKey"));
                    setOfTestCases.updateRow();
                    FlowReport.reportAction("Field 'ProjectKey' updated", true);

                    if (setOfTestCases.getString("ProjectName").contains("Name")) {
                        FlowReport.nextStep();
                        FlowReport.addStep("Updating 'ProjectName' field in DB.", "");
                        setOfTestCases.updateString("ProjectName", methods.genRandomEwerything("Name", ""));
                        setOfTestCases.updateRow();
                        FlowReport.reportAction("Field 'ProjectName' updated", true);
                    }

                    FlowReport.nextStep();
                    FlowReport.addStep("Receive the response from client method.", "");
                    String response = (String) method.invoke(obj, setOfTestCases);
                    ObjectMapper mapper = new ObjectMapper();
                    Project responseObject = mapper.readValue(response, Project.class);
                    FlowReport.reportAction("Response about creating projects recieved ", true);
                    methods.saveAndAddResponse(response);
                    FlowReport.nextStep();
                    FlowReport.addStep("Validating recieved data", "");
                    if (response != null) {
                        Project allProjectInfo = methods.getActiveProjectByProjectKeyProject(setOfTestCases.getString("ProjectKey"));
                        try {
                            if (responseObject.equals(allProjectInfo)) {
                                FlowReport.reportAction("Data succsessfuly validated with RelimeDatabase", true);
                            } else {
                                FlowReport.reportAction("Data in DB and in response is not the same", false);
                            }
                        } catch (Exception e) {
                            FlowReport.reportAction(e.getMessage(), false);
                        }
                    }
                } catch (Throwable ex) {
                    FlowReport.reportAction(ex.getMessage() == null ? "Response wasn`t returned" : ex.getMessage(), false);
                } finally {
                    methods.logOut();
                    FlowReport.finishTest();
                }
            }
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
    }

    public void checkDeleteProject(ResultSet setOfTestCases) {
        try {
            FlowReport.initFlow(setOfTestCases, testInitiatorClass);
            while (setOfTestCases.next()) {
                try {
                    FlowReport.startTest(setOfTestCases.getString("TestCaseName"));
                    FlowReport.addStep("Check that project can be deleted.", "");
                    Method method = clientClass.getDeclaredMethod(responseMethodName, ResultSet.class);
                    Object obj = clientClass.newInstance();

                    try {
                        methods.authorizeWithSpecificCreds(setOfTestCases.getString("Login"), setOfTestCases.getString("Password"));
                        FlowReport.reportAction("Authorization done. Token received.", true);
                    } catch (NullPointerException e) {
                        FlowReport.reportAction(e.getMessage(), false);
                    }

                    //if domains is absent - create new
                    List<String> activeDomainsList = methods.checkThatDomainsIsExist(setOfTestCases);

                    FlowReport.nextStep();
                    FlowReport.addStep("Updating 'DomainName' field in DB.", "");
                    setOfTestCases.updateString("DomainName", activeDomainsList.get(0));
                    setOfTestCases.updateRow();
                    FlowReport.reportAction("Field 'DomainName' updated", true);

                    //if projects is absent - create new
                    List<String> listOfActiveProjectKeys = methods.checkThatProjectsIsExist(setOfTestCases);

                    List<String> oldActiveProjectIdsList = methods.getIdOfActiveProjectByDomainName(setOfTestCases.getString("DomainName"));

                    FlowReport.nextStep();
                    FlowReport.addStep("Updating 'ProjectId' field in DB.", "");
                    setOfTestCases.updateString("ProjectId", oldActiveProjectIdsList.get(0));
                    setOfTestCases.updateRow();
                    FlowReport.reportAction("Field 'ProjectId' updated", true);

                    FlowReport.nextStep();
                    FlowReport.addStep("Receive the response from client method.", "");
                    String response = (String) method.invoke(obj, setOfTestCases);
                    FlowReport.reportAction("Response about project deleting recieved ", true);
                    methods.saveAndAddResponse(response);

                    FlowReport.nextStep();
                    FlowReport.addStep("Validating recieved data", "");
                    if (response != null) {
                        List<String> newActiveProjectIdsList = methods.getIdOfActiveProjectByDomainName(setOfTestCases.getString("DomainName"));
                        try {
                            String reportString = "";
                            if (newActiveProjectIdsList.equals(oldActiveProjectIdsList)) {
                                reportString += "Project is not deleted. Id in relime db is not changed.";
                            }
                            if (!reportString.equals("")) {
                                FlowReport.reportAction(reportString, false);
                            } else {
                                FlowReport.reportAction("Validated. Project deleted successfully.", true);
                            }
                        } catch (Exception e) {
                            FlowReport.reportAction(e.getMessage(), false);
                        }
                    }
                } catch (Throwable ex) {
                    FlowReport.reportAction(ex.getMessage() == null ? "Response wasn`t returned" : ex.getMessage(), false);
                } finally {
                    methods.logOut();
                    FlowReport.finishTest();
                }
            }
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
    }

    public void checkUpdateProject(ResultSet setOfTestCases) {
        try {
            FlowReport.initFlow(setOfTestCases, testInitiatorClass);
            while (setOfTestCases.next()) {
                try {
                    FlowReport.startTest(setOfTestCases.getString("TestCaseName"));
                    FlowReport.addStep("Check that project can be updated.", "");
                    Method method = clientClass.getDeclaredMethod(responseMethodName, ResultSet.class);
                    Object obj = clientClass.newInstance();

                    try {
                        methods.authorizeWithSpecificCreds(setOfTestCases.getString("Login"), setOfTestCases.getString("Password"));
                        FlowReport.reportAction("Authorization done. Token received.", true);
                    } catch (NullPointerException e) {
                        FlowReport.reportAction(e.getMessage(), false);
                    }

                    //if domains is absent - create new
                    List<String> activeDomainsList = methods.checkThatDomainsIsExist(setOfTestCases);

                    FlowReport.nextStep();
                    FlowReport.addStep("Updating 'DomainName' field in DB.", "");
                    setOfTestCases.updateString("DomainName", activeDomainsList.get(0));
                    setOfTestCases.updateRow();
                    FlowReport.reportAction("Field 'DomainName' updated", true);

                    //if projects is absent - create new
                    List<String> listOfActiveProjectKeys = methods.checkThatProjectsIsExist(setOfTestCases);

                    for (int i = 0; i < listOfActiveProjectKeys.size(); i++) {
                        FlowReport.nextStep();
                        FlowReport.addStep("Updating 'ProjectKey' (" + listOfActiveProjectKeys.get(i) + ") field in DB.", "");
                        setOfTestCases.updateString("ProjectKey", listOfActiveProjectKeys.get(i));
                        setOfTestCases.updateRow();
                        FlowReport.reportAction("Field 'ProjectKey' updated", true);

                        Projects oldActiveProject = methods.getActiveProjectByProjectKey(setOfTestCases.getString("ProjectKey"));

                        FlowReport.nextStep();
                        FlowReport.addStep("Updating 'ProjectName' field in DB.", "");
                        setOfTestCases.updateString("ProjectName", methods.genRandomEwerything("Name", ""));
                        setOfTestCases.updateRow();
                        FlowReport.reportAction("Field 'ProjectName' updated", true);

                        FlowReport.nextStep();
                        FlowReport.addStep("Updating 'Description' field in DB.", "");
                        setOfTestCases.updateString("Comments", methods.genRandomEwerything("Description", ""));
                        setOfTestCases.updateRow();
                        FlowReport.reportAction("Field 'Description' updated", true);

                        FlowReport.nextStep();
                        FlowReport.addStep("Receive the response from client method.", "");
                        String response = (String) method.invoke(obj, setOfTestCases);
                        FlowReport.reportAction("Response about project updating recieved ", true);
                        methods.saveAndAddResponse(response);

                        FlowReport.nextStep();
                        FlowReport.addStep("Validating recieved data", "");
                        if (response != null) {
                            Projects newActiveProject = methods.getActiveProjectByProjectKey(setOfTestCases.getString("ProjectKey"));
                            try {
                                String reportString = "";
                                if (!response.contains(setOfTestCases.getString("ExpectedMessage"))) {
                                    reportString += "Project is not updated. Response return false";
                                }
                                if (newActiveProject.getProject().getName().equals(oldActiveProject.getProject().getName())) {
                                    reportString += "Project is not updated. Names still the same.";
                                }
                                if (newActiveProject.getProject().getDescription().equals(oldActiveProject.getProject().getDescription())) {
                                    reportString += "Project is not updated. Description still the same.";
                                }
                                if (!reportString.equals("")) {
                                    FlowReport.reportAction(reportString, false);
                                } else {
                                    FlowReport.reportAction("Validated. Project deleted successfully.", true);
                                }
                            } catch (Exception e) {
                                FlowReport.reportAction(e.getMessage(), false);
                            }
                        }
                    }
                } catch (Throwable ex) {
                    FlowReport.reportAction(ex.getMessage() == null ? "Response wasn`t returned" : ex.getMessage(), false);
                } finally {
                    methods.logOut();
                    FlowReport.finishTest();
                }
            }
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
    }

    public void checkIsNewProject(ResultSet setOfTestCases) {
        try {
            FlowReport.initFlow(setOfTestCases, testInitiatorClass);
            while (setOfTestCases.next()) {
                try {
                    FlowReport.startTest(setOfTestCases.getString("TestCaseName"));
                    FlowReport.addStep("Check that isNewProject API works correctly..", "");
                    Method method = clientClass.getDeclaredMethod(responseMethodName, ResultSet.class);
                    Object obj = clientClass.newInstance();

                    try {
                        methods.authorizeWithSpecificCreds(setOfTestCases.getString("Login"), setOfTestCases.getString("Password"));
                        FlowReport.reportAction("Authorization done. Token received.", true);
                    } catch (NullPointerException e) {
                        FlowReport.reportAction(e.getMessage(), false);
                    }

                    //if domains is absent - create new
                    List<String> activeDomainsList = methods.checkThatDomainsIsExist(setOfTestCases);

                    FlowReport.nextStep();
                    FlowReport.addStep("Updating 'DomainName' field in DB.", "");
                    setOfTestCases.updateString("DomainName", activeDomainsList.get(0));
                    setOfTestCases.updateRow();
                    FlowReport.reportAction("Field 'DomainName' updated", true);

                    //if projects is absent - create new
                    List<String> listOfActiveProjectKeys = methods.checkThatProjectsIsExist(setOfTestCases);

                    for (int i = 0; i < listOfActiveProjectKeys.size(); i++) {
                        FlowReport.nextStep();
                        FlowReport.addStep("Updating 'ProjectKey' field in DB.", "");
                        setOfTestCases.updateString("ProjectKey", listOfActiveProjectKeys.get(i));
                        setOfTestCases.updateRow();
                        FlowReport.reportAction("Field 'ProjectKey' updated", true);

                        //if vcs is absent - create new
                        methods.checkThatVcsIsExist(setOfTestCases, setOfTestCases.getString("ProjectKey"));

                        FlowReport.nextStep();
                        FlowReport.addStep("Receive the response from client method.", "");
                        String response = (String) method.invoke(obj, setOfTestCases);
                        FlowReport.reportAction("Response about project updating recieved ", true);
                        methods.saveAndAddResponse(response);

                        FlowReport.nextStep();
                        FlowReport.addStep("Validating recieved data", "");
                        if (response != null) {
                            if (response.contains("message")) {
                                String vcsByProjectKey = methods.getVcsUrlByProjectKey(setOfTestCases.getString("ProjectKey"));
                                try {
                                    String reportString = "";
                                    if (vcsByProjectKey == null) {
                                        reportString += "Project is absent in DB.";
                                    }
                                    if (!reportString.equals("")) {
                                        FlowReport.reportAction(reportString, false);
                                    } else {
                                        FlowReport.reportAction("Validated. Project updated from git successfully.", true);
                                    }
                                } catch (Exception e) {
                                    FlowReport.reportAction(e.getMessage(), false);
                                }
                            } else {
                                FlowReport.reportAction("It is no 200 OK status code. No message field in response.", false);
                            }
                        }
                    }
                } catch (Throwable ex) {
                    FlowReport.reportAction(ex.getMessage() == null ? "Response wasn`t returned" : ex.getMessage(), false);
                } finally {
                    methods.logOut();
                    FlowReport.finishTest();
                }
            }
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
    }

    public void checkRetrieveVcs(ResultSet setOfTestCases) {
        try {
            FlowReport.initFlow(setOfTestCases, testInitiatorClass);
            while (setOfTestCases.next()) {
                try {
                    FlowReport.startTest(setOfTestCases.getString("TestCaseName"));
                    FlowReport.addStep("Return vcs project info if project have active vcs.", "");
                    Method method = clientClass.getDeclaredMethod(responseMethodName, ResultSet.class);
                    Object obj = clientClass.newInstance();

                    try {
                        methods.authorizeWithSpecificCreds(setOfTestCases.getString("Login"), setOfTestCases.getString("Password"));
                        FlowReport.reportAction("Authorization done. Token received.", true);
                    } catch (NullPointerException e) {
                        FlowReport.reportAction(e.getMessage(), false);
                    }

                    //if domains is absent - create new
                    List<String> activeDomainsList = methods.checkThatDomainsIsExist(setOfTestCases);

                    FlowReport.nextStep();
                    FlowReport.addStep("Updating 'DomainName' (" + activeDomainsList.get(0) + ") field in DB.", "");
                    setOfTestCases.updateString("DomainName", activeDomainsList.get(0));
                    setOfTestCases.updateRow();
                    FlowReport.reportAction("Field 'DomainName' updated", true);

                    //if projects is absent - create new
                    List<String> listOfActiveProjectKeys = methods.checkThatProjectsIsExist(setOfTestCases);

                    for (int i = 0; i < listOfActiveProjectKeys.size(); i++) {
                        FlowReport.nextStep();
                        FlowReport.addStep("Updating 'ProjectKey' (" + listOfActiveProjectKeys.get(i) + ") field in DB.", "");
                        setOfTestCases.updateString("ProjectKey", listOfActiveProjectKeys.get(i));
                        setOfTestCases.updateRow();
                        FlowReport.reportAction("Field 'ProjectKey' updated", true);

                        //if vcs is absent in project - add new vcs
                        methods.checkThatVcsIsExist(setOfTestCases, setOfTestCases.getString("ProjectKey"));

                        FlowReport.nextStep();
                        FlowReport.addStep("Receive the response from client method.", "");
                        String response = (String) method.invoke(obj, setOfTestCases);
                        ObjectMapper mapper = new ObjectMapper();
                        Vcs responseObject = mapper.readValue(response, Vcs.class);
                        FlowReport.reportAction("Response about project vcs recieved ", true);
                        methods.saveAndAddResponse(response);

                        FlowReport.nextStep();
                        FlowReport.addStep("Validating recieved data", "");
                        if (response != null) {
                            Vcs vcsInfoObject = methods.getVcsInfoByProjectKeyWithoutAccount((setOfTestCases.getString("ProjectKey")));
                            try {
                                if (methods.validateProjectVcsInfoWithoutAccount(responseObject, vcsInfoObject)) {
                                    FlowReport.reportAction("Data succsessfuly validated with RelimeDatabase", true);
                                } else {
                                    FlowReport.reportAction("Data in DB and in response is not the same", false);
                                }
                            } catch (Exception e) {
                                FlowReport.reportAction(e.getMessage(), false);
                            }
                        }
                    }
                } catch (Throwable ex) {
                    FlowReport.reportAction(ex.getMessage() == null ? "Response wasn`t returned" : ex.getMessage(), false);
                } finally {
                    methods.logOut();
                    FlowReport.finishTest();
                }
            }
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
    }

    public void checkAddVcs(ResultSet setOfTestCases) {
        try {
            FlowReport.initFlow(setOfTestCases, testInitiatorClass);
            while (setOfTestCases.next()) {
                try {
                    FlowReport.startTest(setOfTestCases.getString("TestCaseName"));
                    FlowReport.addStep("Add VCS to project.", "");
                    Method method = clientClass.getDeclaredMethod(responseMethodName, ResultSet.class);
                    Object obj = clientClass.newInstance();

                    try {
                        methods.authorizeWithSpecificCreds(setOfTestCases.getString("Login"), setOfTestCases.getString("Password"));
                        FlowReport.reportAction("Authorization done. Token received.", true);
                    } catch (NullPointerException e) {
                        FlowReport.reportAction(e.getMessage(), false);
                    }

                    //if domains is absent - create new
                    List<String> activeDomainsList = methods.checkThatDomainsIsExist(setOfTestCases);

                    FlowReport.nextStep();
                    FlowReport.addStep("Updating 'DomainName' field in DB.", "");
                    setOfTestCases.updateString("DomainName", activeDomainsList.get(0));
                    setOfTestCases.updateRow();
                    FlowReport.reportAction("Field 'DomainName' updated", true);

                    FlowReport.nextStep();
                    FlowReport.addStep("Updating 'ProjectKey' field in DB.", "");
                    setOfTestCases.updateString("ProjectKey", methods.genRandomEwerything("", "ProjectKey"));
                    setOfTestCases.updateRow();
                    FlowReport.reportAction("Field 'ProjectKey' updated", true);

                    if (setOfTestCases.getString("ProjectName").contains("Name")) {
                        FlowReport.nextStep();
                        FlowReport.addStep("Updating 'ProjectName' field in DB.", "");
                        setOfTestCases.updateString("ProjectName", methods.genRandomEwerything("Name", ""));
                        setOfTestCases.updateRow();
                        FlowReport.reportAction("Field 'ProjectName' updated", true);
                    }

                    FlowReport.nextStep();
                    FlowReport.addStep("Creating new project without vcs", "");
                    projectClient.getCreateProjectResponse(setOfTestCases);
                    FlowReport.reportAction("Project without vcs created.", true);

                    if (setOfTestCases.getString("URL").contains("git")) {
                        FlowReport.nextStep();
                        FlowReport.addStep("Updating 'URL' field in DB.", "");
                        setOfTestCases.updateString("URl", methods.genRandomEwerything("", "VcsUrl"));
                        setOfTestCases.updateRow();
                        FlowReport.reportAction("Field 'URL' updated", true);
                    }

                    FlowReport.nextStep();
                    FlowReport.addStep("Receive the response from client method.", "");
                    String response = (String) method.invoke(obj, setOfTestCases);
                    ObjectMapper mapper = new ObjectMapper();
                    Vcs responseObject = mapper.readValue(response, Vcs.class);
                    FlowReport.reportAction("Response about adding vcs recieved ", true);
                    methods.saveAndAddResponse(response);

                    FlowReport.nextStep();
                    FlowReport.addStep("Validating recieved data", "");
                    if (response != null) {
                        Vcs vcsInfoByProjectKeyWithoutAccount = methods.getVcsInfoByProjectKeyWithoutAccount((setOfTestCases.getString("ProjectKey")));
                        try {
                            if (responseObject.getId().equals(vcsInfoByProjectKeyWithoutAccount.getId())) {
                                FlowReport.reportAction("Data succsessfuly validated with RelimeDatabase", true);
                            } else {
                                FlowReport.reportAction("Data in DB and in response is not the same", false);
                            }

                            List<String> activeProjectIdsList = methods.getIdOfActiveProjectByDomainName(setOfTestCases.getString("DomainName"));
                            FlowReport.nextStep();
                            FlowReport.addStep("Deleting this project from relime.", "");
                            setOfTestCases.updateString("ProjectId", activeProjectIdsList.get(0));
                            setOfTestCases.updateRow();
                            projectClient.getDeleteProjectResponse(setOfTestCases);
                            FlowReport.reportAction("Project deleted!", true);
                        } catch (Exception e) {
                            FlowReport.reportAction(e.getMessage() == null ? "Response wasn`t returned" : e.getMessage(), false);
                        }
                    }
                } catch (Throwable ex) {
                    FlowReport.reportAction(ex.getMessage() == null ? "Response wasn`t returned" : ex.getMessage(), false);
                } finally {
                    methods.logOut();
                    FlowReport.finishTest();
                }
            }
        } catch (
                Exception e) {
            System.out.print(e.getMessage());
        }
    }

    public void checkUpdateVcs(ResultSet setOfTestCases) {
        try {
            FlowReport.initFlow(setOfTestCases, testInitiatorClass);
            while (setOfTestCases.next()) {
                try {
                    FlowReport.startTest(setOfTestCases.getString("TestCaseName"));
                    FlowReport.addStep("Update VCS in project.", "");
                    Method method = clientClass.getDeclaredMethod(responseMethodName, ResultSet.class);
                    Object obj = clientClass.newInstance();

                    try {
                        methods.authorizeWithSpecificCreds(setOfTestCases.getString("Login"), setOfTestCases.getString("Password"));
                        FlowReport.reportAction("Authorization done. Token received.", true);
                    } catch (NullPointerException e) {
                        FlowReport.reportAction(e.getMessage(), false);
                    }

                    //if domains is absent - create new
                    List<String> activeDomainsList = methods.checkThatDomainsIsExist(setOfTestCases);

                    FlowReport.nextStep();
                    FlowReport.addStep("Updating 'DomainName' field in DB.", "");
                    setOfTestCases.updateString("DomainName", activeDomainsList.get(0));
                    setOfTestCases.updateRow();
                    FlowReport.reportAction("Field 'DomainName' updated", true);

                    List<String> listOfProjectKeys = methods.checkThatProjectsIsExist(setOfTestCases);

                    for (int i = 0; i < listOfProjectKeys.size(); i++) {
                        FlowReport.nextStep();
                        FlowReport.addStep("Updating 'ProjectKey' field in DB.", "");
                        setOfTestCases.updateString("ProjectKey", listOfProjectKeys.get(i));
                        setOfTestCases.updateRow();
                        FlowReport.reportAction("Field 'ProjectKey' updated", true);

                        methods.checkThatVcsIsExist(setOfTestCases, setOfTestCases.getString("ProjectKey"));

                        FlowReport.nextStep();
                        FlowReport.addStep("Updating 'URL' field in DB.", "");
                        setOfTestCases.updateString("URl", methods.genRandomEwerything("", "VcsUrl"));
                        setOfTestCases.updateRow();
                        FlowReport.reportAction("Field 'URL' updated", true);

                        Integer vcsId = methods.getVcsInfoByProjectKey(setOfTestCases.getString("ProjectKey")).getId();

                        FlowReport.nextStep();
                        FlowReport.addStep("Updating 'RequestId' field in DB.", "");
                        setOfTestCases.updateInt("RequestId", vcsId);
                        setOfTestCases.updateRow();
                        FlowReport.reportAction("Field 'RequestId' updated", true);

                        Vcs oldVcsInfo = methods.getVcsInfoByProjectKey(setOfTestCases.getString("ProjectKey"));

                        FlowReport.nextStep();
                        FlowReport.addStep("Receive the response from client method.", "");
                        String response = (String) method.invoke(obj, setOfTestCases);
                        FlowReport.reportAction("Response about updating vcs recieved ", true);
                        methods.saveAndAddResponse(response);

                        FlowReport.nextStep();
                        FlowReport.addStep("Validating recieved data", "");
                        if (response != null) {
                            Vcs newVcsInfo = methods.getVcsInfoByProjectKey(setOfTestCases.getString("ProjectKey"));
                            try {
                                if (!methods.validateProjectVcsInfoWithoutAccount(oldVcsInfo, newVcsInfo)) {
                                    FlowReport.reportAction("Data succsessfuly validated!", true);
                                } else {
                                    FlowReport.reportAction("Validation failed!", false);
                                }
                            } catch (Exception e) {
                                FlowReport.reportAction(e.getMessage(), false);
                            }
                        }
                    }
                } catch (Throwable ex) {
                    FlowReport.reportAction(ex.getMessage() == null ? "Response wasn`t returned" : ex.getMessage(), false);
                } finally {
                    methods.logOut();
                    FlowReport.finishTest();
                }
            }
        } catch (
                Exception e) {
            System.out.print(e.getMessage());
        }
    }

    public void checkCreateAccount(ResultSet setOfTestCases) {
        try {
            FlowReport.initFlow(setOfTestCases, testInitiatorClass);
            while (setOfTestCases.next()) {
                try {
                    FlowReport.startTest(setOfTestCases.getString("TestCaseName"));
                    FlowReport.addStep("Return vcs project info if project have active vcs.", "");
                    Method method = clientClass.getDeclaredMethod(responseMethodName, ResultSet.class);
                    Object obj = clientClass.newInstance();

                    try {
                        methods.authorizeWithSpecificCreds(setOfTestCases.getString("Login"), setOfTestCases.getString("Password"));
                        FlowReport.reportAction("Authorization done. Token received.", true);
                    } catch (NullPointerException e) {
                        FlowReport.reportAction(e.getMessage(), false);
                    }

                    methods.updateFieldsInDBForCreatingAndUpdatingAccounts(setOfTestCases);

                    FlowReport.nextStep();
                    FlowReport.addStep("Receive the response from client method.", "");
                    String response = (String) method.invoke(obj, setOfTestCases);
                    FlowReport.reportAction("Response about project vcs recieved ", true);
                    methods.saveAndAddResponse(response);

                    FlowReport.nextStep();
                    FlowReport.addStep("Validating recieved data", "");
                    if (response != null) {
                        JSONObject jsonObj = new JSONObject(response);
                        String accountNameById = methods.getAccountNameById(jsonObj.getInt("newAccountId"));
                        try {
                            if (accountNameById.equals(setOfTestCases.getString("AccountName"))) {
                                FlowReport.reportAction("Validation done. Account created.", true);
                            } else {
                                FlowReport.reportAction("Validation done. Account didn't created.", false);
                            }
                        } catch (Exception e) {
                            FlowReport.reportAction(e.getMessage(), false);
                        }
                    }
                } catch (Throwable ex) {
                    FlowReport.reportAction(ex.getMessage() == null ? "Response wasn`t returned" : ex.getMessage(), false);
                } finally {
                    methods.logOut();
                    FlowReport.finishTest();
                }
            }
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
    }

    public void checkUpdateAccount(ResultSet setOfTestCases) {
        try {
            FlowReport.initFlow(setOfTestCases, testInitiatorClass);
            while (setOfTestCases.next()) {
                try {
                    FlowReport.startTest(setOfTestCases.getString("TestCaseName"));
                    FlowReport.addStep("Return vcs project info if project have active vcs.", "");
                    Method method = clientClass.getDeclaredMethod(responseMethodName, ResultSet.class);
                    Object obj = clientClass.newInstance();

                    try {
                        methods.authorizeWithSpecificCreds(setOfTestCases.getString("Login"), setOfTestCases.getString("Password"));
                        FlowReport.reportAction("Authorization done. Token received.", true);
                    } catch (NullPointerException e) {
                        FlowReport.reportAction(e.getMessage(), false);
                    }

                    methods.updateFieldsInDBForCreatingAndUpdatingAccounts(setOfTestCases);

                    FlowReport.nextStep();
                    FlowReport.addStep("Creating new account.", "");
                    String newAccountId = accountClient.getCreateAccountResponse(setOfTestCases);
                    FlowReport.reportAction("Account created successfully.", true);
                    JSONObject jsonObj = new JSONObject(newAccountId);

                    methods.updateFieldsInDBForCreatingAndUpdatingAccounts(setOfTestCases);

                    FlowReport.nextStep();
                    FlowReport.addStep("Updating 'AccountId' field in DB.", "");
                    setOfTestCases.updateInt("AccountId", jsonObj.getInt("newAccountId"));
                    setOfTestCases.updateRow();
                    FlowReport.reportAction("Field 'AccountId' updated", true);

                    String oldAccountNameById = methods.getAccountNameById(setOfTestCases.getInt("AccountId"));

                    FlowReport.nextStep();
                    FlowReport.addStep("Receive the response from client method.", "");
                    String response = (String) method.invoke(obj, setOfTestCases);
                    FlowReport.reportAction("Response about updating account recieved ", true);
                    methods.saveAndAddResponse(response);

                    FlowReport.nextStep();
                    FlowReport.addStep("Validating recieved data", "");
                    if (response != null) {
                        String newAccountNameById = methods.getAccountNameById(setOfTestCases.getInt("AccountId"));
                        try {
                            if (!oldAccountNameById.equals(newAccountNameById)) {
                                FlowReport.reportAction("Validation done. Account updated.", true);
                            } else {
                                FlowReport.reportAction("Validation done. Account didn't updated.", false);
                            }
                        } catch (Exception e) {
                            FlowReport.reportAction(e.getMessage(), false);
                        }
                    }
                } catch (Throwable ex) {
                    FlowReport.reportAction(ex.getMessage() == null ? "Response wasn`t returned" : ex.getMessage(), false);
                } finally {
                    methods.logOut();
                    FlowReport.finishTest();
                }
            }
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
    }

    public void checkRetrieveAccounts(ResultSet setOfTestCases) {
        try {
            FlowReport.initFlow(setOfTestCases, testInitiatorClass);
            while (setOfTestCases.next()) {
                try {
                    FlowReport.startTest(setOfTestCases.getString("TestCaseName"));
                    FlowReport.addStep("Return vcs accounts info.", "");
                    Method method = clientClass.getDeclaredMethod(responseMethodName, ResultSet.class);
                    Object obj = clientClass.newInstance();

                    try {
                        methods.authorizeWithSpecificCreds(setOfTestCases.getString("Login"), setOfTestCases.getString("Password"));
                        FlowReport.reportAction("Authorization done. Token received.", true);
                    } catch (NullPointerException e) {
                        FlowReport.reportAction(e.getMessage(), false);
                    }

                    AccountClient accountClient = new AccountClient();

                    methods.updateFieldsInDBForCreatingAndUpdatingAccounts(setOfTestCases);
                    FlowReport.nextStep();
                    FlowReport.addStep("Creating new account.", "");
                    accountClient.getCreateAccountResponse(setOfTestCases);
                    FlowReport.reportAction("Account created successfully.", true);


                    FlowReport.nextStep();
                    FlowReport.addStep("Receive the response from client method.", "");
                    String response = (String) method.invoke(obj, setOfTestCases);
                    String upgradeResponse = "{\"accountsResults\":" + response + "}";
                    ObjectMapper mapper = new ObjectMapper();
                    RetrieveAccounts responseObject = mapper.readValue(upgradeResponse, RetrieveAccounts.class);
                    FlowReport.reportAction("Response about retrieving vcs account info recieved ", true);
                    methods.saveAndAddResponse(response);

                    FlowReport.nextStep();
                    FlowReport.addStep("Validating recieved data", "");
                    if (response != null) {
                        RetrieveAccounts accountInfoObject = methods.getListOfAccountInfoByEmail(setOfTestCases.getString("Login"), setOfTestCases);
                        try {
                            if (methods.validateAccountsInfo(responseObject, accountInfoObject)) {
                                FlowReport.reportAction("Validation done good!", true);
                            } else {
                                FlowReport.reportAction("Validation failed!", false);
                            }
                        } catch (Exception e) {
                            FlowReport.reportAction(e.getMessage(), false);
                        }
                    }
                } catch (Throwable ex) {
                    FlowReport.reportAction(ex.getMessage() == null ? "Response wasn`t returned" : ex.getMessage(), false);
                } finally {
                    methods.logOut();
                    FlowReport.finishTest();
                }
            }
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
    }

    public void checkDeleteAccount(ResultSet setOfTestCases) {
        try {
            FlowReport.initFlow(setOfTestCases, testInitiatorClass);
            while (setOfTestCases.next()) {
                try {
                    FlowReport.startTest(setOfTestCases.getString("TestCaseName"));
                    FlowReport.addStep("Delete the vcs accounts.", "");
                    Method method = clientClass.getDeclaredMethod(responseMethodName, ResultSet.class);
                    Object obj = clientClass.newInstance();

                    String login = setOfTestCases.getString("Login");

                    try {
                        methods.authorizeWithSpecificCreds(login, setOfTestCases.getString("Password"));
                        FlowReport.reportAction("Authorization done. Token received.", true);
                    } catch (NullPointerException e) {
                        FlowReport.reportAction(e.getMessage(), false);
                    }

                    //if accounts is absent - create new
                    List<Integer> oldListOfAccountIds = methods.checkThatAccountsIsExist(setOfTestCases);

                    for (int i = 0; i < oldListOfAccountIds.size(); i++) {
                        FlowReport.nextStep();
                        FlowReport.addStep("Updating 'AccountId' field in DB.", "");
                        setOfTestCases.updateInt("AccountId", oldListOfAccountIds.get(i));
                        setOfTestCases.updateRow();
                        FlowReport.reportAction("Field 'AccountId' updated", true);

                        FlowReport.nextStep();
                        FlowReport.addStep("Receive the response from client method.", "");
                        String response = (String) method.invoke(obj, setOfTestCases);
                        FlowReport.reportAction("Response about deleting account recieved ", true);
                        methods.saveAndAddResponse(response);

                        FlowReport.nextStep();
                        FlowReport.addStep("Validating recieved data", "");
                        if (response != null) {
                            List<Integer> newListOfAccountIds = methods.getListOfAccountIdsByEmail(login);
                            try {
                                if (!newListOfAccountIds.equals(oldListOfAccountIds)) {
                                    FlowReport.reportAction("Validation done. Account deleted.", true);
                                } else {
                                    FlowReport.reportAction("Validation done. Account didn't deleted.", false);
                                }
                            } catch (Exception e) {
                                FlowReport.reportAction(e.getMessage(), false);
                            }
                        }
                    }
                } catch (Throwable ex) {
                    FlowReport.reportAction(ex.getMessage() == null ? "Response wasn`t returned" : ex.getMessage(), false);
                } finally {
                    methods.logOut();
                    FlowReport.finishTest();
                }
            }
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
    }

    public void checkRetrieveStoryTree(ResultSet setOfTestCases) {
        try {
            FlowReport.initFlow(setOfTestCases, testInitiatorClass);
            while (setOfTestCases.next()) {
                try {
                    FlowReport.startTest(setOfTestCases.getString("TestCaseName"));
                    FlowReport.addStep("Return story tree info.", "");
                    Method method = clientClass.getDeclaredMethod(responseMethodName, ResultSet.class);
                    Object obj = clientClass.newInstance();

                    try {
                        methods.authorizeWithSpecificCreds(setOfTestCases.getString("Login"), setOfTestCases.getString("Password"));
                        FlowReport.reportAction("Authorization done. Token received.", true);
                    } catch (NullPointerException e) {
                        FlowReport.reportAction(e.getMessage(), false);
                    }

                    List<String> listProjectKeys = methods.getListOfActiveProjectKeysByDomainName(setOfTestCases.getString("DomainName"));
                    for (int i = 0; i < listProjectKeys.size(); i++) {
                        FlowReport.nextStep();
                        FlowReport.addStep("Updating 'ProjectKey' (" + listProjectKeys.get(i) + ") field in DB.", "");
                        setOfTestCases.updateString("ProjectKey", listProjectKeys.get(i));
                        setOfTestCases.updateRow();
                        FlowReport.reportAction("Field 'ProjectKey' updated", true);

                        FlowReport.nextStep();
                        FlowReport.addStep("Receive the response from client method.", "");
                        String response = (String) method.invoke(obj, setOfTestCases);
                        String upgradeResponse = "{\"storyTree\":" + response + "}";
                        ObjectMapper mapper = new ObjectMapper();
                        StoryTrees responseObject = mapper.readValue(upgradeResponse, StoryTrees.class);
                        FlowReport.reportAction("Response about story tree info recieved ", true);
                        methods.saveAndAddResponse(response);

                        FlowReport.nextStep();
                        FlowReport.addStep("Validating recieved data", "");
                        if (response != null) {
                            StoryTrees listStoryTree = methods.getStoryTreesByProjectKey(setOfTestCases.getString("ProjectKey"));
                            methods.getChildrens(listStoryTree.getStoryTree());
                            responseObject = methods.sortStoryTreeObject(responseObject);
                            listStoryTree = methods.sortStoryTreeObject(listStoryTree);
                            try {
                                if (responseObject.equals(listStoryTree)) {
                                    FlowReport.reportAction("Validation done good!", true);
                                } else {
                                    FlowReport.reportAction("Validation failed!", false);
                                }
                            } catch (Exception e) {
                                FlowReport.reportAction(e.getMessage(), false);
                            }
                        }
                    }
                } catch (Throwable ex) {
                    FlowReport.reportAction(ex.getMessage() == null ? "Response wasn`t returned" : ex.getMessage(), false);
                } finally {
                    methods.logOut();
                    FlowReport.finishTest();
                }
            }
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
    }

    public void checkCreateTree(ResultSet setOfTestCases) {
        try {
            FlowReport.initFlow(setOfTestCases, testInitiatorClass);
            while (setOfTestCases.next()) {
                try {
                    FlowReport.startTest(setOfTestCases.getString("TestCaseName"));
                    FlowReport.addStep("Create a new story/tree in project.", "");
                    Method method = clientClass.getDeclaredMethod(responseMethodName, ResultSet.class);
                    Object obj = clientClass.newInstance();

                    try {
                        methods.authorizeWithSpecificCreds(setOfTestCases.getString("Login"), setOfTestCases.getString("Password"));
                        FlowReport.reportAction("Authorization done. Token received.", true);
                    } catch (NullPointerException e) {
                        FlowReport.reportAction(e.getMessage(), false);
                    }

                    //if domains is absent - create new
                    List<String> activeDomainsList = methods.checkThatDomainsIsExist(setOfTestCases);

                    FlowReport.nextStep();
                    FlowReport.addStep("Updating 'DomainName' field in DB.", "");
                    setOfTestCases.updateString("DomainName", activeDomainsList.get(0));
                    setOfTestCases.updateRow();
                    FlowReport.reportAction("Field 'DomainName' updated", true);

                    //if projects is absent - create new
                    List<String> listOfActiveProjectKeys = methods.checkThatProjectsIsExist(setOfTestCases);

                    FlowReport.nextStep();
                    FlowReport.addStep("Updating 'ProjectKey' field in DB.", "");
                    setOfTestCases.updateString("ProjectKey", listOfActiveProjectKeys.get(0));
                    setOfTestCases.updateRow();
                    FlowReport.reportAction("Field 'ProjectKey' updated", true);

                    FlowReport.nextStep();
                    FlowReport.addStep("Getting a list of parents ids.", "");
                    List<Integer> listOfParentIds = methods.getParentIdsByProjectKey(setOfTestCases.getString("ProjectKey"));
                    if (listOfParentIds.size() == 0) {
                        FlowReport.reportAction("Parent ids is absent.", false);
                    } else {
                        FlowReport.reportAction("List of parents ids received.", true);
                        if (listOfParentIds.get(0) == 0) {
                            FlowReport.nextStep();
                            FlowReport.addStep("Updating 'ParentId' field in DB.", "");
                            setOfTestCases.updateString("ParentId", null);
                            setOfTestCases.updateRow();
                            FlowReport.reportAction("Field 'ParentId' updated", true);
                        } else {
                            FlowReport.nextStep();
                            FlowReport.addStep("Updating 'ParentId' field in DB.", "");
                            setOfTestCases.updateString("ParentId", listOfParentIds.get(0).toString());
                            setOfTestCases.updateRow();
                            FlowReport.reportAction("Field 'ParentId' updated", true);
                        }
                    }

                    FlowReport.nextStep();
                    FlowReport.addStep("Updating 'Label' field in DB.", "");
                    setOfTestCases.updateString("Label", methods.genRandomEwerything("Folder", ""));
                    setOfTestCases.updateRow();
                    FlowReport.reportAction("Field 'Label' updated", true);

                    FlowReport.nextStep();
                    FlowReport.addStep("Receive the response from client method.", "");
                    String response = (String) method.invoke(obj, setOfTestCases);
                    FlowReport.reportAction("Response about creating story recieved ", true);
                    methods.saveAndAddResponse(response);

                    FlowReport.nextStep();
                    FlowReport.addStep("Validating recieved data", "");
                    if (response != null) {
                        JSONObject jsonObj = new JSONObject(response);
                        int newId = jsonObj.getInt("id");
                        List<Integer> listOfStoryIds = methods.getStoryTreeIdsByProjectKey(setOfTestCases.getString("ProjectKey"));
                        try {
                            if (listOfStoryIds.contains(newId)) {
                                FlowReport.reportAction("Validation done good.", true);
                            } else {
                                FlowReport.reportAction("Validation failed!", false);
                            }
                        } catch (Exception e) {
                            FlowReport.reportAction(e.getMessage(), false);
                        }
                    }

                } catch (Throwable ex) {
                    FlowReport.reportAction(ex.getMessage() == null ? "Response wasn`t returned" : ex.getMessage(), false);
                } finally {
                    methods.logOut();
                    FlowReport.finishTest();
                }
            }
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
    }

    public void deleteTreeCheck(ResultSet setOfTestCases) {
        try {
            FlowReport.initFlow(setOfTestCases, testInitiatorClass);
            while (setOfTestCases.next()) {
                try {
                    FlowReport.startTest(setOfTestCases.getString("TestCaseName"));
                    FlowReport.addStep("Delete a story/tree from project.", "");
                    Method method = clientClass.getDeclaredMethod(responseMethodName, ResultSet.class);
                    Object obj = clientClass.newInstance();

                    try {
                        methods.authorizeWithSpecificCreds(setOfTestCases.getString("Login"), setOfTestCases.getString("Password"));
                        FlowReport.reportAction("Authorization done. Token received.", true);
                    } catch (NullPointerException e) {
                        FlowReport.reportAction(e.getMessage(), false);
                    }

                    FlowReport.nextStep();
                    FlowReport.addStep("Getting a list of story ids.", "");
                    List<Integer> oldListOfStoryIds = methods.getStoryTreeIdsByProjectKey(setOfTestCases.getString("ProjectKey"));
                    FlowReport.reportAction("Story ids received", true);
                    if (oldListOfStoryIds.size() == 0) {
                        FlowReport.nextStep();
                        FlowReport.addStep("Getting a list of parents ids.", "");
                        StoryTreeClient storyTreeClient = new StoryTreeClient();
                        List<Integer> listOfParentIds = methods.getParentIdsByProjectKey(setOfTestCases.getString("ProjectKey"));
                        if (listOfParentIds.size() == 0) {
                            FlowReport.reportAction("Parent ids is absent.", false);
                        } else {
                            FlowReport.reportAction("List of parents ids received.", true);
                            if (listOfParentIds.get(0) == 0) {
                                FlowReport.nextStep();
                                FlowReport.addStep("Updating 'ParentId' field in DB.", "");
                                setOfTestCases.updateString("ParentId", null);
                                setOfTestCases.updateRow();
                                FlowReport.reportAction("Field 'ParentId' updated", true);
                            } else {
                                FlowReport.nextStep();
                                FlowReport.addStep("Updating 'ParentId' field in DB.", "");
                                setOfTestCases.updateString("ParentId", listOfParentIds.get(0).toString());
                                setOfTestCases.updateRow();
                                FlowReport.reportAction("Field 'ParentId' updated", true);
                            }
                        }

                        FlowReport.nextStep();
                        FlowReport.addStep("Updating 'Label' field in DB.", "");
                        setOfTestCases.updateString("Label", methods.genRandomEwerything("Folder", ""));
                        setOfTestCases.updateRow();
                        FlowReport.reportAction("Field 'Label' updated", true);

                        FlowReport.nextStep();
                        FlowReport.addStep("Creating new trees in project.", "");
                        storyTreeClient.getCreateTreeResponse(setOfTestCases);
                        oldListOfStoryIds = methods.getStoryTreeIdsByProjectKey(setOfTestCases.getString("ProjectKey"));
                        FlowReport.reportAction("Trees created", true);
                    }

                    FlowReport.nextStep();
                    FlowReport.addStep("Update 'StoryId' field in DB", "");
                    setOfTestCases.updateInt("RequestId", oldListOfStoryIds.get(0));
                    setOfTestCases.updateRow();
                    FlowReport.reportAction("Field 'StoryId' updated", true);

                    FlowReport.nextStep();
                    FlowReport.addStep("Receive the response from client method.", "");
                    String response = (String) method.invoke(obj, setOfTestCases);
                    FlowReport.reportAction("Response about deleting story recieved ", true);
                    methods.saveAndAddResponse(response);

                    FlowReport.nextStep();
                    FlowReport.addStep("Validating recieved data", "");
                    if (response != null) {
                        List<Integer> newListOfStoryIds = methods.getStoryTreeIdsByProjectKey(setOfTestCases.getString("ProjectKey"));
                        try {
                            if (!newListOfStoryIds.contains(setOfTestCases.getString("RequestId"))) {
                                FlowReport.reportAction("Validation done good.", true);
                            } else {
                                FlowReport.reportAction("Validation failed!", false);
                            }
                        } catch (Exception e) {
                            FlowReport.reportAction(e.getMessage(), false);
                        }
                    }
                } catch (Throwable ex) {
                    FlowReport.reportAction(ex.getMessage() == null ? "Response wasn`t returned" : ex.getMessage(), false);
                } finally {
                    methods.logOut();
                    FlowReport.finishTest();
                }
            }
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
    }

    public void checkUpdateTree(ResultSet setOfTestCases) {
        try {
            FlowReport.initFlow(setOfTestCases, testInitiatorClass);
            while (setOfTestCases.next()) {
                try {
                    FlowReport.startTest(setOfTestCases.getString("TestCaseName"));
                    FlowReport.addStep("Delete a story/tree from project.", "");
                    Method method = clientClass.getDeclaredMethod(responseMethodName, ResultSet.class);
                    Object obj = clientClass.newInstance();

                    try {
                        methods.authorizeWithSpecificCreds(setOfTestCases.getString("Login"), setOfTestCases.getString("Password"));
                        FlowReport.reportAction("Authorization done. Token received.", true);
                    } catch (NullPointerException e) {
                        FlowReport.reportAction(e.getMessage(), false);
                    }

                    FlowReport.nextStep();
                    FlowReport.addStep("Getting a list of story ids.", "");
                    List<Integer> oldListOfStoryIds = methods.getStoryTreeIdsByProjectKey(setOfTestCases.getString("ProjectKey"));
                    FlowReport.reportAction("Story ids received", true);
                    if (oldListOfStoryIds.size() == 0) {
                        FlowReport.nextStep();
                        FlowReport.addStep("Getting a list of parents ids.", "");
                        StoryTreeClient storyTreeClient = new StoryTreeClient();
                        List<Integer> listOfParentIds = methods.getParentIdsByProjectKey(setOfTestCases.getString("ProjectKey"));
                        if (listOfParentIds.size() == 0) {
                            FlowReport.reportAction("Parent ids is absent.", false);
                        } else {
                            FlowReport.reportAction("List of parents ids received.", true);
                            if (listOfParentIds.get(0) == 0) {
                                FlowReport.nextStep();
                                FlowReport.addStep("Updating 'ParentId' field in DB.", "");
                                setOfTestCases.updateString("ParentId", null);
                                setOfTestCases.updateRow();
                                FlowReport.reportAction("Field 'ParentId' updated", true);
                            } else {
                                FlowReport.nextStep();
                                FlowReport.addStep("Updating 'ParentId' field in DB.", "");
                                setOfTestCases.updateString("ParentId", listOfParentIds.get(0).toString());
                                setOfTestCases.updateRow();
                                FlowReport.reportAction("Field 'ParentId' updated", true);
                            }
                        }

                        FlowReport.nextStep();
                        FlowReport.addStep("Updating 'Label' field in DB.", "");
                        setOfTestCases.updateString("Label", methods.genRandomEwerything("Folder", ""));
                        setOfTestCases.updateRow();
                        FlowReport.reportAction("Field 'Label' updated", true);

                        FlowReport.nextStep();
                        FlowReport.addStep("Creating new trees in project.", "");
                        storyTreeClient.getCreateTreeResponse(setOfTestCases);
                        oldListOfStoryIds = methods.getStoryTreeIdsByProjectKey(setOfTestCases.getString("ProjectKey"));
                        FlowReport.reportAction("Trees created", true);
                    }

                    FlowReport.nextStep();
                    FlowReport.addStep("Update 'StoryId' field in DB", "");
                    setOfTestCases.updateInt("RequestId", oldListOfStoryIds.get(0));
                    setOfTestCases.updateRow();
                    FlowReport.reportAction("Field 'StoryId' updated", true);

                    FlowReport.nextStep();
                    FlowReport.addStep("Updating 'Label' field in DB.", "");
                    setOfTestCases.updateString("Label", methods.genRandomEwerything("Folder", ""));
                    setOfTestCases.updateRow();
                    FlowReport.reportAction("Field 'Label' updated", true);

                    FlowReport.nextStep();
                    FlowReport.addStep("Receive the response from client method.", "");
                    String response = (String) method.invoke(obj, setOfTestCases);
                    FlowReport.reportAction("Response about deleting story recieved ", true);
                    methods.saveAndAddResponse(response);

                    FlowReport.nextStep();
                    FlowReport.addStep("Validating recieved data", "");
                    if (response != null) {
                        List<String> listOfStoryNames = methods.getListOfStoryTreeNamesByProjectKey(setOfTestCases.getString("ProjectKey"));
                        try {
                            if (listOfStoryNames.contains(setOfTestCases.getString("Label"))) {
                                FlowReport.reportAction("Validation done good.", true);
                            } else {
                                FlowReport.reportAction("Validation failed!", false);
                            }
                        } catch (Exception e) {
                            FlowReport.reportAction(e.getMessage(), false);
                        }
                    }
                } catch (Throwable ex) {
                    FlowReport.reportAction(ex.getMessage() == null ? "Response wasn`t returned" : ex.getMessage(), false);
                } finally {
                    methods.logOut();
                    FlowReport.finishTest();
                }
            }
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
    }

    public void checkRetrieveStory(ResultSet setOfTestCases) {
        try {
            FlowReport.initFlow(setOfTestCases, testInitiatorClass);
            while (setOfTestCases.next()) {
                try {
                    FlowReport.startTest(setOfTestCases.getString("TestCaseName"));
                    FlowReport.addStep("Return story tree info.", "");
                    Method method = clientClass.getDeclaredMethod(responseMethodName, ResultSet.class);
                    Object obj = clientClass.newInstance();

                    try {
                        methods.authorizeWithSpecificCreds(setOfTestCases.getString("Login"), setOfTestCases.getString("Password"));
                        FlowReport.reportAction("Authorization done. Token received.", true);
                    } catch (NullPointerException e) {
                        FlowReport.reportAction(e.getMessage(), false);
                    }

                    List<String> listProjectKeys = methods.getListOfActiveProjectKeysByDomainName(setOfTestCases.getString("DomainName"));
                    for (int i = 0; i < listProjectKeys.size(); i++) {
                        FlowReport.nextStep();
                        FlowReport.addStep("Updating 'ProjectKey' (" + listProjectKeys.get(i) + ") field in DB.", "");
                        setOfTestCases.updateString("ProjectKey", listProjectKeys.get(i));
                        setOfTestCases.updateRow();
                        FlowReport.reportAction("Field 'ProjectKey' updated", true);

                        FlowReport.nextStep();
                        FlowReport.addStep("Receive the response from client method.", "");
                        String response = (String) method.invoke(obj, setOfTestCases);
                        String upgradeResponse = "{\"stories\":" + response + "}";
                        ObjectMapper mapper = new ObjectMapper();
                        Stories responseObject = mapper.readValue(upgradeResponse, Stories.class);
                        FlowReport.reportAction("Response about story info recieved ", true);
                        methods.saveAndAddResponse(response);

                        Stories databaseObject = methods.getStoriesInfoByProjectKey(setOfTestCases.getString("ProjectKey"));
                        List<String> tagsList = methods.getTagsByProjectKey(setOfTestCases.getString("ProjectKey"));
                        databaseObject = methods.setTagsList(databaseObject.getStories(), tagsList);
                        FlowReport.nextStep();
                        FlowReport.addStep("Validating recieved data", "");
                        if (response != null) {
                            try {
                                if (responseObject.equals(databaseObject)) {
                                    FlowReport.reportAction("Validation done good!", true);
                                } else {
                                    Converter converter = new Converter();
                                    methods.saveAndAddResponse(converter.objectToJson(databaseObject));
                                    FlowReport.reportAction("Validation failed!", false);
                                }
                            } catch (Exception e) {
                                FlowReport.reportAction(e.getMessage(), false);
                            }
                        }
                    }
                } catch (Throwable ex) {
                    FlowReport.reportAction(ex.getMessage() == null ? "Response wasn`t returned" : ex.getMessage(), false);
                } finally {
                    methods.logOut();
                    FlowReport.finishTest();
                }
            }
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
    }

    public void checkCreateStory(ResultSet setOfTestCases) {
        try {
            FlowReport.initFlow(setOfTestCases, testInitiatorClass);
            while (setOfTestCases.next()) {
                try {
                    FlowReport.startTest(setOfTestCases.getString("TestCaseName"));
                    FlowReport.addStep("Create a new story/tree in project.", "");
                    Method method = clientClass.getDeclaredMethod(responseMethodName, ResultSet.class);
                    Object obj = clientClass.newInstance();

                    try {
                        methods.authorizeWithSpecificCreds(setOfTestCases.getString("Login"), setOfTestCases.getString("Password"));
                        FlowReport.reportAction("Authorization done. Token received.", true);
                    } catch (NullPointerException e) {
                        FlowReport.reportAction(e.getMessage(), false);
                    }

                    FlowReport.nextStep();
                    FlowReport.addStep("Getting a list of parents ids.", "");
                    List<Integer> listOfParentIds = methods.getParentIdsByProjectKey(setOfTestCases.getString("ProjectKey"));
                    if (listOfParentIds.size() == 0) {
                        FlowReport.reportAction("Parent ids is absent.", false);
                    } else {
                        FlowReport.reportAction("List of parents ids received.", true);
                        if (listOfParentIds.get(0) == 0) {
                            FlowReport.nextStep();
                            FlowReport.addStep("Updating 'ParentFolderId' (null) field in DB.", "");
                            setOfTestCases.updateString("ParentFolderId", null);
                            setOfTestCases.updateRow();
                            FlowReport.reportAction("Field 'ParentFolderId' updated", true);
                        } else {
                            FlowReport.nextStep();
                            FlowReport.addStep("Updating 'ParentFolderId' (" + listOfParentIds.get(0) + ") field in DB.", "");
                            setOfTestCases.updateString("ParentFolderId", listOfParentIds.get(0).toString());
                            setOfTestCases.updateRow();
                            FlowReport.reportAction("Field 'ParentFolderId' updated", true);
                        }
                    }

                    if (setOfTestCases.getString("StoryName").contains("Name")) {
                        FlowReport.nextStep();
                        FlowReport.addStep("Updating 'StoryName' field in DB.", "");
                        setOfTestCases.updateString("StoryName", methods.genRandomEwerything("Name", ""));
                        setOfTestCases.updateRow();
                        FlowReport.reportAction("Field 'StoryName' updated", true);
                    } else {
                        FlowReport.reportAction("Something wrong with 'StoryName' field updating!", false);
                    }

                    if (setOfTestCases.getString("FileName").contains("Name")) {
                        FlowReport.nextStep();
                        FlowReport.addStep("Updating 'FileName' field in DB.", "");
                        setOfTestCases.updateString("FileName", methods.genRandomEwerything("Name", ""));
                        setOfTestCases.updateRow();
                        FlowReport.reportAction("Field 'FileName' updated", true);
                    } else {
                        FlowReport.reportAction("Something wrong with 'FileName' field updating!", false);
                    }

                    FlowReport.nextStep();
                    FlowReport.addStep("Receive the response from client method.", "");
                    String response = (String) method.invoke(obj, setOfTestCases);
                    ObjectMapper mapper = new ObjectMapper();
                    Story responseObject = mapper.readValue(response, Story.class);
                    FlowReport.reportAction("Response about creating story recieved ", true);
                    methods.saveAndAddResponse(response);

                    FlowReport.nextStep();
                    FlowReport.addStep("Validating recieved data", "");
                    if (response != null) {
                        Stories stories = methods.getStoriesInfoByProjectKey(setOfTestCases.getString("ProjectKey"));
                        try {
                            if (methods.validateStoryCreation(stories.getStories(), responseObject)) {
                                FlowReport.reportAction("Validation done good.", true);
                            } else {
                                FlowReport.reportAction("Validation failed!", false);
                            }
                        } catch (Exception e) {
                            FlowReport.reportAction(e.getMessage(), false);
                        }
                    }

                } catch (Throwable ex) {
                    FlowReport.reportAction(ex.getMessage() == null ? "Response wasn`t returned" : ex.getMessage(), false);
                } finally {
                    methods.logOut();
                    FlowReport.finishTest();
                }
            }
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
    }

    public void checkCreateJiraLinkedStory(ResultSet setOfTestCases) {
        try {
            FlowReport.initFlow(setOfTestCases, testInitiatorClass);
            while (setOfTestCases.next()) {
                try {
                    StoryClient storyClient = new StoryClient();
                    FlowReport.startTest(setOfTestCases.getString("TestCaseName"));
                    FlowReport.addStep("Create a new story/tree in project.", "");
                    Method method = clientClass.getDeclaredMethod(responseMethodName, ResultSet.class);
                    Object obj = clientClass.newInstance();

                    try {
                        methods.authorizeWithSpecificCreds(setOfTestCases.getString("Login"), setOfTestCases.getString("Password"));
                        FlowReport.reportAction("Authorization done. Token received.", true);
                    } catch (NullPointerException e) {
                        FlowReport.reportAction(e.getMessage(), false);
                    }

                    FlowReport.nextStep();
                    FlowReport.addStep("Updating 'FileName' (" + setOfTestCases.getString("Key_") + ") field in DB.", "");
                    setOfTestCases.updateString("FileName", setOfTestCases.getString("Key_"));
                    setOfTestCases.updateRow();
                    FlowReport.reportAction("Field 'FileName' updated", true);

                    Story listOfJiraStory = methods.getListOfJiraLinkedStoryByJiraStoryKey(setOfTestCases.getString("FileName"));

                    if (listOfJiraStory.getStoryId() != null) {
                        FlowReport.nextStep();
                        FlowReport.addStep("Update 'StoryId' (" + listOfJiraStory.getStoryId() + ") field in DB", "");
                        setOfTestCases.updateInt("StoryId", listOfJiraStory.getStoryId());
                        setOfTestCases.updateRow();
                        FlowReport.reportAction("Field 'StoryId' updated", true);

                        FlowReport.nextStep();
                        FlowReport.addStep("Deleting Jira-linked story from project...", "");
                        storyClient.getDeleteStoryResponse(setOfTestCases);
                        FlowReport.reportAction("Jira-linked story deleted.", true);
                    }

                    FlowReport.nextStep();
                    FlowReport.addStep("Receive the response from client method.", "");
                    String response = (String) method.invoke(obj, setOfTestCases);
                    ObjectMapper mapper = new ObjectMapper();
                    Story responseObject = mapper.readValue(response, Story.class);
                    FlowReport.reportAction("Response about creating Jira-linked story recieved ", true);
                    methods.saveAndAddResponse(response);

                    FlowReport.nextStep();
                    FlowReport.addStep("Update 'StoryId' (" + responseObject.getStoryId() + ") field in DB", "");
                    setOfTestCases.updateInt("StoryId", responseObject.getStoryId());
                    setOfTestCases.updateRow();
                    FlowReport.reportAction("Field 'StoryId' updated", true);

                    FlowReport.nextStep();
                    FlowReport.addStep("Validating recieved data", "");
                    if (response != null) {
                        Stories stories = methods.getStoriesInfoByProjectKey(setOfTestCases.getString("ProjectKey"));
                        try {
                            if (methods.validateStoryCreation(stories.getStories(), responseObject)) {
                                FlowReport.reportAction("Validation done good.", true);
                                FlowReport.nextStep();
                                FlowReport.addStep("Deleting a Jira linked story from project.", "");
                                storyClient.getDeleteStoryResponse(setOfTestCases);
                                FlowReport.reportAction("Story deleted succesfully.", true);
                            } else {
                                FlowReport.reportAction("Validation failed!", false);
                            }
                        } catch (Exception e) {
                            FlowReport.reportAction(e.getMessage(), false);
                        }
                    }

                } catch (Throwable ex) {
                    FlowReport.reportAction(ex.getMessage() == null ? "Response wasn`t returned" : ex.getMessage(), false);
                } finally {
                    methods.logOut();
                    FlowReport.finishTest();
                }
            }
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
    }

    public void checkDeleteStory(ResultSet setOfTestCases) {
        try {
            FlowReport.initFlow(setOfTestCases, testInitiatorClass);
            while (setOfTestCases.next()) {
                try {
                    FlowReport.startTest(setOfTestCases.getString("TestCaseName"));
                    FlowReport.addStep("Delete a story/tree from project.", "");
                    Method method = clientClass.getDeclaredMethod(responseMethodName, ResultSet.class);
                    Object obj = clientClass.newInstance();

                    try {
                        methods.authorizeWithSpecificCreds(setOfTestCases.getString("Login"), setOfTestCases.getString("Password"));
                        FlowReport.reportAction("Authorization done. Token received.", true);
                    } catch (NullPointerException e) {
                        FlowReport.reportAction(e.getMessage(), false);
                    }

                    // check that story exist or create new
                    List<Integer> oldListOfStoryIds = methods.checkThatStoryIsExist(setOfTestCases);

                    FlowReport.nextStep();
                    FlowReport.addStep("Update 'StoryId' (" + oldListOfStoryIds.get(0) + ") field in DB", "");
                    setOfTestCases.updateInt("StoryId", oldListOfStoryIds.get(0));
                    setOfTestCases.updateRow();
                    FlowReport.reportAction("Field 'StoryId' updated", true);

                    FlowReport.nextStep();
                    FlowReport.addStep("Receive the response from client method.", "");
                    String response = (String) method.invoke(obj, setOfTestCases);
                    FlowReport.reportAction("Response about deleting story recieved ", true);
                    methods.saveAndAddResponse(response);

                    FlowReport.nextStep();
                    FlowReport.addStep("Validating recieved data", "");
                    if (response != null) {
                        List<Integer> newListOfStoryIds = methods.getListOfStoryIdsByProjectKey(setOfTestCases.getString("ProjectKey"));
                        try {
                            String reportString = "";
                            if (newListOfStoryIds.contains(setOfTestCases.getInt("StoryId"))) {
                                reportString += "StoryId that must be deleted still in db. ";
                            }
                            if (oldListOfStoryIds.size() <= newListOfStoryIds.size()) {
                                reportString += "List of StoryIds doesn't change";
                            }
                            if (!reportString.equals("")) {
                                FlowReport.reportAction(reportString, false);
                            } else {
                                FlowReport.reportAction("Validation done good!", true);
                            }
                        } catch (Exception e) {
                            FlowReport.reportAction(e.getMessage(), false);
                        }
                    }
                } catch (Throwable ex) {
                    FlowReport.reportAction(ex.getMessage() == null ? "Response wasn`t returned" : ex.getMessage(), false);
                } finally {
                    methods.logOut();
                    FlowReport.finishTest();
                }
            }
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
    }

    public void checkUpdateStory(ResultSet setOfTestCases) {
        try {
            FlowReport.initFlow(setOfTestCases, testInitiatorClass);
            while (setOfTestCases.next()) {
                try {
                    FlowReport.startTest(setOfTestCases.getString("TestCaseName"));
                    FlowReport.addStep("Update story in project.", "");
                    Method method = clientClass.getDeclaredMethod(responseMethodName, ResultSet.class);
                    Object obj = clientClass.newInstance();

                    try {
                        methods.authorizeWithSpecificCreds(setOfTestCases.getString("Login"), setOfTestCases.getString("Password"));
                        FlowReport.reportAction("Authorization done. Token received.", true);
                    } catch (NullPointerException e) {
                        FlowReport.reportAction(e.getMessage(), false);
                    }

                    Stories oldStoriesInfo = methods.getStoriesInfoByProjectKey(setOfTestCases.getString("ProjectKey"));
                    List<String> oldTagsList = methods.getTagsByProjectKey(setOfTestCases.getString("ProjectKey"));
                    oldStoriesInfo = methods.setTagsList(oldStoriesInfo.getStories(), oldTagsList);

                    // check that story exist or create new
                    List<Integer> listOfStoryIds = methods.checkThatStoryIsExist(setOfTestCases);

                    for (int i = 0; i < listOfStoryIds.size(); i++) {
                        FlowReport.nextStep();
                        FlowReport.addStep("Update 'StoryId' (" + listOfStoryIds.get(i) + ") field in DB", "");
                        setOfTestCases.updateInt("StoryId", listOfStoryIds.get(i));
                        setOfTestCases.updateRow();
                        FlowReport.reportAction("Field 'StoryId' updated", true);

                        if (setOfTestCases.getString("StoryName").contains("Name")) {
                            FlowReport.nextStep();
                            FlowReport.addStep("Updating 'StoryName' field in DB.", "");
                            setOfTestCases.updateString("StoryName", methods.genRandomEwerything("Name", ""));
                            setOfTestCases.updateRow();
                            FlowReport.reportAction("Field 'StoryName' updated", true);
                        } else {
                            FlowReport.reportAction("Something wrong with 'StoryName' field updating!", false);
                        }

                        FlowReport.nextStep();
                        FlowReport.addStep("Updating 'Tags' field in DB", "");
                        setOfTestCases.updateString("Tags", methods.genRandomEwerything("tag", "ProjectKey") + ", " + methods.genRandomEwerything("tag", "ProjectKey") + ", " + methods.genRandomEwerything("tag", "ProjectKey"));
                        setOfTestCases.updateRow();
                        FlowReport.reportAction("Field 'Tags' updated.", true);

                        FlowReport.nextStep();
                        FlowReport.addStep("Updating 'StoryDescription' field in DB.", "");
                        setOfTestCases.updateString("StoryDescription", methods.genRandomEwerything("StoryDescription", ""));
                        setOfTestCases.updateRow();
                        FlowReport.reportAction("Field 'StoryDescription' updated", true);

                        FlowReport.nextStep();
                        FlowReport.addStep("Receive the response from client method.", "");
                        String response = (String) method.invoke(obj, setOfTestCases);
                        FlowReport.reportAction("Response about updating story recieved ", true);
                        methods.saveAndAddResponse(response);

                        FlowReport.nextStep();
                        FlowReport.addStep("Validating recieved data", "");
                        if (response != null) {
                            Stories newStoriesInfo = methods.getStoriesInfoByProjectKey(setOfTestCases.getString("ProjectKey"));
                            List<String> newTagsList = methods.getTagsByProjectKey(setOfTestCases.getString("ProjectKey"));
                            newStoriesInfo = methods.setTagsList(newStoriesInfo.getStories(), newTagsList);
                            try {
                                String reportString = "";
                                if (oldStoriesInfo.equals(newStoriesInfo)) {
                                    reportString += "Story info doesn't changed ";
                                }
                                if (!reportString.equals("")) {
                                    FlowReport.reportAction(reportString, false);
                                } else {
                                    FlowReport.reportAction("Validation done good!", true);
                                }
                            } catch (Exception e) {
                                FlowReport.reportAction(e.getMessage(), false);
                            }
                        }
                    }
                } catch (Throwable ex) {
                    FlowReport.reportAction(ex.getMessage() == null ? "Response wasn`t returned" : ex.getMessage(), false);
                } finally {
                    methods.logOut();
                    FlowReport.finishTest();
                }
            }
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
    }

    // fails. Bug: REL-2669
    public void checkCreateScenario(ResultSet setOfTestCases) {
        try {
            FlowReport.initFlow(setOfTestCases, testInitiatorClass);
            while (setOfTestCases.next()) {
                try {
                    FlowReport.startTest(setOfTestCases.getString("TestCaseName"));
                    FlowReport.addStep("Create a new scenario in project.", "");
                    Method method = clientClass.getDeclaredMethod(responseMethodName, ResultSet.class);
                    Object obj = clientClass.newInstance();

                    try {
                        methods.authorizeWithSpecificCreds(setOfTestCases.getString("Login"), setOfTestCases.getString("Password"));
                        FlowReport.reportAction("Authorization done. Token received.", true);
                    } catch (NullPointerException e) {
                        FlowReport.reportAction(e.getMessage(), false);
                    }

                    //check that story is present in project or create it
                    List<Integer> listOfStoryIds = methods.checkThatStoryIsExist(setOfTestCases);

                    // rand some fields to create scenario
                    if (setOfTestCases.getString("ScenarioName").contains("Name")) {
                        FlowReport.nextStep();
                        FlowReport.addStep("Updating 'ScenarioName' field in DB.", "");
                        setOfTestCases.updateString("ScenarioName", methods.genRandomEwerything("Name", ""));
                        setOfTestCases.updateRow();
                        FlowReport.reportAction("Field 'ScenarioName' updated", true);
                    } else {
                        FlowReport.reportAction("Something wrong with 'ScenarioName' field updating!", false);
                    }

                    if (setOfTestCases.getString("TableName").contains("Table")) {
                        FlowReport.nextStep();
                        FlowReport.addStep("Updating 'TableName' field in DB.", "");
                        setOfTestCases.updateString("TableName", methods.genRandomEwerything("Table", ""));
                        setOfTestCases.updateRow();
                        FlowReport.reportAction("Field 'TableName' updated", true);
                    } else {
                        FlowReport.reportAction("Something wrong with 'TableName' field updating!", false);
                    }

                    FlowReport.nextStep();
                    FlowReport.addStep("Updating 'StoryId' (" + listOfStoryIds.get(0) + ") field in DB", "");
                    setOfTestCases.updateInt("StoryId", listOfStoryIds.get(0));
                    setOfTestCases.updateRow();
                    FlowReport.reportAction("Field 'StoryId' updated", true);

                    FlowReport.nextStep();
                    FlowReport.addStep("Receive the response from client method.", "");
                    String response = (String) method.invoke(obj, setOfTestCases);
                    ObjectMapper mapper = new ObjectMapper();
                    ScenarioCreateResponse responseObject = mapper.readValue(response, ScenarioCreateResponse.class);
                    FlowReport.reportAction("Response about creating scenario recieved ", true);
                    methods.saveAndAddResponse(response);

                    FlowReport.nextStep();
                    FlowReport.addStep("Validating recieved data", "");
                    if (response != null) {
                        ScenarioCreateResponse databaseObject = methods.getScenarioInfoFromDBByStoryId(setOfTestCases.getInt("StoryId"));
                        try {
                            if (responseObject.equals(databaseObject)) {
                                FlowReport.reportAction("Validation done good.", true);
                            } else {
                                FlowReport.reportAction("Validation failed!", false);
                            }
                        } catch (Exception e) {
                            FlowReport.reportAction(e.getMessage(), false);
                        }
                    }

                } catch (Throwable ex) {
                    FlowReport.reportAction(ex.getMessage() == null ? "Response wasn`t returned" : ex.getMessage(), false);
                } finally {
                    methods.logOut();
                    FlowReport.finishTest();
                }
            }
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
    }

    // fails. Bug: REL-2669
    public void checkCreateBackground(ResultSet setOfTestCases) {
        try {
            FlowReport.initFlow(setOfTestCases, testInitiatorClass);
            while (setOfTestCases.next()) {
                try {
                    FlowReport.startTest(setOfTestCases.getString("TestCaseName"));
                    FlowReport.addStep("Create a new scenario in project.", "");
                    Method method = clientClass.getDeclaredMethod(responseMethodName, ResultSet.class);
                    Object obj = clientClass.newInstance();

                    try {
                        methods.authorizeWithSpecificCreds(setOfTestCases.getString("Login"), setOfTestCases.getString("Password"));
                        FlowReport.reportAction("Authorization done. Token received.", true);
                    } catch (NullPointerException e) {
                        FlowReport.reportAction(e.getMessage(), false);
                    }

                    //check that story is present in project or create it
                    List<Integer> listOfStoryIds = methods.checkThatStoryIsExist(setOfTestCases);
                    for (int i = 0; i < listOfStoryIds.size(); ) {
                        FlowReport.nextStep();
                        FlowReport.addStep("Updating 'StoryId' (" + listOfStoryIds.get(i) + ") field in DB", "");
                        setOfTestCases.updateInt("StoryId", listOfStoryIds.get(i));
                        setOfTestCases.updateRow();
                        FlowReport.reportAction("Field 'StoryId' updated", true);

                        if (methods.checkThatStoryHaveBackground(setOfTestCases.getInt("StoryId"))) {
                            i++;
                            if (i == listOfStoryIds.size()) {
                                FlowReport.reportAction("All stories in project have background.", false);
                            }
                        } else {
                            break;
                        }
                    }

                    FlowReport.nextStep();
                    FlowReport.addStep("Receive the response from client method.", "");
                    String response = (String) method.invoke(obj, setOfTestCases);
                    ObjectMapper mapper = new ObjectMapper();
                    ScenarioCreateResponse responseObject = mapper.readValue(response, ScenarioCreateResponse.class);
                    FlowReport.reportAction("Response about creating scenario recieved ", true);
                    methods.saveAndAddResponse(response);

                    FlowReport.nextStep();
                    FlowReport.addStep("Validating recieved data", "");
                    if (response != null) {
                        ScenarioCreateResponse databaseObject = methods.getScenarioInfoFromDBByStoryId(setOfTestCases.getInt("StoryId"));
                        try {
                            if (responseObject.equals(databaseObject)) {
                                FlowReport.reportAction("Validation done good.", true);
                            } else {
                                FlowReport.reportAction("Validation failed!", false);
                            }
                        } catch (Exception e) {
                            FlowReport.reportAction(e.getMessage(), false);
                        }
                    }
                } catch (Throwable ex) {
                    FlowReport.reportAction(ex.getMessage() == null ? "Response wasn`t returned" : ex.getMessage(), false);
                } finally {
                    methods.logOut();
                    FlowReport.finishTest();
                }
            }
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
    }

    public void deleteScenarioCheck(ResultSet setOfTestCases) {
        try {
            FlowReport.initFlow(setOfTestCases, testInitiatorClass);
            while (setOfTestCases.next()) {
                try {
                    FlowReport.startTest(setOfTestCases.getString("TestCaseName"));
                    FlowReport.addStep("Create a new scenario in project.", "");
                    Method method = clientClass.getDeclaredMethod(responseMethodName, ResultSet.class);
                    Object obj = clientClass.newInstance();

                    try {
                        methods.authorizeWithSpecificCreds(setOfTestCases.getString("Login"), setOfTestCases.getString("Password"));
                        FlowReport.reportAction("Authorization done. Token received.", true);
                    } catch (NullPointerException e) {
                        FlowReport.reportAction(e.getMessage(), false);
                    }

                    // check that story is present in project or create it
                    List<Integer> listOfStoryIds = methods.checkThatStoryIsExist(setOfTestCases);

                    FlowReport.nextStep();
                    FlowReport.addStep("Updating 'StoryId' (" + listOfStoryIds.get(0) + ") field in DB.", "");
                    setOfTestCases.updateInt("StoryId", listOfStoryIds.get(0));
                    setOfTestCases.updateRow();
                    FlowReport.reportAction("Field 'StoryId' updated", true);

                    // check that story have a scenario, or create new one
                    List<Integer> listOfScenarioIds = methods.checkThatCsenarioIsExist(setOfTestCases);

                    FlowReport.nextStep();
                    FlowReport.addStep("Updating 'ScenarioId' (" + listOfScenarioIds.get(0) + ") field in DB.", "");
                    setOfTestCases.updateInt("ScenarioId", listOfScenarioIds.get(0));
                    setOfTestCases.updateRow();
                    FlowReport.reportAction("Field 'ScenarioId' updated", true);

                    FlowReport.nextStep();
                    FlowReport.addStep("Receive the response from client method.", "");
                    String response = (String) method.invoke(obj, setOfTestCases);
                    FlowReport.reportAction("Response about deleting scenario recieved ", true);
                    methods.saveAndAddResponse(response);

                    FlowReport.nextStep();
                    FlowReport.addStep("Validating recieved data", "");
                    if (response != null) {
                        List<Integer> newListOfScenarioIds = methods.getScenarioIdsListByStoryId(setOfTestCases.getInt("StoryId"));
                        try {
                            String reportString = "";
                            if (listOfScenarioIds.size() <= newListOfScenarioIds.size()) {
                                reportString += "Scenario ids lists are equals.";
                            }
                            if (newListOfScenarioIds.contains(setOfTestCases.getInt("ScenarioId"))) {
                                reportString += "Deleted scenario is still in DB.";
                            }
                            if (reportString.equals("")) {
                                FlowReport.reportAction("Validation done good!", true);
                            } else {
                                FlowReport.reportAction(reportString, false);
                            }
                        } catch (Exception e) {
                            FlowReport.reportAction(e.getMessage(), false);
                        }
                    }

                } catch (Throwable ex) {
                    FlowReport.reportAction(ex.getMessage() == null ? "Response wasn`t returned" : ex.getMessage(), false);
                } finally {
                    methods.logOut();
                    FlowReport.finishTest();
                }
            }
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
    }

    public void updateScenarioCheck(ResultSet setOfTestCases) {
        try {
            FlowReport.initFlow(setOfTestCases, testInitiatorClass);
            while (setOfTestCases.next()) {
                try {
                    FlowReport.startTest(setOfTestCases.getString("TestCaseName"));
                    FlowReport.addStep("Create a new scenario in project.", "");
                    Method method = clientClass.getDeclaredMethod(responseMethodName, ResultSet.class);
                    Object obj = clientClass.newInstance();

                    try {
                        methods.authorizeWithSpecificCreds(setOfTestCases.getString("Login"), setOfTestCases.getString("Password"));
                        FlowReport.reportAction("Authorization done. Token received.", true);
                    } catch (NullPointerException e) {
                        FlowReport.reportAction(e.getMessage(), false);
                    }

                    // check that story is present in project or create it
                    List<Integer> listOfStoryIds = methods.checkThatStoryIsExist(setOfTestCases);


                    FlowReport.nextStep();
                    FlowReport.addStep("Updating 'StoryId' (" + listOfStoryIds.get(0) + ") field in DB.", "");
                    setOfTestCases.updateInt("StoryId", listOfStoryIds.get(0));
                    setOfTestCases.updateRow();
                    FlowReport.reportAction("Field 'StoryId' updated", true);

                    // check that story have a scenario, or create new one
                    List<Integer> listOfScenarioIds = methods.checkThatCsenarioIsExist(setOfTestCases);

                    for (int i = 0; i < listOfScenarioIds.size(); i++) {
                        FlowReport.nextStep();
                        FlowReport.addStep("Updating 'ScenarioId' (" + listOfScenarioIds.get(i) + ") field in DB.", "");
                        setOfTestCases.updateInt("ScenarioId", listOfScenarioIds.get(i));
                        setOfTestCases.updateRow();
                        FlowReport.reportAction("Field 'ScenarioId' updated", true);

                        // rand some fields to update scenario
                        FlowReport.nextStep();
                        FlowReport.addStep("Updating 'ScenarioName' field in DB.", "");
                        setOfTestCases.updateString("ScenarioName", methods.genRandomEwerything("Name", ""));
                        setOfTestCases.updateRow();
                        FlowReport.reportAction("Field 'ScenarioName' updated", true);

                        FlowReport.nextStep();
                        FlowReport.addStep("Updating 'ScenarioDescription' field in DB.", "");
                        setOfTestCases.updateString("ScenarioDescription", methods.genRandomEwerything("ScenarioDescription", ""));
                        setOfTestCases.updateRow();
                        FlowReport.reportAction("Field 'ScenarioDescription' updated", true);

                        FlowReport.nextStep();
                        FlowReport.addStep("Updating 'TableName' field in DB.", "");
                        setOfTestCases.updateString("TableName", methods.genRandomEwerything("Table", ""));
                        setOfTestCases.updateRow();
                        FlowReport.reportAction("Field 'TableName' updated", true);

                        FlowReport.nextStep();
                        FlowReport.addStep("Updating 'TableDescription' field in DB.", "");
                        setOfTestCases.updateString("TableDescription", methods.genRandomEwerything("TableDescription", ""));
                        setOfTestCases.updateRow();
                        FlowReport.reportAction("Field 'TableDescription' updated", true);

                        FlowReport.nextStep();
                        FlowReport.addStep("Updating 'Tags' field in DB", "");
                        setOfTestCases.updateString("Tags", methods.genRandomEwerything("tag", "ProjectKey") + ", " + methods.genRandomEwerything("tag", "ProjectKey") + ", " + methods.genRandomEwerything("tag", "ProjectKey"));
                        setOfTestCases.updateRow();
                        FlowReport.reportAction("Field 'Tags' updated.", true);

                        FlowReport.nextStep();
                        FlowReport.addStep("Updating 'StepText' field in DB.", "");
                        setOfTestCases.updateString("StepText", "Given " + methods.genRandomEwerything("Step", "") + ", " + "When " + methods.genRandomEwerything("Step", "") + ", " + "Then " + methods.genRandomEwerything("Step", "") + " <arg1> <arg2>");
                        setOfTestCases.updateRow();
                        FlowReport.reportAction("Field 'StepText' updated", true);

                        FlowReport.nextStep();
                        FlowReport.addStep("Updating 'Data' field in DB.", "");
                        setOfTestCases.updateString("Data", methods.genRandomEwerything("", "ProjectKey") + ", " + methods.genRandomEwerything("", "ProjectKey"));
                        setOfTestCases.updateRow();
                        FlowReport.reportAction("Field 'Data' updated", true);

                        ScenarioInfoResponse oldDatabaseObject = methods.getScenarioInfoByScenarioIdFromDB(setOfTestCases.getInt("ScenarioId"));

                        FlowReport.nextStep();
                        FlowReport.addStep("Receive the response from client method.", "");
                        String response = (String) method.invoke(obj, setOfTestCases);
                        FlowReport.reportAction("Response about updating scenario recieved ", true);
                        methods.saveAndAddResponse(response);

                        FlowReport.nextStep();
                        FlowReport.addStep("Validating recieved data", "");
                        if (response != null) {
                            ScenarioInfoResponse newDatabaseObject = methods.getScenarioInfoByScenarioIdFromDB(setOfTestCases.getInt("ScenarioId"));
                            try {
                                if (!oldDatabaseObject.equals(newDatabaseObject)) {
                                    FlowReport.reportAction("Validation done good!", true);
                                } else {
                                    FlowReport.reportAction("Validation failed!", false);
                                }
                            } catch (Exception e) {
                                FlowReport.reportAction(e.getMessage(), false);
                            }
                        }
                    }
                } catch (Throwable ex) {
                    FlowReport.reportAction(ex.getMessage() == null ? "Response wasn`t returned" : ex.getMessage(), false);
                } finally {
                    methods.logOut();
                    FlowReport.finishTest();
                }
            }
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
    }

    public void checkScenarioInfoByScenarioId(ResultSet setOfTestCases) {
        try {
            FlowReport.initFlow(setOfTestCases, testInitiatorClass);
            while (setOfTestCases.next()) {
                try {
                    FlowReport.startTest(setOfTestCases.getString("TestCaseName"));
                    FlowReport.addStep("Create a new scenario in project.", "");
                    Method method = clientClass.getDeclaredMethod(responseMethodName, ResultSet.class);
                    Object obj = clientClass.newInstance();

                    try {
                        methods.authorizeWithSpecificCreds(setOfTestCases.getString("Login"), setOfTestCases.getString("Password"));
                        FlowReport.reportAction("Authorization done. Token received.", true);
                    } catch (NullPointerException e) {
                        FlowReport.reportAction(e.getMessage(), false);
                    }

                    //if domains is absent - create new
                    List<String> activeDomainsList = methods.checkThatDomainsIsExist(setOfTestCases);

                    FlowReport.nextStep();
                    FlowReport.addStep("Updating 'DomainName'(" + activeDomainsList.get(0) + ") field in DB.", "");
                    setOfTestCases.updateString("DomainName", activeDomainsList.get(0));
                    setOfTestCases.updateRow();
                    FlowReport.reportAction("Field 'DomainName' updated", true);

                    //if projects is absent - create new
                    List<String> listOfActiveProjectKeys = methods.checkThatProjectsIsExist(setOfTestCases);

                    FlowReport.nextStep();
                    FlowReport.addStep("Updating 'ProjectKey'(" + listOfActiveProjectKeys.get(0) + ") field in DB.", "");
                    setOfTestCases.updateString("ProjectKey", listOfActiveProjectKeys.get(0));
                    setOfTestCases.updateRow();
                    FlowReport.reportAction("Field 'ProjectKey' updated", true);

                    // check that story is present in project or create it
                    List<Integer> listOfStoryIds = methods.checkThatStoryIsExist(setOfTestCases);

                    FlowReport.nextStep();
                    FlowReport.addStep("Updating 'StoryId' (" + listOfStoryIds.get(0) + ") field in DB.", "");
                    setOfTestCases.updateInt("StoryId", listOfStoryIds.get(0));
                    setOfTestCases.updateRow();
                    FlowReport.reportAction("Field 'StoryId' updated", true);

                    // check that story have a scenario, or create new one
                    List<Integer> listOfScenarioIds = methods.checkThatCsenarioIsExist(setOfTestCases);

                    for (int i = 0; i < listOfScenarioIds.size(); i++) {
                        FlowReport.nextStep();
                        FlowReport.addStep("Updating 'ScenarioId' (" + listOfScenarioIds.get(i) + ") field in DB.", "");
                        setOfTestCases.updateInt("ScenarioId", listOfScenarioIds.get(0));
                        setOfTestCases.updateRow();
                        FlowReport.reportAction("Field 'ScenarioId' updated", true);

                        FlowReport.nextStep();
                        FlowReport.addStep("Receive the response from client method.", "");
                        String response = (String) method.invoke(obj, setOfTestCases);
                        ObjectMapper mapper = new ObjectMapper();
                        ScenarioInfoResponse responseObject = mapper.readValue(response, ScenarioInfoResponse.class);
                        FlowReport.reportAction("Response about updating scenario recieved ", true);
                        methods.saveAndAddResponse(response);

                        FlowReport.nextStep();
                        FlowReport.addStep("Validating recieved data", "");
                        if (response != null) {
                            ScenarioInfoResponse databaseObject = methods.getScenarioInfoByScenarioIdFromDB(setOfTestCases.getInt("ScenarioId"));
                            try {
                                if (responseObject.equals(databaseObject)) {
                                    FlowReport.reportAction("Validation done good!", true);
                                } else {
                                    FlowReport.reportAction("Validation failed!", false);
                                }
                            } catch (Exception e) {
                                FlowReport.reportAction(e.getMessage(), false);
                            }
                        }
                    }
                } catch (Throwable ex) {
                    FlowReport.reportAction(ex.getMessage() == null ? "Response wasn`t returned" : ex.getMessage(), false);
                } finally {
                    methods.logOut();
                    FlowReport.finishTest();
                }
            }
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
    }

    public void checkCreateTaskTracker(ResultSet setOfTestCases) {
        try {
            FlowReport.initFlow(setOfTestCases, testInitiatorClass);
            while (setOfTestCases.next()) {
                try {
                    FlowReport.startTest(setOfTestCases.getString("TestCaseName"));
                    FlowReport.addStep("Add TTS to project.", "");
                    Method method = clientClass.getDeclaredMethod(responseMethodName, ResultSet.class);
                    Object obj = clientClass.newInstance();

                    try {
                        methods.authorizeWithSpecificCreds(setOfTestCases.getString("Login"), setOfTestCases.getString("Password"));
                        FlowReport.reportAction("Authorization done. Token received.", true);
                    } catch (NullPointerException e) {
                        FlowReport.reportAction(e.getMessage(), false);
                    }

                    //if domains is absent - create new
                    List<String> activeDomainsList = methods.checkThatDomainsIsExist(setOfTestCases);

                    // create new project without tts
                    FlowReport.nextStep();
                    FlowReport.addStep("Updating 'DomainName' (" + activeDomainsList.get(0) + ") field in DB.", "");
                    setOfTestCases.updateString("DomainName", activeDomainsList.get(0));
                    setOfTestCases.updateRow();
                    FlowReport.reportAction("Field 'DomainName' updated", true);

                    FlowReport.nextStep();
                    FlowReport.addStep("Updating 'ProjectKey' field in DB.", "");
                    setOfTestCases.updateString("ProjectKey", methods.genRandomEwerything("", "ProjectKey"));
                    setOfTestCases.updateRow();
                    FlowReport.reportAction("Field 'ProjectKey' updated", true);

                    FlowReport.nextStep();
                    FlowReport.addStep("Updating 'ProjectName' field in DB.", "");
                    setOfTestCases.updateString("ProjectName", methods.genRandomEwerything("Name", ""));
                    setOfTestCases.updateRow();
                    FlowReport.reportAction("Field 'ProjectName' updated", true);

                    FlowReport.nextStep();
                    FlowReport.addStep("Creating new project without tts", "");
                    projectClient.getCreateProjectResponse(setOfTestCases);
                    FlowReport.reportAction("Project without tts created.", true);

                    FlowReport.nextStep();
                    FlowReport.addStep("Receive the response from client method.", "");
                    String response = (String) method.invoke(obj, setOfTestCases);
                    FlowReport.reportAction("Response about adding vcs recieved ", true);
                    methods.saveAndAddResponse(response);

                    FlowReport.nextStep();
                    FlowReport.addStep("Validating recieved data", "");
                    if (response != null) {
                        JSONObject jsonObject = new JSONObject(response);
                        Integer responseInfo = jsonObject.getInt("id");
                        ManagementTool databaseInfo = methods.getProjectManagementToolIdByProjectKey(setOfTestCases.getString("ProjectKey"));
                        try {
                            if (responseInfo.equals(databaseInfo.getId())) {
                                FlowReport.reportAction("Data succsessfuly validated with RelimeDatabase", true);
                                List<String> activeProjectIdsList = methods.getIdOfActiveProjectByDomainName(setOfTestCases.getString("DomainName"));
                                FlowReport.nextStep();
                                FlowReport.addStep("Deleting this (" + setOfTestCases.getString("ProjectKey") + ") project from relime.", "");
                                setOfTestCases.updateString("ProjectId", activeProjectIdsList.get(0));
                                setOfTestCases.updateRow();
                                projectClient.getDeleteProjectResponse(setOfTestCases);
                                FlowReport.reportAction("Project deleted!", true);
                            } else {
                                FlowReport.reportAction("Data in DB and in response is not the same", false);
                            }
                        } catch (Exception e) {
                            FlowReport.reportAction(e.getMessage(), false);
                        }
                    }
                } catch (Throwable ex) {
                    FlowReport.reportAction(ex.getMessage() == null ? "Response wasn`t returned" : ex.getMessage(), false);
                } finally {
                    methods.logOut();
                    FlowReport.finishTest();
                }
            }
        } catch (
                Exception e) {
            System.out.print(e.getMessage());
        }
    }

    public void checkDeleteTaskTracker(ResultSet setOfTestCases) {
        try {
            FlowReport.initFlow(setOfTestCases, testInitiatorClass);
            while (setOfTestCases.next()) {
                try {
                    FlowReport.startTest(setOfTestCases.getString("TestCaseName"));
                    FlowReport.addStep("Create a new scenario in project.", "");
                    Method method = clientClass.getDeclaredMethod(responseMethodName, ResultSet.class);
                    Object obj = clientClass.newInstance();

                    try {
                        methods.authorizeWithSpecificCreds(setOfTestCases.getString("Login"), setOfTestCases.getString("Password"));
                        FlowReport.reportAction("Authorization done. Token received.", true);
                    } catch (NullPointerException e) {
                        FlowReport.reportAction(e.getMessage(), false);
                    }

                    // if domains is absent - create new
                    List<String> activeDomainsList = methods.checkThatDomainsIsExist(setOfTestCases);

                    FlowReport.nextStep();
                    FlowReport.addStep("Updating 'DomainName' (" + activeDomainsList.get(0) + ") field in DB.", "");
                    setOfTestCases.updateString("DomainName", activeDomainsList.get(0));
                    setOfTestCases.updateRow();
                    FlowReport.reportAction("Field 'DomainName' updated", true);

                    // if projects doesn't exist - create new
                    List<String> listOfACtiveProjects = methods.checkThatProjectsIsExist(setOfTestCases);

                    FlowReport.nextStep();
                    FlowReport.addStep("Updating 'ProjectKey' (" + listOfACtiveProjects.get(0) + ") field in DB.", "");
                    setOfTestCases.updateString("ProjectKey", listOfACtiveProjects.get(0));
                    setOfTestCases.updateRow();
                    FlowReport.reportAction("Field 'ProjectKey' updated", true);

                    // check that project have tts, or add it
                    Integer oldTTSId = methods.checkThatTtsIsExist(setOfTestCases);

                    FlowReport.nextStep();
                    FlowReport.addStep("Updating 'TaskTrackerId' (" + oldTTSId + ") field in DB.", "");
                    setOfTestCases.updateInt("TaskTrackerId", oldTTSId);
                    setOfTestCases.updateRow();
                    FlowReport.reportAction("Field 'TaskTrackerId' updated", true);

                    FlowReport.nextStep();
                    FlowReport.addStep("Receive the response from client method.", "");
                    String response = (String) method.invoke(obj, setOfTestCases);
                    FlowReport.reportAction("Response about deleting scenario recieved ", true);
                    methods.saveAndAddResponse(response);

                    FlowReport.nextStep();
                    FlowReport.addStep("Validating recieved data", "");
                    if (response != null) {
                        TaskTrackers taskTrackers = methods.getTaskTrackerInfoFromDbByProjectKey(setOfTestCases.getString("ProjectKey"));
                        try {
                            if (taskTrackers.getTaskTrackers().size() == 0) {
                                FlowReport.reportAction("Validation done. Tts was removed successfully.", true);
                            } else {
                                FlowReport.reportAction("Validation faild, project still have active tts.", false);
                            }
                        } catch (Exception e) {
                            FlowReport.reportAction(e.getMessage(), false);
                        }
                    }

                } catch (Throwable ex) {
                    FlowReport.reportAction(ex.getMessage() == null ? "Response wasn`t returned" : ex.getMessage(), false);
                } finally {
                    methods.logOut();
                    FlowReport.finishTest();
                }
            }
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
    }

    // fails because sql query is invslid. belongsToAccount field can be null
    public void checkRetrieveTaskTracker(ResultSet setOfTestCases) {
        try {
            FlowReport.initFlow(setOfTestCases, testInitiatorClass);
            while (setOfTestCases.next()) {
                try {
                    FlowReport.startTest(setOfTestCases.getString("TestCaseName"));
                    FlowReport.addStep("Return vcs project info if project have active vcs.", "");
                    Method method = clientClass.getDeclaredMethod(responseMethodName, ResultSet.class);
                    Object obj = clientClass.newInstance();

                    try {
                        methods.authorizeWithSpecificCreds(setOfTestCases.getString("Login"), setOfTestCases.getString("Password"));
                        FlowReport.reportAction("Authorization done. Token received.", true);
                    } catch (NullPointerException e) {
                        FlowReport.reportAction(e.getMessage(), false);
                    }

                    //if domains is absent - create new
                    List<String> activeDomainsList = methods.checkThatDomainsIsExist(setOfTestCases);

                    FlowReport.nextStep();
                    FlowReport.addStep("Updating 'DomainName' (" + activeDomainsList.get(0) + ") field in DB.", "");
                    setOfTestCases.updateString("DomainName", activeDomainsList.get(0));
                    setOfTestCases.updateRow();
                    FlowReport.reportAction("Field 'DomainName' updated", true);

                    //if projects is absent - create new
                    List<String> listOfActiveProjectKeys = methods.checkThatProjectsIsExist(setOfTestCases);


                    for (int i = 0; i < listOfActiveProjectKeys.size(); i++) {
                        FlowReport.nextStep();
                        FlowReport.addStep("Updating 'ProjectKey' (" + listOfActiveProjectKeys.get(i) + ") field in DB.", "");
                        setOfTestCases.updateString("ProjectKey", listOfActiveProjectKeys.get(i));
                        setOfTestCases.updateRow();
                        FlowReport.reportAction("Field 'ProjectKey' updated", true);

                        // check that project have active tts, or add it to project
                        Integer ttsId = methods.checkThatTtsIsExist(setOfTestCases);

                        FlowReport.nextStep();
                        FlowReport.addStep("Receive the response from client method.", "");
                        String response = (String) method.invoke(obj, setOfTestCases);
                        String upgradeResponse = "{\"taskTrackers\":" + response + "}";
                        ObjectMapper mapper = new ObjectMapper();
                        TaskTrackers responseObject = mapper.readValue(upgradeResponse, TaskTrackers.class);
                        FlowReport.reportAction("Response about project tts recieved ", true);
                        methods.saveAndAddResponse(response);

                        FlowReport.nextStep();
                        FlowReport.addStep("Validating recieved data", "");
                        if (response != null) {
                            TaskTrackers databaseObject = methods.getTaskTrackerInfoFromDbByProjectKey(setOfTestCases.getString("ProjectKey"));
                            try {
                                if (methods.validateTaskTrackersInfo(responseObject, databaseObject)) {
                                    FlowReport.reportAction("Data succsessfuly validated with RelimeDatabase", true);
                                } else {
                                    FlowReport.reportAction("Data in DB and in response is not the same", false);
                                }
                            } catch (Exception e) {
                                FlowReport.reportAction(e.getMessage() == null ? e.getClass().getName() : e.getMessage(), false);
                            }
                        }
                    }
                } catch (Throwable ex) {
                    FlowReport.reportAction(ex.getMessage() == null ? "Response wasn`t returned" : ex.getMessage(), false);
                } finally {
                    methods.logOut();
                    FlowReport.finishTest();
                }
            }
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
    }

    // how to validate it as well. Issue: REL-2749
    public void checkValidateConnectionToTaskTracker(ResultSet setOfTestCases) {
        try {
            FlowReport.initFlow(setOfTestCases, testInitiatorClass);
            while (setOfTestCases.next()) {
                try {
                    FlowReport.startTest(setOfTestCases.getString("TestCaseName"));
                    FlowReport.addStep("Add TTS to project.", "");
                    Method method = clientClass.getDeclaredMethod(responseMethodName, ResultSet.class);
                    Object obj = clientClass.newInstance();

                    try {
                        methods.authorizeWithSpecificCreds(setOfTestCases.getString("Login"), setOfTestCases.getString("Password"));
                        FlowReport.reportAction("Authorization done. Token received.", true);
                    } catch (NullPointerException e) {
                        FlowReport.reportAction(e.getMessage(), false);
                    }

                    // if domains is absent - create new
                    List<String> activeDomainsList = methods.checkThatDomainsIsExist(setOfTestCases);

                    FlowReport.nextStep();
                    FlowReport.addStep("Updating 'DomainName' (" + activeDomainsList.get(0) + ") field in DB.", "");
                    setOfTestCases.updateString("DomainName", activeDomainsList.get(0));
                    setOfTestCases.updateRow();
                    FlowReport.reportAction("Field 'DomainName' updated", true);

                    // if projects doesn't exist - create new
                    List<String> listOfACtiveProjects = methods.checkThatProjectsIsExist(setOfTestCases);

                    FlowReport.nextStep();
                    FlowReport.addStep("Updating 'ProjectKey' (" + listOfACtiveProjects.get(0) + ") field in DB.", "");
                    setOfTestCases.updateString("ProjectKey", listOfACtiveProjects.get(0));
                    setOfTestCases.updateRow();
                    FlowReport.reportAction("Field 'ProjectKey' updated", true);

                    // check that project have tts, or add it
                    Integer oldTTSId = methods.checkThatTtsIsExist(setOfTestCases);

                    FlowReport.nextStep();
                    FlowReport.addStep("Updating 'TaskTrackerId' (" + oldTTSId + ") field in DB.", "");
                    setOfTestCases.updateInt("TaskTrackerId", oldTTSId);
                    setOfTestCases.updateRow();
                    FlowReport.reportAction("Field 'TaskTrackerId' updated", true);

                    Integer jiraAccountId = methods.getJiraAccountIdByProjectKey(setOfTestCases.getString("ProjectKey"));

                    FlowReport.nextStep();
                    FlowReport.addStep("Updating 'BelongsToAccount' (" + jiraAccountId + ") field in DB.", "");
                    setOfTestCases.updateInt("BelongsToAccount", jiraAccountId);
                    setOfTestCases.updateRow();
                    FlowReport.reportAction("Field 'BelongsToAccount' updated", true);

                    FlowReport.nextStep();
                    FlowReport.addStep("Receive the response from client method.", "");
                    String response = (String) method.invoke(obj, setOfTestCases);
                    FlowReport.reportAction("Response about adding vcs recieved ", true);
                    methods.saveAndAddResponse(response);

                    FlowReport.nextStep();
                    FlowReport.addStep("Validating recieved data", "");
                    if (response != null) {
                        JSONObject jsonObject = new JSONObject(response);
                        Boolean responseInfo = jsonObject.getBoolean("isAccessible");
                        try {
                            if (responseInfo.equals(true) | responseInfo.equals(false)) {
                                FlowReport.reportAction("Data succsessfuly validated!", true);
                            } else {
                                FlowReport.reportAction("Data in DB and in response is not the same", false);
                            }
                        } catch (Exception e) {
                            FlowReport.reportAction(e.getMessage(), false);
                        }
                    }
                } catch (Throwable ex) {
                    FlowReport.reportAction(ex.getMessage() == null ? "Response wasn`t returned" : ex.getMessage(), false);
                } finally {
                    methods.logOut();
                    FlowReport.finishTest();
                }
            }
        } catch (
                Exception e) {
            System.out.print(e.getMessage());
        }
    }

    public void checkUpdateTaskTracker(ResultSet setOfTestCases) {
        try {
            FlowReport.initFlow(setOfTestCases, testInitiatorClass);
            while (setOfTestCases.next()) {
                try {
                    FlowReport.startTest(setOfTestCases.getString("TestCaseName"));
                    FlowReport.addStep("Add TTS to project.", "");
                    Method method = clientClass.getDeclaredMethod(responseMethodName, ResultSet.class);
                    Object obj = clientClass.newInstance();

                    try {
                        methods.authorizeWithSpecificCreds(setOfTestCases.getString("Login"), setOfTestCases.getString("Password"));
                        FlowReport.reportAction("Authorization done. Token received.", true);
                    } catch (NullPointerException e) {
                        FlowReport.reportAction(e.getMessage(), false);
                    }

                    // if domains is absent - create new
                    List<String> activeDomainsList = methods.checkThatDomainsIsExist(setOfTestCases);

                    FlowReport.nextStep();
                    FlowReport.addStep("Updating 'DomainName' (" + activeDomainsList.get(0) + ") field in DB.", "");
                    setOfTestCases.updateString("DomainName", activeDomainsList.get(0));
                    setOfTestCases.updateRow();
                    FlowReport.reportAction("Field 'DomainName' updated", true);

                    FlowReport.nextStep();
                    FlowReport.addStep("Updating 'ProjectKey' field in DB.", "");
                    setOfTestCases.updateString("ProjectKey", methods.genRandomEwerything("", "ProjectKey"));
                    setOfTestCases.updateRow();
                    FlowReport.reportAction("Field 'ProjectKey' updated", true);

                    FlowReport.nextStep();
                    FlowReport.addStep("Updating 'ProjectName' field in DB.", "");
                    setOfTestCases.updateString("ProjectName", methods.genRandomEwerything("Name", ""));
                    setOfTestCases.updateRow();
                    FlowReport.reportAction("Field 'ProjectName' updated", true);

                    FlowReport.nextStep();
                    FlowReport.addStep("Creating new project without tts", "");
                    projectClient.getCreateProjectResponse(setOfTestCases);
                    FlowReport.reportAction("Project without tts created.", true);

                    // check that project have tts, or add it
                    Integer oldTTSId = methods.checkThatTtsIsExist(setOfTestCases);

                    FlowReport.nextStep();
                    FlowReport.addStep("Updating 'TaskTrackerId' (" + oldTTSId + ") field in DB.", "");
                    setOfTestCases.updateInt("TaskTrackerId", oldTTSId);
                    setOfTestCases.updateRow();
                    FlowReport.reportAction("Field 'TaskTrackerId' updated", true);

                    FlowReport.nextStep();
                    FlowReport.addStep("Updating 'TaskTrackerURL' field in DB.", "");
                    setOfTestCases.updateString("TaskTrackerURL", methods.genRandomEwerything("", "JiraURL"));
                    setOfTestCases.updateRow();
                    FlowReport.reportAction("Field 'TaskTrackerURL' updated", true);

                    ManagementTool oldDatabaseInfo = methods.getProjectManagementToolIdByProjectKey(setOfTestCases.getString("ProjectKey"));

                    FlowReport.nextStep();
                    FlowReport.addStep("Receive the response from client method.", "");
                    String response = (String) method.invoke(obj, setOfTestCases);
                    FlowReport.reportAction("Response about adding vcs recieved ", true);
                    methods.saveAndAddResponse(response);

                    FlowReport.nextStep();
                    FlowReport.addStep("Validating recieved data", "");
                    if (response != null) {
                        ManagementTool newDatabaseInfo = methods.getProjectManagementToolIdByProjectKey(setOfTestCases.getString("ProjectKey"));
                        try {
                            if (!oldDatabaseInfo.equals(newDatabaseInfo)) {
                                FlowReport.reportAction("Data succsessfuly validated with RelimeDatabase", true);
                            } else {
                                FlowReport.reportAction("Data in DB and in response is not the same", false);
                            }

                            List<String> activeProjectIdsList = methods.getIdOfActiveProjectByDomainName(setOfTestCases.getString("DomainName"));
                            FlowReport.nextStep();
                            FlowReport.addStep("Deleting this (" + setOfTestCases.getString("ProjectKey") + ") project from relime.", "");
                            setOfTestCases.updateString("ProjectId", activeProjectIdsList.get(0));
                            setOfTestCases.updateRow();
                            projectClient.getDeleteProjectResponse(setOfTestCases);
                            FlowReport.reportAction("Project deleted!", true);
                        } catch (Exception e) {
                            FlowReport.reportAction(e.getMessage(), false);
                        }
                    }
                } catch (Throwable ex) {
                    FlowReport.reportAction(ex.getMessage() == null ? "Response wasn`t returned" : ex.getMessage(), false);
                } finally {
                    methods.logOut();
                    FlowReport.finishTest();
                }
            }
        } catch (
                Exception e) {
            System.out.print(e.getMessage());
        }
    }

    public void checkIsJiraUniqueStory(ResultSet setOfTestCases) {
        try {
            FlowReport.initFlow(setOfTestCases, testInitiatorClass);
            while (setOfTestCases.next()) {
                try {
                    FlowReport.startTest(setOfTestCases.getString("TestCaseName"));
                    FlowReport.addStep("Return vcs project info if project have active vcs.", "");
                    Method method = clientClass.getDeclaredMethod(responseMethodName, ResultSet.class);
                    Object obj = clientClass.newInstance();

                    try {
                        methods.authorizeWithSpecificCreds(setOfTestCases.getString("Login"), setOfTestCases.getString("Password"));
                        FlowReport.reportAction("Authorization done. Token received.", true);
                    } catch (NullPointerException e) {
                        FlowReport.reportAction(e.getMessage(), false);
                    }

                    //if domains is absent - create new
                    List<String> activeDomainsList = methods.checkThatDomainsIsExist(setOfTestCases);

                    FlowReport.nextStep();
                    FlowReport.addStep("Updating 'DomainName' (" + activeDomainsList.get(0) + ") field in DB.", "");
                    setOfTestCases.updateString("DomainName", activeDomainsList.get(0));
                    setOfTestCases.updateRow();
                    FlowReport.reportAction("Field 'DomainName' updated", true);

                    //if projects is absent - create new
                    List<String> listOfActiveProjectKeys = methods.checkThatProjectsIsExist(setOfTestCases);


                    for (int i = 0; i < listOfActiveProjectKeys.size(); i++) {
                        FlowReport.nextStep();
                        FlowReport.addStep("Updating 'ProjectKey' (" + listOfActiveProjectKeys.get(i) + ") field in DB.", "");
                        setOfTestCases.updateString("ProjectKey", listOfActiveProjectKeys.get(i));
                        setOfTestCases.updateRow();
                        FlowReport.reportAction("Field 'ProjectKey' updated", true);

                        FlowReport.nextStep();
                        FlowReport.addStep("Receive the response from client method.", "");
                        String response = (String) method.invoke(obj, setOfTestCases);
                        FlowReport.reportAction("Response about project tts recieved ", true);
                        methods.saveAndAddResponse(response);

                        FlowReport.nextStep();
                        FlowReport.addStep("Validating recieved data", "");
                        if (response != null) {
                            Story jiraStoryList = methods.getListOfJiraLinkedStoryByJiraStoryKey(setOfTestCases.getString("NewJiraStoryKey"));
                            JSONObject jsonObject = new JSONObject(response);
                            String exceptionText = jsonObject.getString("exceptionText");
                            try {
                                String reportString = "";
                                if (!exceptionText.equals("")) {
                                    if (!jiraStoryList.getKey().equals(setOfTestCases.getString("NewJiraStoryKey"))) {
                                        reportString += "Something wrong with exceptionText in response.";
                                    }
                                }
                                if (exceptionText.equals("")) {
                                    if (!(jiraStoryList.equals(null))) {
                                        reportString += "JiraStoryList must be null";
                                    }
                                }
                                if (reportString.equals("")) {
                                    FlowReport.reportAction("Validation done good.", true);
                                } else {
                                    FlowReport.reportAction("Validation failed.", false);
                                }
                            } catch (Exception e) {
                                FlowReport.reportAction(e.getMessage(), false);
                            }
                        }
                    }
                } catch (Throwable ex) {
                    FlowReport.reportAction(ex.getMessage() == null ? "Response wasn`t returned" : ex.getMessage(), false);
                } finally {
                    methods.logOut();
                    FlowReport.finishTest();
                }
            }
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
    }

    // Done. Ask Zhenya about validation.
    public void checkIsVcsLinkedProject(ResultSet setOfTestCases) {
        try {
            FlowReport.initFlow(setOfTestCases, testInitiatorClass);
            while (setOfTestCases.next()) {
                try {
                    FlowReport.startTest(setOfTestCases.getString("TestCaseName"));
                    FlowReport.addStep("Check can retrieve project by project key.", "");
                    Method method = clientClass.getDeclaredMethod(responseMethodName, ResultSet.class);
                    Object obj = clientClass.newInstance();
                    FlowReport.reportAction("An instance created.", true);

                    try {
                        FlowReport.nextStep();
                        FlowReport.addStep("Authorization...", "");
                        methods.authorizeWithSpecificCreds(setOfTestCases.getString("Login"), setOfTestCases.getString("Password"));
                        FlowReport.reportAction("Authorization done. Token received.", true);
                    } catch (NullPointerException e) {
                        FlowReport.reportAction(e.getMessage(), false);
                    }

                    //if domains is absent - create new
                    List<String> activeDomainsList = methods.checkThatDomainsIsExist(setOfTestCases);

                    FlowReport.nextStep();
                    FlowReport.addStep("Updating 'DomainName'(" + activeDomainsList.get(0) + ") field in DB.", "");
                    setOfTestCases.updateString("DomainName", activeDomainsList.get(0));
                    setOfTestCases.updateRow();
                    FlowReport.reportAction("Field 'DomainName' updated", true);

                    //if projects is absent - create new
                    List<String> listOfActiveProjectKeys = methods.checkThatProjectsIsExist(setOfTestCases);

                    for (int i = 0; i < listOfActiveProjectKeys.size(); i++) {
                        FlowReport.nextStep();
                        FlowReport.addStep("Updating 'ProjectKey' (" + listOfActiveProjectKeys.get(i) + ") field in DB.", "");
                        setOfTestCases.updateString("ProjectKey", "UPL");
                        setOfTestCases.updateRow();
                        FlowReport.reportAction("Field 'ProjectKey' updated", true);

                        FlowReport.nextStep();
                        FlowReport.addStep("Receive the response from client method.", "");
                        String response = (String) method.invoke(obj, setOfTestCases);
                        FlowReport.reportAction("Response about retrieved project recieved ", true);
                        methods.saveAndAddResponse(response);

                        FlowReport.nextStep();
                        FlowReport.addStep("Validating recieved data", "");
                        if (response != null) {
                            try {
                                if (response.contains("isProjectUploaded")) {
                                    FlowReport.reportAction("Validation done!", true);
                                } else {
                                    FlowReport.reportAction("It is no 200 OK status code, something wrong.", false);
                                }
                            } catch (Exception e) {
                                FlowReport.reportAction(e.getMessage(), false);
                            }
                        }
                    }
                } catch (Throwable ex) {

                    FlowReport.reportAction(ex.getMessage() == null ? "Response wasn`t returned" : ex.getMessage(), false);
                } finally {
                    methods.logOut();
                    FlowReport.finishTest();
                }
            }
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
    }

    // Done. Ask Zhenya about validation.
    public void checkValidateVcs(ResultSet setOfTestCases) {
        try {
            FlowReport.initFlow(setOfTestCases, testInitiatorClass);
            while (setOfTestCases.next()) {
                try {
                    FlowReport.startTest(setOfTestCases.getString("TestCaseName"));
                    FlowReport.addStep("Return vcs project info if project have active vcs.", "");
                    Method method = clientClass.getDeclaredMethod(responseMethodName, ResultSet.class);
                    Object obj = clientClass.newInstance();

                    try {
                        methods.authorizeWithSpecificCreds(setOfTestCases.getString("Login"), setOfTestCases.getString("Password"));
                        FlowReport.reportAction("Authorization done. Token received.", true);
                    } catch (NullPointerException e) {
                        FlowReport.reportAction(e.getMessage(), false);
                    }

                    //if domains is absent - create new
                    List<String> activeDomainsList = methods.checkThatDomainsIsExist(setOfTestCases);

                    FlowReport.nextStep();
                    FlowReport.addStep("Updating 'DomainName' (" + activeDomainsList.get(0) + ") field in DB.", "");
                    setOfTestCases.updateString("DomainName", activeDomainsList.get(0));
                    setOfTestCases.updateRow();
                    FlowReport.reportAction("Field 'DomainName' updated", true);

                    //if projects is absent - create new
                    List<String> listOfActiveProjectKeys = methods.checkThatProjectsIsExist(setOfTestCases);

                    for (int i = 0; i < listOfActiveProjectKeys.size(); i++) {
                        FlowReport.nextStep();
                        FlowReport.addStep("Updating 'ProjectKey' (" + listOfActiveProjectKeys.get(i) + ") field in DB.", "");
                        setOfTestCases.updateString("ProjectKey", listOfActiveProjectKeys.get(i));
                        setOfTestCases.updateRow();
                        FlowReport.reportAction("Field 'ProjectKey' updated", true);

                        //if vcs is absent in project - add new vcs
                        methods.checkThatVcsIsExist(setOfTestCases, setOfTestCases.getString("ProjectKey"));

                        FlowReport.nextStep();
                        FlowReport.addStep("Receive the response from client method.", "");
                        String response = (String) method.invoke(obj, setOfTestCases);
                        ObjectMapper mapper = new ObjectMapper();
                        Vcs responseObject = mapper.readValue(response, Vcs.class);
                        FlowReport.reportAction("Response about project vcs recieved ", true);
                        methods.saveAndAddResponse(response);

                        FlowReport.nextStep();
                        FlowReport.addStep("Validating recieved data", "");
                        if (response != null) {
                            try {
                                if (!(responseObject.getIsValid().equals(null)) & !(responseObject.getErrorMessage().equals(null))) {
                                    FlowReport.reportAction("Validation done good!", true);
                                } else {
                                    FlowReport.reportAction("Validation failed!", false);
                                }
                            } catch (Exception e) {
                                FlowReport.reportAction(e.getMessage(), false);
                            }
                        }
                    }
                } catch (Throwable ex) {
                    FlowReport.reportAction(ex.getMessage() == null ? "Response wasn`t returned" : ex.getMessage(), false);
                } finally {
                    methods.logOut();
                    FlowReport.finishTest();
                }
            }
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
    }

    // smell validation
    public void checkUpdateGitStory(ResultSet setOfTestCases) {
        try {
            FlowReport.initFlow(setOfTestCases, testInitiatorClass);
            while (setOfTestCases.next()) {
                try {
                    FlowReport.startTest(setOfTestCases.getString("TestCaseName"));
                    FlowReport.addStep("Check that project can be updated.", "");
                    Method method = clientClass.getDeclaredMethod(responseMethodName, ResultSet.class);
                    Object obj = clientClass.newInstance();

                    try {
                        methods.authorizeWithSpecificCreds(setOfTestCases.getString("Login"), setOfTestCases.getString("Password"));
                        FlowReport.reportAction("Authorization done. Token received.", true);
                    } catch (NullPointerException e) {
                        FlowReport.reportAction(e.getMessage(), false);
                    }

                    //if domains is absent - create new
                    List<String> activeDomainsList = methods.checkThatDomainsIsExist(setOfTestCases);

                    FlowReport.nextStep();
                    FlowReport.addStep("Updating 'DomainName' field in DB.", "");
                    setOfTestCases.updateString("DomainName", activeDomainsList.get(0));
                    setOfTestCases.updateRow();
                    FlowReport.reportAction("Field 'DomainName' updated", true);

                    //if projects is absent - create new
                    List<String> listOfActiveProjectKeys = methods.checkThatProjectsIsExist(setOfTestCases);

                    for (int i = 0; i < listOfActiveProjectKeys.size(); i++) {
                        FlowReport.nextStep();
                        FlowReport.addStep("Updating 'ProjectKey' (" + listOfActiveProjectKeys.get(i) + ") field in DB.", "");
                        setOfTestCases.updateString("ProjectKey", listOfActiveProjectKeys.get(i));
                        setOfTestCases.updateRow();
                        FlowReport.reportAction("Field 'ProjectKey' updated", true);

                        List<Integer> listOfStoryIds = methods.getListOfStoryIdsByProjectKey(setOfTestCases.getString("ProjectKey"));

                        String storiesIds = listOfStoryIds.toString().replace("[", "").replace("]", "").replace(" ", "");
                        FlowReport.nextStep();
                        FlowReport.addStep("Updating 'StoriesIds'  field in DB.", "");
                        setOfTestCases.updateString("StoriesIds", storiesIds);
                        setOfTestCases.updateRow();
                        FlowReport.reportAction("Field 'StoriesIds' updated", true);

                        FlowReport.nextStep();
                        FlowReport.addStep("Receive the response from client method.", "");
                        String response = (String) method.invoke(obj, setOfTestCases);
                        FlowReport.reportAction("Response about project updating recieved ", true);
                        methods.saveAndAddResponse(response);

                        FlowReport.nextStep();
                        FlowReport.addStep("Validating recieved data", "");
                        if (response != null) {
                            try {
                                if (!response.equals("")) {
                                    FlowReport.reportAction("Response contatins some text or status code not 200.", false);
                                } else {
                                    FlowReport.reportAction("Validation done good!", true);
                                }
                            } catch (Exception e) {
                                FlowReport.reportAction(e.getMessage(), false);
                            }
                        }
                    }
                } catch (Throwable ex) {
                    FlowReport.reportAction(ex.getMessage() == null ? "Response wasn`t returned" : ex.getMessage(), false);
                } finally {
                    methods.logOut();
                    FlowReport.finishTest();
                }
            }
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
    }

    // fails. Bug: REL-2858
    public void checkJiraLinkedStoryDetails(ResultSet setOfTestCases) {
        try {
            FlowReport.initFlow(setOfTestCases, testInitiatorClass);
            while (setOfTestCases.next()) {
                try {
                    FlowReport.startTest(setOfTestCases.getString("TestCaseName"));
                    FlowReport.addStep("Retrieve an info of Jira-linked feature.", "");
                    Method method = clientClass.getDeclaredMethod(responseMethodName, ResultSet.class);
                    Object obj = clientClass.newInstance();

                    try {
                        methods.authorizeWithSpecificCreds(setOfTestCases.getString("Login"), setOfTestCases.getString("Password"));
                        FlowReport.reportAction("Authorization done. Token received.", true);
                    } catch (NullPointerException e) {
                        FlowReport.reportAction(e.getMessage(), false);
                    }

                    // check that story is present in project or create it
                    List<Integer> listOfStoryIds = methods.checkThatStoryIsExist(setOfTestCases);

                    FlowReport.nextStep();
                    FlowReport.addStep("Receive the response from client method.", "");
                    String response = (String) method.invoke(obj, setOfTestCases);
                    ObjectMapper mapper = new ObjectMapper();
                    JiraStory responseObject = mapper.readValue(response, JiraStory.class);
                    FlowReport.reportAction("Response about updating scenario recieved ", true);
                    methods.saveAndAddResponse(response);

                    FlowReport.nextStep();
                    FlowReport.addStep("Validating recieved data", "");
                    if (response != null) {
                        JiraStory infoFromRelimeDb = methods.getJiraStoryInfoFromRelimeDbByJiraKey(setOfTestCases.getString("NewJiraStoryKey"));
                        try {
                            if (responseObject.equals(infoFromRelimeDb)){
                                FlowReport.reportAction("Validation done good!", true);
                            } else {
                                FlowReport.reportAction("Validation failed! Object is not equals!", false);
                            }
                        } catch (Exception e) {
                            FlowReport.reportAction(e.getMessage(), false);
                        }
                    }
                } catch (Throwable ex) {
                    FlowReport.reportAction(ex.getMessage() == null ? "Response wasn`t returned" : ex.getMessage(), false);
                } finally {
                    methods.logOut();
                    FlowReport.finishTest();
                }
            }
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
    }

    public void checkSandboxProjects(ResultSet setOfTestCases) {
        try {
            FlowReport.initFlow(setOfTestCases, testInitiatorClass);
            while (setOfTestCases.next()) {
                try {
                    FlowReport.startTest(setOfTestCases.getString("TestCaseName"));
                    FlowReport.addStep("Check sandbox projects info", "");
                    Method method = clientClass.getDeclaredMethod(responseMethodName, ResultSet.class);
                    Object obj = clientClass.newInstance();
                    FlowReport.reportAction("New instance created.", true);

                    FlowReport.nextStep();
                    FlowReport.addStep("Receive the response from client method.", "");
                    String response = (String) method.invoke(obj, setOfTestCases);
                    ObjectMapper mapper = new ObjectMapper();
                    Results responseObject = mapper.readValue(response, Results.class);
                    FlowReport.reportAction("Response about sandbox projects recieved ", true);
                    methods.saveAndAddResponse(response);

                    FlowReport.nextStep();
                    FlowReport.addStep("Updating 'SandboxToken' (" + sanboxToken + ") field in DB.", "");
                    setOfTestCases.updateString("SandboxToken", sanboxToken);
                    setOfTestCases.updateRow();
                    FlowReport.reportAction("Field 'SandboxToken' updated", true);

                    FlowReport.nextStep();
                    FlowReport.addStep("Validating recieved data", "");
                    if (response != null) {
                        Results allProjectsList = methods.getListOfProjectsByDomainName(setOfTestCases.getString("DomainName"));
                        try {
                            if (methods.validateResultsInfo(responseObject, allProjectsList)) {
                                FlowReport.reportAction("Data succsessfuly validated with RelimeDatabase", true);
                            } else {
                                FlowReport.reportAction("Data in DB and in response is not the same", false);
                            }
                        } catch (Exception e) {
                            FlowReport.reportAction(e.getMessage(), false);
                        }


                    }
                } catch (Throwable ex) {
                    FlowReport.reportAction(ex.getMessage() == null ? "Response wasn`t returned" : ex.getMessage(), false);
                } finally {
                    methods.logOut();
                    FlowReport.finishTest();
                }
            }
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
    }

    // update me
    public void checkLeaveSandbox(ResultSet setOfTestCases) {

    }

}