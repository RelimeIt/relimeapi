package Clients;

import Common.Methods;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import static Helpers.ConfigurationInfo.getURLWithPrefix;
import static Helpers.ConfigurationInfo.setURLWithPrefix;

/**
 * Created by mukhin on 11/21/2016.
 */
public class VcsClient {
    Methods methods = new Methods();

    // isRepositoryAccesible have a typo "s", must be "ss"
    private String getContentForRequest(ResultSet testCase) throws SQLException {
        StringBuilder sb = new StringBuilder();
        sb.append("{\"id\":\"").append(testCase.getString("RequestId")).append("\",").append("\"vcsType\":\"").append(testCase.getString("VCSType")).append("\",").append("\"url\":\"").append(testCase.getString("URL")).append("\",").append("\"belongToAccount\":\"").append(testCase.getString("BelongToAccount")).append("\",").append("\"isRepositoryAccesible\":\"").append(testCase.getString("IsRepositoryAccesible")).append("\",").append("\"isValid\":\"").append(testCase.getString("IsValid")).append("\",").append("\"errorMessage\":\"").append(testCase.getString("ErrorMessage")).append("\"}");
        return sb.toString();
    }

    public String getRetrieveVcsResponse(ResultSet testCase) throws SQLException {
        String projectKey = testCase.getString("ProjectKey");
        setURLWithPrefix(testCase.getString("DomainName"));
        return methods.getGETResponse(getURLWithPrefix() + "api/vcs", null, projectKey);
    }

    public String getAddVcsToProjectResponse(ResultSet testCase) throws SQLException, IOException {
        String content = getContentForRequest(testCase);
        String projectKey = testCase.getString("ProjectKey");
        setURLWithPrefix(testCase.getString("DomainName"));
        return methods.getPOSTResponse(getURLWithPrefix() + "api/vcs", content, projectKey);
    }

    public String getValidateVcsResponse(ResultSet testCase) throws SQLException {
        String content = getContentForRequest(testCase);
        String projectKey = testCase.getString("ProjectKey");
        setURLWithPrefix(testCase.getString("DomainName"));
        return methods.getPOSTResponse(getURLWithPrefix() + "api/vcs/validate", content, projectKey);
    }

    public String getUpdateVcsResponse(ResultSet testCase) throws SQLException {
        String content = getContentForRequest(testCase);
        String projectKey = testCase.getString("ProjectKey");
        setURLWithPrefix(testCase.getString("DomainName"));
        return methods.getPUTResponse(getURLWithPrefix()+"api/vcs", content, projectKey);
    }
}
