package Clients;

import Common.Methods;
import org.json.JSONObject;

import java.io.*;
import java.sql.ResultSet;
import java.sql.SQLException;

import static Helpers.ConfigurationInfo.URL;
import static Common.SessionAuthorizationInfo.*;

/**
 * Created by logovskoy on 10/26/2016.
 */
public class AuthClient {
    Methods methods = new Methods();

    public String getAuthorizationResponse(ResultSet testCase) throws IOException, SQLException {

        StringBuilder sb = new StringBuilder();
        sb.append("{\"Email\":\"").append(testCase.getString("Login")).append("\",").append("\"Password\":\"").append(testCase.getString("Password")).append("\"}");
        String content = sb.toString();
        String responseString = methods.getPOSTResponse(URL + "api/auth/login", content, null);
        JSONObject jsonObj = new JSONObject(responseString);
        token = jsonObj.getString("token");
        tokenType = jsonObj.getString("tokenType");
        userId = jsonObj.getInt("userId");
        firstName = jsonObj.getString("firstName");
        lastName = jsonObj.getString("lastName");

        return responseString;
    }

    public String getRegistrationResponse(ResultSet testCase) throws IOException, SQLException {

        StringBuilder sb = new StringBuilder();
        String email = testCase.getString("Email");
        sb.append("{\"Password\":\"").append(testCase.getString("Password")).append("\",").append("\"ConfirmPassword\":\"").append(testCase.getString("ConfirmPassword")).append("\",").append("\"InvitationCode\":\"").append(testCase.getString("InvitationCode")).append("\",").append("\"firstName\":\"").append(testCase.getString("FirstName")).append("\",").append("\"lastName\":\"").append(testCase.getString("LastName")).append("\",").append("\"email\":\"").append(email).append("\",").append("\"isActive\":\"").append(testCase.getString("IsActive")).append("\"}");
        String content = sb.toString();
        return methods.getPOSTResponse(URL + "api/auth/register", content, null);
    }

    public String getValidationResponse(ResultSet testCase) throws SQLException, IOException {
        String email = testCase.getString("Email");
        return methods.getGETResponse(URL + "api/auth/validate", "email=" + email, null);
    }

    public String getChangePasswordResponse(ResultSet testCase) throws SQLException, IOException {
        StringBuilder sb = new StringBuilder();
        sb.append("{\"OldPassword\":\"").append(testCase.getString("Password")).append("\",").append("\"NewPassword\":\"").append(testCase.getString("NewPassword")).append("\",").append("\"ConfirmNewPassword\":\"").append(testCase.getString("ConfirmNewPassword")).append("\"}");
        String content = sb.toString();
        String responseString = methods.getPUTResponse(URL + "api/auth/changePassword", content, null);
        JSONObject jsonObj = new JSONObject(responseString);
        token = jsonObj.getString("token");
        return responseString;
    }

    public String getChangeBasicInfoResponse(ResultSet testCase) throws SQLException, IOException {
        StringBuilder sb = new StringBuilder();
        sb.append("{\"firstName\":\"").append(testCase.getString("FirstName")).append("\",").append("\"lastName\":\"").append(testCase.getString("LastName")).append("\",").append("\"email\":\"").append(testCase.getString("Login")).append("\",").append("\"isActive\":\"").append(testCase.getString("IsActive")).append("\"}");
        String content = sb.toString();
        return methods.getPUTResponse(URL + "api/auth/basicInfo", content, null);
    }

    public String getBasicInfoResponse(ResultSet testCase) {
        return methods.getGETResponse(URL + "api/auth/basicInfo", null, null);
    }
}