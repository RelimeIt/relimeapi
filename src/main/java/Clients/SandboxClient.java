package Clients;

import Common.Methods;

import java.sql.ResultSet;
import java.sql.SQLException;

import static Helpers.ConfigurationInfo.getURLWithPrefix;
import static Helpers.ConfigurationInfo.setURLWithPrefix;

/**
 * Created by mukhin on 1/4/2017.
 */
public class SandboxClient {
    Methods methods = new Methods();

    public String getLeaveSandboxResponse(ResultSet testCase) throws SQLException {
        setURLWithPrefix(testCase.getString("DomainName"));
        return methods.getDELETEResponse(getURLWithPrefix() + "api/sandbox", null, null);
    }
}
