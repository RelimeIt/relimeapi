package Clients;

import Common.Methods;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

import static Helpers.ConfigurationInfo.URL;
import static Helpers.ConfigurationInfo.getURLWithPrefix;
import static Helpers.ConfigurationInfo.setURLWithPrefix;

/**
 * Created by mukhin on 11/7/2016.
 */
public class DomainClient {
    Methods methods = new Methods();

    public String getDomainCreationResponse(ResultSet testCase) throws SQLException, IOException {
        StringBuilder sb = new StringBuilder();
        sb.append("{\"id\":\"").append(testCase.getString("RequestId")).append("\",").append("\"name\":\"").append(testCase.getString("DomainName")).append("\",").append("\"isPrivate\":\"").append(testCase.getString("IsPrivate")).append("\",").append("\"ownerId\":\"").append(testCase.getString("OwnerId")).append("\"}");
        String content = sb.toString();
        return methods.getPOSTResponse(URL + "api/domains", content, null);
    }

    public String getValidationResponse(ResultSet testCase) throws SQLException {
        String domainName = testCase.getString("DomainName");
        return methods.getGETResponse(URL + "api/domains", "domainName=" + domainName, null);
    }

    public String getHitsResponse(ResultSet testCase) throws SQLException {
        String hits = testCase.getString("Hits");
        return methods.getGETResponse(URL + "api/domains", "hits=" + hits, null);
    }

    public String getDeleteResponse(ResultSet testCase) throws SQLException {
        String domainId = testCase.getString("DomainId");
        return methods.getDELETEResponse(URL + "api/domains", "domainId=" + domainId, null);
    }

    public String getOwnedDomainsResponse(ResultSet testCase) throws SQLException {
        String offset = testCase.getString("Offset");
        String hits = testCase.getString("Hits");
        String onlyOnwned = testCase.getString("OnlyOwned");
        return methods.getGETResponse(URL + "api/domains", "offset=" + offset + "&hits=" + hits + "&onlyOwned=" + onlyOnwned, null);
    }

    public String getUpdateDomainResponse(ResultSet testCase) throws SQLException {
        StringBuilder sb = new StringBuilder();
        sb.append("{\"id\":\"").append(testCase.getString("RequestId")).append("\",").append("\"name\":\"").append(testCase.getString("DomainName")).append("\",").append("\"isPrivate\":\"").append(testCase.getString("IsPrivate")).append("\",").append("\"ownerId\":\"").append(testCase.getString("OwnerId")).append("\"}");
        String content = sb.toString();
        return methods.getPUTResponse(URL + "api/domains", content, null);
    }

    public String getRetreiveDomainResponse(ResultSet testCase) throws SQLException {
        String domainId = testCase.getString("DomainId");
        setURLWithPrefix(testCase.getString("DomainName"));
        return methods.getGETResponse(getURLWithPrefix() + "api/domains", "domainId=" + domainId, null);
    }

    public String getDomainRigthsResponse(ResultSet testCase) throws SQLException {
        String domainNameAccess = testCase.getString("DomainNameAccess");
        String projectKey = testCase.getString("ProjectKey");
        return methods.getGETResponse(URL + "api/domains", "domainNameAccess=" + domainNameAccess + "&projectKey=" + projectKey, null);
    }
}
