package Clients;

import Common.Methods;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

import static Helpers.ConfigurationInfo.URL;
import static Helpers.ConfigurationInfo.getURLWithPrefix;
import static Helpers.ConfigurationInfo.setURLWithPrefix;

/**
 * Created by mukhin on 11/14/2016.
 */
public class ProjectClient {
    Methods methods = new Methods();

    private String getContent(ResultSet testCase) throws SQLException {
        StringBuilder sb = new StringBuilder();
        String searchString = testCase.getString("SearchString");
        String sortParameter = testCase.getString("SortParameter");
        if (searchString == null) {
            searchString = "";
        }
        if (sortParameter == null) {
            sortParameter = "";
        }
        sb.append("{\"searchString\":\"").append(searchString).append("\",").append("\"hits\":\"").append(testCase.getString("Hits")).append("\",").append("\"offset\":\"").append(testCase.getString("Offset")).append("\",").append("\"isPrivate\":\"").append(testCase.getString("IsPrivate")).append("\",").append("\"isPublic\":\"").append(testCase.getString("IsPublic")).append("\",").append("\"sortParameter\":\"").append(sortParameter).append("\",").append("\"isAsc\":\"").append(testCase.getString("IsAsc")).append("\"}");
        return sb.toString();
    }

    public String getAllProjectsResponse(ResultSet testCase) throws IOException, SQLException {
        String content = getContent(testCase);
        setURLWithPrefix(testCase.getString("DomainName"));
        return methods.getPOSTResponse(getURLWithPrefix() + "api/projects/all", content, null);
    }

    public String getSubscribedProjectResponse(ResultSet testCase) throws SQLException, IOException {
        String content = getContent(testCase);
        setURLWithPrefix(testCase.getString("DomainName"));
        return methods.getPOSTResponse(getURLWithPrefix() + "api/projects/subscribed", content, null);
    }

    public String getPublicProjectsResponse(ResultSet testCase) throws IOException, SQLException {
        String content = getContent(testCase);
        setURLWithPrefix(testCase.getString("DomainName"));
        return methods.getPOSTResponse(getURLWithPrefix() + "api/projects/public", content, null);
    }

    public String getSandboxProjectsResponse(ResultSet testCase) throws IOException, SQLException {
        setURLWithPrefix(testCase.getString("DomainName"));
        return methods.getPOSTResponse(getURLWithPrefix() + "api/projects/sandbox", null, null);
    }

    public String getRetrieveProjectResponse(ResultSet testCase) throws SQLException {
        String additionalParameter = testCase.getString("ProjectKey");
        setURLWithPrefix(testCase.getString("DomainName"));
        return methods.getGETResponse(getURLWithPrefix() + "api/projects", null, additionalParameter);
    }

    public String getCreateProjectResponse(ResultSet testCase) throws IOException, SQLException {
        StringBuilder sb = new StringBuilder();
        sb.append("{\"id\":\"").append(testCase.getString("RequestId")).append("\",").append("\"name\":\"").append(testCase.getString("ProjectName")).append("\",").append("\"key\":\"").append(testCase.getString("ProjectKey")).append("\",").append("\"description\":\"").append(testCase.getString("Description")).append("\",").append("\"bddFrameworkType\":\"").append(testCase.getString("BDDFrameworkType")).append("\",").append("\"isActive\":\"").append(testCase.getString("IsActive")).append("\",").append("\"isPrivate\":\"").append(testCase.getString("IsPrivate")).append("\",").append("\"savingMode\":\"").append(testCase.getString("SavingMode")).append("\"}");
        String content = sb.toString();
        setURLWithPrefix(testCase.getString("DomainName"));
        return methods.getPOSTResponse(getURLWithPrefix() + "api/projects", content, null);
    }

    public String getDeleteProjectResponse(ResultSet testCase) throws SQLException {
        String projectId = testCase.getString("ProjectId");
        setURLWithPrefix(testCase.getString("DomainName"));
        return methods.getDELETEResponse(getURLWithPrefix() + "api/projects", "projectId=" + projectId, null);
    }

    public String getUpdateProjectResponse(ResultSet testCase) throws SQLException {
        StringBuilder sb = new StringBuilder();
        sb.append("{\"id\":\"").append(testCase.getString("RequestId")).append("\",").append("\"name\":\"").append(testCase.getString("ProjectName")).append("\",").append("\"key\":\"").append(testCase.getString("ProjectKey")).append("\",").append("\"description\":\"").append(testCase.getString("Comments")).append("\",").append("\"bddFrameworkType\":\"").append(testCase.getString("BDDFrameworkType")).append("\",").append("\"isActive\":\"").append(testCase.getString("IsActive")).append("\",").append("\"isPrivate\":\"").append(testCase.getString("IsPrivate")).append("\",").append("\"savingMode\":\"").append(testCase.getString("SavingMode")).append("\"}");
        String content = sb.toString();
        String projectKey = testCase.getString("ProjectKey");
        setURLWithPrefix(testCase.getString("DomainName"));
        return methods.getPUTResponse(getURLWithPrefix() + "api/projects", content, projectKey);
    }

    public String getIsNewProjectResponse(ResultSet testCase) throws SQLException {
        String isNewProject = testCase.getString("IsNewProject");
        String projectKey = testCase.getString("ProjectKey");
        setURLWithPrefix(testCase.getString("DomainName"));
        return methods.getGETResponse(getURLWithPrefix() + "api/projects", "isNewProject=" + isNewProject, projectKey);
    }

    public String getVcsLinkedResponse(ResultSet testCase) throws SQLException {
        String projectKey = testCase.getString("ProjectKey");
        setURLWithPrefix(testCase.getString("DomainName"));
        return methods.getGETResponse(getURLWithPrefix() + "api/projects/vcs/linked", null, projectKey);
    }

    public String getUpdateStoriesToGitResponse(ResultSet testCase) throws SQLException {
        String storiesIds = testCase.getString("StoriesIds");
        String projectKey = testCase.getString("ProjectKey");
        setURLWithPrefix(testCase.getString("DomainName"));
        return methods.getGETResponse(getURLWithPrefix() + "api/projects", "storiesIds=" + storiesIds, projectKey);
    }
}
