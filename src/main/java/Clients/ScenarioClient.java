package Clients;

import Common.Methods;
import Helpers.Classes.*;

import static Helpers.ConfigurationInfo.getURLWithPrefix;
import static Helpers.ConfigurationInfo.setURLWithPrefix;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by mukhin on 12/1/2016.
 */
public class ScenarioClient {
    Methods methods = new Methods();

    public String getCreateScenarioResponse(ResultSet testCase) throws SQLException, IOException {
        Converter converter = new Converter();
        ScenarioSample scenarioSample = fillScenarioSample(testCase, "Create");
        String content = converter.objectToJson(scenarioSample);
        String projectKey = testCase.getString("ProjectKey");
        setURLWithPrefix(testCase.getString("DomainName"));
        return methods.getPOSTResponse(getURLWithPrefix() + "api/scenarios", content, projectKey);
    }

    public String getDeleteScenarioResponse(ResultSet testCase) throws SQLException {
        Integer scenarioId = testCase.getInt("ScenarioId");
        String projectKey = testCase.getString("ProjectKey");
        setURLWithPrefix(testCase.getString("DomainName"));
        return methods.getDELETEResponse(getURLWithPrefix() + "api/scenarios", "scenarioId=" + scenarioId, projectKey);
    }

    public String getUpdateScenarioResponse(ResultSet testCase) throws SQLException, IOException {
        Converter converter = new Converter();
        ScenarioSample scenarioSample = fillScenarioSample(testCase, "Update");
        String content = converter.objectToJson(scenarioSample);
        String projectKey = testCase.getString("ProjectKey");
        setURLWithPrefix(testCase.getString("DomainName"));
        return methods.getPUTResponse(getURLWithPrefix() + "api/scenarios", content, projectKey);
    }

    public String getRetrieveScenarioInfoById(ResultSet testCase) throws SQLException {
        String projectKey = testCase.getString("ProjectKey");
        Integer scenarioId = testCase.getInt("ScenarioId");
        setURLWithPrefix(testCase.getString("DomainName"));
        return methods.getGETResponse(getURLWithPrefix() + "api/scenarios", "scenarioId=" + scenarioId, projectKey);
    }

    public String getCreateBackgroundResponse(ResultSet testCase) throws SQLException {
        List<String> stepTextList = getListFromDB(testCase, "StepText");
        StringBuilder sb = new StringBuilder();
        sb.append("{\"isModified\":").append(testCase.getString("IsModified")).append(",").append("\"isOpen\":").append(testCase.getString("IsOpen")).append(",").append("\"storyId\":").append(testCase.getInt("StoryId")).append(",").append("\"stepText\":").append(stepTextList).append(",").append("\"type\":\"").append(testCase.getString("Type")).append("\",").append("\"isActive\":").append(testCase.getString("IsActive")).append("}");
        String content = sb.toString();
        String projectKey = testCase.getString("ProjectKey");
        setURLWithPrefix(testCase.getString("DomainName"));
        return methods.getPOSTResponse(getURLWithPrefix() + "api/scenarios", content, projectKey);
    }

    public ScenarioSample fillScenarioSample(ResultSet testCase, String apiMethodName) throws SQLException {
        ScenarioSample scenarioSample = new ScenarioSample();

        if (apiMethodName.equals("Create")) {
            scenarioSample.setExampleTable(getExampleTableObject(testCase));
            scenarioSample.setDescription(testCase.getString("ScenarioDescription"));
            scenarioSample.setName(testCase.getString("ScenarioName"));
            scenarioSample.setTags(getListFromDB(testCase, "Tags"));
            scenarioSample.setIsBriefInfo(Boolean.parseBoolean(testCase.getString("IsBriefInfo")));
            scenarioSample.setStepText(getListFromDB(testCase, "StepText"));
            scenarioSample.setStoryId(testCase.getInt("StoryId"));
            scenarioSample.setType(testCase.getString("Type"));
        }

        else if (apiMethodName.equals("Update")) {
            scenarioSample.setExampleTable(getExampleTableObject(testCase));
            scenarioSample.setDescription(testCase.getString("ScenarioDescription"));
            scenarioSample.setName(testCase.getString("ScenarioName"));
            scenarioSample.setTags(getListFromDB(testCase, "Tags"));
            scenarioSample.setIsBriefInfo(Boolean.parseBoolean(testCase.getString("IsBriefInfo")));
            scenarioSample.setStepText(getListFromDB(testCase, "StepText"));
            scenarioSample.setId(testCase.getInt("ScenarioId"));
            scenarioSample.setScenarioType(testCase.getString("Type"));
        }
        return scenarioSample;
    }

    public List<String> getListFromDB(ResultSet testCase, String textType) throws SQLException {
        List<String> tagsList = new ArrayList<>();
        if (textType.equals("Tags")) {
            tagsList = Arrays.asList(testCase.getString("Tags").split(", "));
        } else if (textType.equals("StepText")) {
            tagsList = Arrays.asList(testCase.getString("StepText").split(", "));
        } else if (textType.equals("Columns")) {
            tagsList = Arrays.asList(testCase.getString("Columns").split(", "));
        } else if (textType.equals("Data")) {
            tagsList = Arrays.asList(testCase.getString("Data").split(", "));
        }
        return tagsList;
    }

    public ExampleTable getExampleTableObject(ResultSet testCase) throws SQLException {
        ExampleTable exampleTable = new ExampleTable();

        exampleTable.setDescription(testCase.getString("TableDescription"));
        exampleTable.setName(testCase.getString("TableName"));
        exampleTable.setColumns(getListFromDB(testCase, "Columns"));
        exampleTable.setData(getDataList(testCase));

        return exampleTable;
    }

    public List<Data> getDataList(ResultSet testCase) throws SQLException {
        List<Data> dataList = new ArrayList<>();
        List<String> columnList = getListFromDB(testCase, "Columns");
        List<String> dataListForColumn = getListFromDB(testCase, "Data");
        Data data = new Data();
        for (int i = 0; i < columnList.size(); i++) {
            data.setAdditionalProperty(columnList.get(i), dataListForColumn.get(i));
            dataList.add(data);
        }
        return dataList;
    }
}
