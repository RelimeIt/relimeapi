package Clients;

import Common.Methods;
import static Helpers.ConfigurationInfo.getURLWithPrefix;
import static Helpers.ConfigurationInfo.setURLWithPrefix;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by mukhin on 12/20/2016.
 */
public class JiraClient {
    Methods methods = new Methods();

    public String getIsUniqueJiraResponse(ResultSet testCase) throws SQLException {
        String projectKey = testCase.getString("ProjectKey");
        String newJiraStoryKey = testCase.getString("NewJiraStoryKey");
        setURLWithPrefix(testCase.getString("DomainName"));
        return methods.getGETResponse(getURLWithPrefix() + "api/jira/validate", "newJiraStoryKey=" + newJiraStoryKey, projectKey);
    }

    public String getArrayOfJiraSummaryResponse(ResultSet testCase) throws SQLException {
        String projectKey = testCase.getString("ProjectKey");
        setURLWithPrefix(testCase.getString("DomainName"));
        return methods.getGETResponse(getURLWithPrefix() + "api/jira", null, projectKey);
    }

    public String getJiraLinkedStoryDetailsResponse(ResultSet testCase) throws SQLException {
        String storyKey = testCase.getString("NewJiraStoryKey");
        String projectKey = testCase.getString("ProjectKey");
        setURLWithPrefix(testCase.getString("DomainName"));
        return methods.getGETResponse(getURLWithPrefix() + "api/jira", "storyKey=" + storyKey, projectKey);
    }
}
