package Clients;

import Common.Methods;

import static Helpers.ConfigurationInfo.getURLWithPrefix;
import static Helpers.ConfigurationInfo.setURLWithPrefix;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mukhin on 11/28/2016.
 */
public class StoryClient {
    Methods methods = new Methods();

    public String getRetrieveStoryResponse(ResultSet testCase) throws SQLException {
        String projectKey = testCase.getString("ProjectKey");
        setURLWithPrefix(testCase.getString("DomainName"));
        return methods.getGETResponse(getURLWithPrefix() + "api/stories", null, projectKey);
    }

    public String getCreateStoryResponse(ResultSet testCase) throws IOException, SQLException {
        List<String> tagsList = getTagsList(testCase);
        StringBuilder sb = new StringBuilder();
        sb.append("{\"name\":\"").append(testCase.getString("StoryName")).append("\",").append("\"tags\":").append(tagsList).append(",").append("\"description\":\"").append(testCase.getString("Description")).append("\",").append("\"parentFolderId\":").append(testCase.getString("ParentFolderId")).append(",").append("\"fileName\":\"").append(testCase.getString("FileName")).append("\"}");
        String content = sb.toString();
        String projectKey = testCase.getString("ProjectKey");
        setURLWithPrefix(testCase.getString("DomainName"));
        return methods.getPOSTResponse(getURLWithPrefix() + "api/stories", content, projectKey);
    }

    public List<String> getTagsList(ResultSet testCase) throws SQLException {
        String str = "\"" + testCase.getString("Tags").replace(" ", "").replace(",", "\",\"") + "\"";
        List<String> listForReturnStatement = new ArrayList<>();
        listForReturnStatement.add(str);
        return listForReturnStatement;
    }

    public String getDeleteStoryResponse(ResultSet testCase) throws SQLException {
        int storyId = testCase.getInt("StoryId");
        String projectKey = testCase.getString("ProjectKey");
        setURLWithPrefix(testCase.getString("DomainName"));
        return methods.getDELETEResponse(getURLWithPrefix() + "api/stories", "storyId=" + storyId, projectKey);
    }

    public String getUpdateStoryResponse(ResultSet testCase) throws SQLException {
        List<String> tagsList = getTagsList(testCase);
        StringBuilder sb = new StringBuilder();
        sb.append("{\"id\":").append(testCase.getString("StoryId")).append(",").append("\"name\":\"").append(testCase.getString("StoryName")).append("\",").append("\"description\":\"").append(testCase.getString("StoryDescription")).append("\",").append("\"tags\":").append(tagsList).append("}");
        String content = sb.toString();
        String projectKey = testCase.getString("ProjectKey");
        setURLWithPrefix(testCase.getString("DomainName"));
        return methods.getPUTResponse(getURLWithPrefix() + "api/stories", content, projectKey);
    }

    public String getCreateJiraLinkedStoryResponse(ResultSet testCase) throws SQLException {
        List<String> tagsList = getTagsList(testCase);
        StringBuilder sb = new StringBuilder();
        sb.append("{\"name\":").append(testCase.getString("StoryName")).append(",").append("\"tags\":").append(tagsList).append(",").append("\"description\":\"").append(testCase.getString("Description")).append("\",").append("\"fileName\":\"").append(testCase.getString("FileName")).append("\",").append("\"managementToolId\":").append(testCase.getString("ManagementToolId")).append(",").append("\"key\":\"").append(testCase.getString("Key_")).append("\",").append("\"parsedScenarios\":").append(testCase.getString("ParsedScenarios")).append(",").append("\"isJiraLinkedStory\":").append(testCase.getString("IsJiraLinkedStory")).append("}");
        String content = sb.toString();
        String projectKey = testCase.getString("ProjectKey");
        setURLWithPrefix(testCase.getString("DomainName"));
        return methods.getPOSTResponse(getURLWithPrefix() + "api/stories", content, projectKey);
    }

}
