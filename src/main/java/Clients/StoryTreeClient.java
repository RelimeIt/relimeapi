package Clients;

import Common.Methods;

import static Helpers.ConfigurationInfo.getURLWithPrefix;
import static Helpers.ConfigurationInfo.setURLWithPrefix;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by mukhin on 11/24/2016.
 */
public class StoryTreeClient {
    Methods methods = new Methods();

    public String getRetrieveStoryTreeResponse(ResultSet testCase) throws SQLException {
        String additionalParameter = testCase.getString("ProjectKey");
        setURLWithPrefix(testCase.getString("DomainName"));
        return methods.getGETResponse(getURLWithPrefix() + "api/stories/tree", null, additionalParameter);
    }

    public String getCreateTreeResponse(ResultSet testCase) throws SQLException, IOException {
        StringBuilder sb = new StringBuilder();
        sb.append("{\"Label\":\"").append(testCase.getString("Label")).append("\",").append("\"ParentId\":").append(testCase.getString("ParentId")).append("}");
        String content = sb.toString();
        String additionalParameter = testCase.getString("ProjectKey");
        setURLWithPrefix(testCase.getString("DomainName"));
        return methods.getPOSTResponse(getURLWithPrefix() + "api/stories/tree", content, additionalParameter);
    }

    public String getDeleteStoryTreeResponse(ResultSet testCase) throws SQLException {
        String projectKey = testCase.getString("ProjectKey");
        String branchId = testCase.getString("RequestId");
        setURLWithPrefix(testCase.getString("DomainName"));
        return methods.getDELETEResponse(getURLWithPrefix() + "api/stories/tree", "id=" + branchId, projectKey);
    }

    public String getUpdateStoryTreeResponse(ResultSet testCase) throws IOException, SQLException {
        StringBuilder sb = new StringBuilder();
        sb.append("{\"id\":\"").append(testCase.getString("RequestId")).append("\",").append("\"parentId\":").append(testCase.getString("ParentId")).append(",").append("\"label\":\"").append(testCase.getString("Label")).append("\"}");
        String content = sb.toString();
        String additionalParameter = testCase.getString("ProjectKey");
        setURLWithPrefix(testCase.getString("DomainName"));
        return methods.getPUTResponse(getURLWithPrefix() + "api/stories/tree", content, additionalParameter);
    }
}
