package Clients;

import Common.Methods;

import static Helpers.ConfigurationInfo.getURLWithPrefix;
import static Helpers.ConfigurationInfo.setURLWithPrefix;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by mukhin on 12/13/2016.
 */
public class TaskTrackerClient {
    Methods methods = new Methods();

    public String getContent(ResultSet testCase) throws SQLException {
        StringBuilder sb = new StringBuilder();
        sb.append("{\"id\":").append(testCase.getString("RequestId")).append(",").append("\"name\":\"").append(testCase.getString("TaskTrackerName")).append("\",").append("\"url\":\"").append(testCase.getString("TaskTrackerURL")).append("\",").append("\"belongsToAccount\":").append(testCase.getString("BelongsToAccount")).append(",").append("\"isAccessible\":").append(testCase.getString("IsAccessible")).append("}");
        return sb.toString();
    }

    public String getCreateTaskTrackerSystemResponse(ResultSet testCase) throws SQLException {
        String content = getContent(testCase);
        String projectKey = testCase.getString("ProjectKey");
        setURLWithPrefix(testCase.getString("DomainName"));
        return methods.getPOSTResponse(getURLWithPrefix() + "api/tasktracker", content, projectKey);
    }

    public String getDeleteTaskTrackerSystemResponse(ResultSet testCase) throws SQLException {
        String projectKey = testCase.getString("ProjectKey");
        Integer taskTrackerId = testCase.getInt("TaskTrackerId");
        setURLWithPrefix(testCase.getString("DomainName"));
        return methods.getDELETEResponse(getURLWithPrefix() + "api/tasktracker", "taskTrackerId=" + taskTrackerId, projectKey);
    }

    public String getRetrieveTaskTrackerSystem(ResultSet testCase) throws SQLException {
        String projectKey = testCase.getString("ProjectKey");
        setURLWithPrefix(testCase.getString("DomainName"));
        return methods.getGETResponse(getURLWithPrefix() + "api/tasktracker", null, projectKey);
    }

    public String getUpdateTaskTrackerSystemResponse(ResultSet testCase) throws SQLException {
        String content = getContent(testCase);
        String projectKey = testCase.getString("ProjectKey");
        setURLWithPrefix(testCase.getString("DomainName"));
        return methods.getPUTResponse(getURLWithPrefix() + "api/tasktracker", content, projectKey);
    }

    public String getValidateConnectionToTaskTracker(ResultSet testCase) throws SQLException {
        String content = getContent(testCase);
        String projectKey = testCase.getString("ProjectKey");
        setURLWithPrefix(testCase.getString("DomainName"));
        return methods.getPOSTResponse(getURLWithPrefix() + "api/tasktracker/connection", content, projectKey);
    }
}
