package Clients;

import Common.Methods;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

import static Helpers.ConfigurationInfo.URL;
import static Helpers.ConfigurationInfo.getURLWithPrefix;
import static Helpers.ConfigurationInfo.setURLWithPrefix;

/**
 * Created by mukhin on 11/22/2016.
 */

public class AccountClient {
    Methods methods = new Methods();

    public String getCreateAccountResponse(ResultSet testCase) throws SQLException, IOException {
        StringBuilder sb = new StringBuilder();
        sb.append("{\"password\":\"").append(testCase.getString("AccountPassword")).append("\",").append("\"isPasswordChanged\":\"").append(testCase.getString("IsPasswordChanged")).append("\",").append("\"login\":\"").append(testCase.getString("AccountLogin")).append("\",").append("\"id\":\"").append(testCase.getString("AccountId")).append("\",").append("\"accountName\":\"").append(testCase.getString("AccountName")).append("\"}");
        String content = sb.toString();
        return methods.getPOSTResponse(URL + "api/accounts", content, null);
    }

    public String getUpdateAccountResponse(ResultSet testCase) throws SQLException {
        StringBuilder sb = new StringBuilder();
        sb.append("{\"password\":\"").append(testCase.getString("AccountPassword")).append("\",").append("\"isPasswordChanged\":\"").append(testCase.getString("IsPasswordChanged")).append("\",").append("\"login\":\"").append(testCase.getString("AccountLogin")).append("\",").append("\"id\":\"").append(testCase.getString("AccountId")).append("\",").append("\"accountName\":\"").append(testCase.getString("AccountName")).append("\"}");
        String content = sb.toString();
        return methods.getPUTResponse(URL + "api/accounts", content, null);
    }

    public String getDeleteAccountResponse(ResultSet testCase) throws SQLException {
        String accountId = testCase.getString("AccountId");
        return methods.getDELETEResponse(URL + "api/accounts", "accountId=" + accountId, null);
    }

    public String getRetrieveAccountsResponse (ResultSet testCase) throws SQLException {
        String includeLoginNames = testCase.getString("IncludeLoginNames");
        return methods.getGETResponse(URL + "api/accounts", "includeLoginNames=" + includeLoginNames, null);
    }
}
