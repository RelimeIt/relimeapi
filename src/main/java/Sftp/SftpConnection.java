package Sftp;

import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import static Helpers.ConfigurationInfo.*;

/**
 * Created by mukhin on 12/28/2016.
 */

public class SftpConnection {
    public ChannelSftp getSessionConnection() throws JSchException {
        JSch jsch = new JSch();
        Session session = jsch.getSession(serverLogin, databaseIp, 22);
        session.setPassword(serverPassword);
        session.setConfig("StrictHostKeyChecking", "no");
        session.connect();
        ChannelSftp sftpChannel = (ChannelSftp) session.openChannel("sftp");
        sftpChannel.connect();
        return sftpChannel;
    }
}
