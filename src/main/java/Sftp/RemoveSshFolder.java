package Sftp;

import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.SftpException;

import java.util.Vector;

/**
 * Created by mukhin on 12/28/2016.
 */
public class RemoveSshFolder {

    public String getFolderDirectory(String domainName, String projectKey) {
        return "C:\\Saved repositories\\" + domainName + "\\" + projectKey;
    }

    public static void RemoveFolderViaSSH(ChannelSftp session, String directory) throws SftpException {
        Vector v = session.ls(directory);
        for (Object e : v) {
            ChannelSftp.LsEntry entry = (ChannelSftp.LsEntry) e;
            if (entry.getLongname().startsWith("-")) {
                session.chmod(511, directory + "/" + entry.getFilename());
                session.rm(directory + "/" + entry.getFilename());
            } else {
                RemoveFolderViaSSH(session, directory + "/" + entry.getFilename());
            }
        }
        session.chmod(511, directory);
        session.rmdir(directory);
    }
}
