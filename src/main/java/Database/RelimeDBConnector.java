package Database;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static Helpers.ConfigurationInfo.*;

/**
 * Created by logovskoy on 11/1/2016.
 */
public class RelimeDBConnector {
    //static Statement statement;
    public static Connection connection = null;
    public static List<Statement> statementList = new ArrayList<>();

static {
    try
    {
        if(connection==null||connection.isClosed()) {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            connection = DriverManager.getConnection("jdbc:sqlserver://52.58.1.102;databaseName=RelimeDB", dbLogin, dbPassword);
        }
    }catch (Exception ex){
        ex.printStackTrace();

    }
}

    public static Statement createRelimeDBConnection() {
        try {
            Statement statement = connection.createStatement();
            System.out.println("Statement of Relime db created.");
            statementList.add(statement);
            return statement;
        } catch (SQLException e) {
            System.out.println("Connection Failed! Check output console");
            e.printStackTrace();
            return null;
        }
    }

    public static void closeRelimeDBStatement() {
        try {
            for (Statement statement : statementList
                 ) {
                statement.close();
                System.out.println("Statement of Relime db closed.");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static ResultSet executeQueryToRelimeBase(String query) throws SQLException {
        Statement x = createRelimeDBConnection();
        return x.executeQuery(query);
    }
}
