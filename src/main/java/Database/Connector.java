package Database;

import java.security.spec.ECField;
import java.sql.*;

import static Helpers.ConfigurationInfo.*;

/**
 * Created by logovskoy on 10/27/2016.
 */
public class Connector {
    static Connection conn;
    static Statement statement;

  /*  public ResultSet getSetOfResults(String tableName) throws ClassNotFoundException, IllegalAccessException, InstantiationException, SQLException {

        Class.forName("com.mysql.jdbc.Driver").newInstance();

        Statement statement = conn.createStatement();
        ResultSet results = statement.executeQuery("SELECT * FROM `relimeapi`.`"+tableName+"`");
        return results;
    }
*/
  static {
      try{
      Class.forName("com.mysql.jdbc.Driver").newInstance();
          if (conn == null) {
              conn = DriverManager.getConnection("jdbc:mysql://" + DBLocation + ":3306/relimeapi?" +
                      "user=APIUser&password=1234");
          }
      }catch (Exception ex){
          ex.printStackTrace();
      }
  }

    public static Statement testCasesDatabaseConnection() {
        try {
                if (statement == null||statement.isClosed()) {
                    statement = conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
                    System.out.println("Statement of testCases db created.");
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }


        return statement;
    }

    public static ResultSet getTestCases(String tableName, String assertionType, String businessCriticality) {

        try {
            if (statement == null) {
                statement = testCasesDatabaseConnection();
            }
        } catch (Exception e) {
            e.printStackTrace();

        }
        ResultSet resultSet = null;
        try {
            resultSet = statement.executeQuery("SELECT * FROM " + tableName.toLowerCase() + " WHERE AssertionType = '" + assertionType.toLowerCase() + "' AND BusinessCriticality = '" + businessCriticality.toLowerCase() + "'");

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return resultSet;
    }

    public static void closeMySqlStatement() {
        try {
            if(statement!=null||!statement.isClosed()){
            statement.close();
            statement = null;
            System.out.println("Statement of testCases db  closed.");}
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
